package models

type HeadReq struct {
	Key   string
	Value string
}

type DataSource struct {
	Url    string
	Source string
	Symbol string
	Path   string
	Method string
	Reader string
	Header []*HeadReq
}
