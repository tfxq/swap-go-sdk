package models

import "github.com/ethereum/go-ethereum/common"

type Config struct {
	Token  Token `yaml:"token"`
	KuSwap KuSwap
	Mojito Mojito `yaml:"mojito"`
	Cherry Cherry
	Chain  Chain `yaml:"chain"`
}

type Token struct {
	BTCAddress   string
	ETHAddress   string
	SKCSAddress  string
	WKCSAddress  string
	WOKTAddress  string
	OKBAddress   string
	USDTAddress  string
	USDCAddress  string
	MJTAddress   string
	CHEAddress   string
	APEAddress   string
	MULTIAddress string
	SAXAddress   string
	SANDAddress  string
	MANAAddress  string
	MLSAddress   string
	AAVEAddress  string
	CRVAddress   string
	UNIAddress   string
	CFXAddress   string
	LINKAddress  string
	KUSAddress   string
}

type KuSwap struct {
	KuswapRouter string
}

type Mojito struct {
	MojitoFactory    string `yaml:"mojitoFactory"`
	Masterchef       string `yaml:"masterchef"`
	MasterchefV2     string
	MojitoRouter     string `yaml:"mojitorouter"`
	SwapWrapper      string
	SwapWrapperV2    string
	SKCSWrapper      string
	SKCSWrapperV2    string
	SwapMiningAddr   string `yaml:"swapMiningAddr"`
	MojitoOracle     string
	LP               Lp     `yaml:"lp"`
	LpPid            LpPid  `yaml:"lpPid"`
	Path             Path   `yaml:"path"`
	EIP712DomainHash string `yaml:"eip712DomainHash"`
	PermitTypeHash   string `yaml:"permitTypeHash"`
}

type Cherry struct {
	CherryFactory    string
	Masterchef       string `yaml:"masterchef"`
	MasterchefV2     string
	CherryRouter     string
	SwapMiningAddr   string `yaml:"swapMiningAddr"`
	LP               Lp     `yaml:"lp"`
	LpPid            LpPid  `yaml:"lpPid"`
	Path             Path   `yaml:"path"`
	EIP712DomainHash string
	PermitTypeHash   string
}

type Lp struct {
	LpMJTUSDT  string `yaml:"LPMjtUsdt"`
	LpMJTUSDC  string `yaml:"LPMjtUsdc"`
	LpUSDTUSDC string `yaml:"LPUsdtUsdc"`
	LpWKCSMJT  string
	LpWKCSUSDC string
	LpWKCSUSDT string
}

type LpPid struct {
	LpMJTUSDTPid  int64 `yaml:"LPMjtUsdtPid"`
	LpMJTUSDCPid  int64 `yaml:"LPMjtUsdcPid"`
	LpUSDTUSDCPid int64 `yaml:"LPUsdtUsdcPid"`
}

type Path struct {
	PathUsdcUsdt []common.Address
	PathUsdtUsdc []common.Address
	PathUsdtMjt  []common.Address
	PathMjtUsdt  []common.Address
	PathUsdtChe  []common.Address
	PathCheUsdt  []common.Address
}

type Chain struct {
	Network    string `yaml:"network"`
	ChainId    int64  `yaml:"chainId"`
	TXUrl      string `yaml:"txUrl"`
	RPCUrl     string `yaml:"rpcUrl"`
	Multicall  string
	Multicall2 string
}
