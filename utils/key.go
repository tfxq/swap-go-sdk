package utils

import (
	"context"
	"crypto/ecdsa"
	"gitee.com/tfxq/swap-go-sdk/info"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"math/big"
	"strings"
)

func VerifyPriKey(key string) (error, string, string) {
	addrLowerStr := strings.ToLower(key)
	if strings.HasPrefix(addrLowerStr, "0x") {
		addrLowerStr = addrLowerStr[2:]
		key = key[2:]
	}
	privateKey, err := crypto.HexToECDSA(key)
	if err != nil {
		return err, "", key
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return err, "", key
	}
	address := crypto.PubkeyToAddress(*publicKeyECDSA).Hex()
	return nil, address, key
}

func ConvertKeyToAddr(key string) (error, string) {
	addrLowerStr := strings.ToLower(key)
	if strings.HasPrefix(addrLowerStr, "0x") {
		addrLowerStr = addrLowerStr[2:]
		key = key[2:]
	}
	privateKey, err := crypto.HexToECDSA(key)
	if err != nil {
		return err, ""
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return err, ""
	}
	address := crypto.PubkeyToAddress(*publicKeyECDSA).Hex()
	return nil, address
}

func ConvertKeyToAuth(privateKey string, client *ethclient.Client) (error, string, *bind.TransactOpts) {
	addrLowerStr := strings.ToLower(privateKey)
	if strings.HasPrefix(addrLowerStr, "0x") {
		addrLowerStr = addrLowerStr[2:]
		privateKey = privateKey[2:]
	}
	userPrivateKey, err := crypto.HexToECDSA(privateKey)
	if err != nil {
		return errors.Wrap(err, "private key err"), "", nil
	}

	publicKey := userPrivateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return errors.New("cannot assert type: publicKey is not of type *ecdsa.PublicKey"), "", nil
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	//logger.SugarLogger.Infof("addr：%v", fromAddress.String())

	//chainID, err := client.ChainID(context.TODO())
	//if err != nil {
	//	return errors.Wrap(err, "chainId fail"), "", nil
	//}

	chainID := info.ChainId

	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		return errors.Wrap(err, "PendingNonceAt err"), "", nil
	}

	auth, err := bind.NewKeyedTransactorWithChainID(userPrivateKey, chainID)
	if err != nil {
		return errors.Wrap(err, "NewKeyedTransactorWithChainID err"), "", nil
	}

	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0) // in wei

	var clientGasPrice *big.Int
	if info.GasPrice > 0 && info.GasPrice < 10 {
		clientGasPrice = GWei(info.GasPrice)
	} else {
		suggestGasPrice, err2 := client.SuggestGasPrice(context.Background())
		if err2 != nil {
			return errors.Wrap(err2, "suggest gas price err"), "", nil
		}
		clientGasPrice = suggestGasPrice
	}

	gasPriceMultipoint := info.GasPriceMultiPoint
	finalGasPrice := decimal.NewFromBigInt(clientGasPrice, 0).Mul(decimal.NewFromFloat(gasPriceMultipoint)).BigInt()

	auth.GasPrice = finalGasPrice
	return nil, fromAddress.String(), auth
}
