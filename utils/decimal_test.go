package utils

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPow(t *testing.T) {
	t.Log(pow(2.0, 256))
}

func pow(x float64, n int) float64 {
	if x == 0 {
		return 0
	}
	result := calPow(x, n)
	if n < 0 {
		result = 1 / result
	}
	return result
}

func calPow(x float64, n int) float64 {
	if n == 0 {
		return 1
	}
	if n == 1 {
		return x
	}

	// 向右移动一位
	result := calPow(x, n>>1)
	result *= result

	// 如果n是奇数
	if n&1 == 1 {
		result *= x
	}

	return result
}

func TestDecimal(t *testing.T) {
	var tt = []struct {
		v           interface{}
		expectedErr bool
	}{
		{"1.1", false},
		{int(1), false},
		{int(-1), false},
		{int8(1), false},
		{int16(1), false},
		{int32(1), false},
		{int64(-1), false},
		{int32(-1), false},
		{uint64(1), false},
		{float64(1.1), false},
		{float32(1.1), false},
		{float64(-1.1), false},
		{math.Inf(1), true},
		{math.Inf(-1), true},
		{float32(math.Inf(-1)), true},
		{float32(math.Inf(1)), true},
		{math.NaN(), true},
		{float32(math.NaN()), true},
	}
	for _, tc := range tt {
		tc := tc
		_, err := ToDecimal(tc.v)
		if tc.expectedErr {
			assert.Error(t, err)
		} else {
			assert.NoError(t, err)
		}
	}
}

func TestMath(t *testing.T) {
	usdcBalance, _ := ToDecimal("3953052.789648614852902258")
	usdtBalance, _ := ToDecimal("3956005.9152892063450798")
	userUsdtBalance, _ := ToDecimal(20)
	//在使用decimal进行高精度除法时候需注意官方默认是16，其他精度需要自行调整
	userUsdcBalance := usdcBalance.DivRound(usdtBalance, 36).Mul(userUsdtBalance).RoundDown(18)
	t.Logf("userUsdcBalance:%v", userUsdcBalance)
}

func TestRemoveMath(t *testing.T) {
	poolShare, _ := ToDecimal("0.000010109786260370019549554378861405")
	pathABalance, _ := ToDecimal("3952990139220476659794709")
	pathBBalance, _ := ToDecimal("3957206294249096761804667")
	//在使用decimal进行高精度除法时候需注意官方默认是16，其他精度需要自行调整
	t.Logf("poolShare:%v", poolShare.RoundDown(18))
	amountAMax := poolShare.Mul(pathABalance).RoundDown(0)
	amountBMax := poolShare.Mul(pathBBalance).RoundDown(0)
	t.Logf("amountAMax:%v amountBMax:%v", amountAMax, amountBMax)
}
