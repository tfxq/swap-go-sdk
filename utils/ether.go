package utils

import (
	"github.com/shopspring/decimal"
	"math/big"
)

func ParseEtherString(value string) string {
	valueDecimal, err := decimal.NewFromString(value)
	if err != nil {
		return ""
	}
	return valueDecimal.DivRound(decimal.New(1, 18), 18).StringFixed(18)
}

func ParseEther(value *big.Int) string {
	return decimal.NewFromBigInt(value, 0).DivRound(decimal.New(1, 18), 18).StringFixed(18)
}

func ParseEtherDec(value decimal.Decimal) string {
	return value.DivRound(decimal.New(1, 18), 18).StringFixed(18)
}

func ParseEtherReturnDec(value *big.Int) decimal.Decimal {
	return decimal.NewFromBigInt(value, 0).DivRound(decimal.New(1, 18), 18)
}

func ParseEtherBigInt(value *big.Int) *big.Int {
	return decimal.NewFromBigInt(value, 0).DivRound(decimal.New(1, 18), 18).BigInt()
}
