package utils

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/pkg/errors"
	"strings"
)

func Sign(message []byte, privateKeyString string) (err error, rs, ss [32]byte, vs uint8) {

	addrLowerStr := strings.ToLower(privateKeyString)
	if strings.HasPrefix(addrLowerStr, "0x") {
		addrLowerStr = addrLowerStr[2:]
		privateKeyString = privateKeyString[2:]
	}

	privateKey, err := crypto.HexToECDSA(privateKeyString)
	if err != nil {
		return errors.Wrap(err, "HexToECDSA err"), rs, ss, vs
	}

	sig, err := crypto.Sign(message, privateKey)
	if err != nil {
		return errors.Wrap(err, "sign failed"), rs, ss, vs
	}

	rs = common.BytesToHash(sig[:32])
	ss = common.BytesToHash(sig[32:64])
	byteToInt := BytesToInt(sig[64:])
	vs = uint8(byteToInt + 27)
	return err, rs, ss, vs
}
