// Package gethwrappers keeps track of the golang wrappers of the solidity contracts
package main

//go:generate ./compile.sh 15000 ../../../contracts/AccessControlledOffchainAggregator.sol
//go:generate ./compile.sh 15000 ../../../contracts/OffchainAggregator.sol
//go:generate ./compile.sh 15000 ../../../contracts/chainlink/EACAggregatorProxy.sol
//go:generate ./compile.sh 15000 ../../../contracts/mojito/MojitoRouter.sol
//go:generate ./compile.sh 15000 ../../contracts/mojito/MasterChefV2.sol
//go:generate ./compile.sh 15000 ../../contracts/mojito/WKCS.sol
//go:generate ./compile.sh 15000 ../../contracts/mojito/IERC20.sol
//go:generate ./compile.sh 15000 ../../contracts/MasterChef.sol
//go:generate ./compile.sh 15000 ../../contracts/SwapMining.sol
//go:generate ./compile.sh 15000 ../../contracts/IMojitoPair.sol
//go:generate ./compile.sh 15000 ../../contracts/IMojitoFactory.sol
//go:generate ./compile.sh 15000 ../../contracts/cherry/CherryRouter.sol
//go:generate ./compile.sh 15000 ../../contracts/cherry/ICherryPair.sol
//go:generate ./compile.sh 15000 ../../contracts/mojito/SwapWrapper.sol
//go:generate ./compile.sh 15000 ../../contracts/skcs/SKCSWrapper.sol
//go:generate ./compile.sh 15000 ../../contracts/skcs/SKCSWrapperV2.sol
//go:generate ./compile.sh 15000 ../../contracts/kuswap/KuswapRouter02.sol
//go:generate ./compile.sh 15000 ../../contracts/mojito/SwapWrapperV2.sol
//go:generate ./compile.sh 15000 ../../contracts/skcs/SKCSWrapperV2.sol
