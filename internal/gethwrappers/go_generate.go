// Package gethwrappers provides tools for wrapping solidity contracts with
// golang packages, using abigen.
package gethwrappers

// Make sure solidity compiler artifacts are up to date. Only output stdout on failure.
//go:generate ./generation/compile_contracts.sh

//go:generate go run ./generation/generate/wrap.go ../gethwrappers2/OffchainAggregator/OffchainAggregator.abi - OffchainAggregator offchain_aggregator_wrapper
//go:generate go run ./generation/generate/wrap.go ../gethwrappers2/eacaggregatorproxy/EACAggregatorProxy.abi - EACAggregatorProxy aggregator_proxy_wrapper
