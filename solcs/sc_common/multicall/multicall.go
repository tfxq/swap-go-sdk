// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package multicall

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

type MulticallCall struct {
	Target   common.Address
	CallData []byte
}

var MulticallMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"target\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"callData\",\"type\":\"bytes\"}],\"internalType\":\"structMulticall.Call[]\",\"name\":\"calls\",\"type\":\"tuple[]\"}],\"name\":\"aggregate\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"blockNumber\",\"type\":\"uint256\"},{\"internalType\":\"bytes[]\",\"name\":\"returnData\",\"type\":\"bytes[]\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"blockNumber\",\"type\":\"uint256\"}],\"name\":\"getBlockHash\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"blockHash\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getCurrentBlockCoinbase\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"coinbase\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getCurrentBlockDifficulty\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"difficulty\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getCurrentBlockGasLimit\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"gaslimit\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getCurrentBlockTimestamp\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"timestamp\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"addr\",\"type\":\"address\"}],\"name\":\"getEthBalance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"balance\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getLastBlockHash\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"blockHash\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"addr\",\"type\":\"address\"}],\"name\":\"isContract\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561001057600080fd5b50610786806100206000396000f3fe608060405234801561001057600080fd5b50600436106100a35760003560e01c80634d2301cc1161007657806386d516e81161005b57806386d516e814610138578063a8b0574e1461013e578063ee82ac5e1461014c57600080fd5b80634d2301cc1461010a57806372425d9d1461013257600080fd5b80630f28c97d146100a857806316279055146100bd578063252dba42146100e157806327e86d6e14610102575b600080fd5b425b6040519081526020015b60405180910390f35b6100d16100cb36600461044f565b3b151590565b60405190151581526020016100b4565b6100f46100ef366004610470565b61015e565b6040516100b4929190610566565b6100aa610347565b6100aa61011836600461044f565b73ffffffffffffffffffffffffffffffffffffffff163190565b446100aa565b456100aa565b6040514181526020016100b4565b6100aa61015a366004610532565b4090565b8051439060609067ffffffffffffffff8111156101a4577f4e487b7100000000000000000000000000000000000000000000000000000000600052604160045260246000fd5b6040519080825280602002602001820160405280156101d757816020015b60608152602001906001900390816101c25790505b50905060005b835181101561034157600080858381518110610222577f4e487b7100000000000000000000000000000000000000000000000000000000600052603260045260246000fd5b60200260200101516000015173ffffffffffffffffffffffffffffffffffffffff1686848151811061027d577f4e487b7100000000000000000000000000000000000000000000000000000000600052603260045260246000fd5b602002602001015160200151604051610296919061054a565b6000604051808303816000865af19150503d80600081146102d3576040519150601f19603f3d011682016040523d82523d6000602084013e6102d8565b606091505b5091509150816102e757600080fd5b80848481518110610321577f4e487b7100000000000000000000000000000000000000000000000000000000600052603260045260246000fd5b602002602001018190525050508080610339906106e2565b9150506101dd565b50915091565b600061035460014361069b565b40905090565b803573ffffffffffffffffffffffffffffffffffffffff8116811461037e57600080fd5b919050565b600060408284031215610394578081fd5b61039c610623565b90506103a78261035a565b815260208083013567ffffffffffffffff808211156103c557600080fd5b818501915085601f8301126103d957600080fd5b8135818111156103eb576103eb61074a565b61041b847fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0601f8401160161064c565b9150808252868482850101111561043157600080fd5b80848401858401376000908201840152918301919091525092915050565b600060208284031215610460578081fd5b6104698261035a565b9392505050565b60006020808385031215610482578182fd5b823567ffffffffffffffff80821115610499578384fd5b818501915085601f8301126104ac578384fd5b8135818111156104be576104be61074a565b8060051b6104cd85820161064c565b8281528581019085870183870188018b10156104e7578889fd5b8893505b848410156105245780358681111561050157898afd5b61050f8c8a838b0101610383565b845250600193909301929187019187016104eb565b509998505050505050505050565b600060208284031215610543578081fd5b5035919050565b6000825161055c8184602087016106b2565b9190910192915050565b600060408201848352602060408185015281855180845260608601915060608160051b8701019350828701855b82811015610615577fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa0888703018452815180518088526105d881888a018985016106b2565b601f017fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe01696909601850195509284019290840190600101610593565b509398975050505050505050565b6040805190810167ffffffffffffffff811182821017156106465761064661074a565b60405290565b604051601f82017fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe016810167ffffffffffffffff811182821017156106935761069361074a565b604052919050565b6000828210156106ad576106ad61071b565b500390565b60005b838110156106cd5781810151838201526020016106b5565b838111156106dc576000848401525b50505050565b60007fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8214156107145761071461071b565b5060010190565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052601160045260246000fd5b7f4e487b7100000000000000000000000000000000000000000000000000000000600052604160045260246000fdfea164736f6c6343000804000a",
}

var MulticallABI = MulticallMetaData.ABI

var MulticallBin = MulticallMetaData.Bin

func DeployMulticall(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Multicall, error) {
	parsed, err := MulticallMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(MulticallBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Multicall{MulticallCaller: MulticallCaller{contract: contract}, MulticallTransactor: MulticallTransactor{contract: contract}, MulticallFilterer: MulticallFilterer{contract: contract}}, nil
}

type Multicall struct {
	address common.Address
	abi     abi.ABI
	MulticallCaller
	MulticallTransactor
	MulticallFilterer
}

type MulticallCaller struct {
	contract *bind.BoundContract
}

type MulticallTransactor struct {
	contract *bind.BoundContract
}

type MulticallFilterer struct {
	contract *bind.BoundContract
}

type MulticallSession struct {
	Contract     *Multicall
	CallOpts     bind.CallOpts
	TransactOpts bind.TransactOpts
}

type MulticallCallerSession struct {
	Contract *MulticallCaller
	CallOpts bind.CallOpts
}

type MulticallTransactorSession struct {
	Contract     *MulticallTransactor
	TransactOpts bind.TransactOpts
}

type MulticallRaw struct {
	Contract *Multicall
}

type MulticallCallerRaw struct {
	Contract *MulticallCaller
}

type MulticallTransactorRaw struct {
	Contract *MulticallTransactor
}

func NewMulticall(address common.Address, backend bind.ContractBackend) (*Multicall, error) {
	abi, err := abi.JSON(strings.NewReader(MulticallABI))
	if err != nil {
		return nil, err
	}
	contract, err := bindMulticall(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Multicall{address: address, abi: abi, MulticallCaller: MulticallCaller{contract: contract}, MulticallTransactor: MulticallTransactor{contract: contract}, MulticallFilterer: MulticallFilterer{contract: contract}}, nil
}

func NewMulticallCaller(address common.Address, caller bind.ContractCaller) (*MulticallCaller, error) {
	contract, err := bindMulticall(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &MulticallCaller{contract: contract}, nil
}

func NewMulticallTransactor(address common.Address, transactor bind.ContractTransactor) (*MulticallTransactor, error) {
	contract, err := bindMulticall(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &MulticallTransactor{contract: contract}, nil
}

func NewMulticallFilterer(address common.Address, filterer bind.ContractFilterer) (*MulticallFilterer, error) {
	contract, err := bindMulticall(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &MulticallFilterer{contract: contract}, nil
}

func bindMulticall(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(MulticallABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

func (_Multicall *MulticallRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Multicall.Contract.MulticallCaller.contract.Call(opts, result, method, params...)
}

func (_Multicall *MulticallRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Multicall.Contract.MulticallTransactor.contract.Transfer(opts)
}

func (_Multicall *MulticallRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Multicall.Contract.MulticallTransactor.contract.Transact(opts, method, params...)
}

func (_Multicall *MulticallCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Multicall.Contract.contract.Call(opts, result, method, params...)
}

func (_Multicall *MulticallTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Multicall.Contract.contract.Transfer(opts)
}

func (_Multicall *MulticallTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Multicall.Contract.contract.Transact(opts, method, params...)
}

func (_Multicall *MulticallCaller) GetBlockHash(opts *bind.CallOpts, blockNumber *big.Int) ([32]byte, error) {
	var out []interface{}
	err := _Multicall.contract.Call(opts, &out, "getBlockHash", blockNumber)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

func (_Multicall *MulticallSession) GetBlockHash(blockNumber *big.Int) ([32]byte, error) {
	return _Multicall.Contract.GetBlockHash(&_Multicall.CallOpts, blockNumber)
}

func (_Multicall *MulticallCallerSession) GetBlockHash(blockNumber *big.Int) ([32]byte, error) {
	return _Multicall.Contract.GetBlockHash(&_Multicall.CallOpts, blockNumber)
}

func (_Multicall *MulticallCaller) GetCurrentBlockCoinbase(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Multicall.contract.Call(opts, &out, "getCurrentBlockCoinbase")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_Multicall *MulticallSession) GetCurrentBlockCoinbase() (common.Address, error) {
	return _Multicall.Contract.GetCurrentBlockCoinbase(&_Multicall.CallOpts)
}

func (_Multicall *MulticallCallerSession) GetCurrentBlockCoinbase() (common.Address, error) {
	return _Multicall.Contract.GetCurrentBlockCoinbase(&_Multicall.CallOpts)
}

func (_Multicall *MulticallCaller) GetCurrentBlockDifficulty(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Multicall.contract.Call(opts, &out, "getCurrentBlockDifficulty")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_Multicall *MulticallSession) GetCurrentBlockDifficulty() (*big.Int, error) {
	return _Multicall.Contract.GetCurrentBlockDifficulty(&_Multicall.CallOpts)
}

func (_Multicall *MulticallCallerSession) GetCurrentBlockDifficulty() (*big.Int, error) {
	return _Multicall.Contract.GetCurrentBlockDifficulty(&_Multicall.CallOpts)
}

func (_Multicall *MulticallCaller) GetCurrentBlockGasLimit(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Multicall.contract.Call(opts, &out, "getCurrentBlockGasLimit")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_Multicall *MulticallSession) GetCurrentBlockGasLimit() (*big.Int, error) {
	return _Multicall.Contract.GetCurrentBlockGasLimit(&_Multicall.CallOpts)
}

func (_Multicall *MulticallCallerSession) GetCurrentBlockGasLimit() (*big.Int, error) {
	return _Multicall.Contract.GetCurrentBlockGasLimit(&_Multicall.CallOpts)
}

func (_Multicall *MulticallCaller) GetCurrentBlockTimestamp(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Multicall.contract.Call(opts, &out, "getCurrentBlockTimestamp")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_Multicall *MulticallSession) GetCurrentBlockTimestamp() (*big.Int, error) {
	return _Multicall.Contract.GetCurrentBlockTimestamp(&_Multicall.CallOpts)
}

func (_Multicall *MulticallCallerSession) GetCurrentBlockTimestamp() (*big.Int, error) {
	return _Multicall.Contract.GetCurrentBlockTimestamp(&_Multicall.CallOpts)
}

func (_Multicall *MulticallCaller) GetEthBalance(opts *bind.CallOpts, addr common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Multicall.contract.Call(opts, &out, "getEthBalance", addr)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_Multicall *MulticallSession) GetEthBalance(addr common.Address) (*big.Int, error) {
	return _Multicall.Contract.GetEthBalance(&_Multicall.CallOpts, addr)
}

func (_Multicall *MulticallCallerSession) GetEthBalance(addr common.Address) (*big.Int, error) {
	return _Multicall.Contract.GetEthBalance(&_Multicall.CallOpts, addr)
}

func (_Multicall *MulticallCaller) GetLastBlockHash(opts *bind.CallOpts) ([32]byte, error) {
	var out []interface{}
	err := _Multicall.contract.Call(opts, &out, "getLastBlockHash")

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

func (_Multicall *MulticallSession) GetLastBlockHash() ([32]byte, error) {
	return _Multicall.Contract.GetLastBlockHash(&_Multicall.CallOpts)
}

func (_Multicall *MulticallCallerSession) GetLastBlockHash() ([32]byte, error) {
	return _Multicall.Contract.GetLastBlockHash(&_Multicall.CallOpts)
}

func (_Multicall *MulticallCaller) IsContract(opts *bind.CallOpts, addr common.Address) (bool, error) {
	var out []interface{}
	err := _Multicall.contract.Call(opts, &out, "isContract", addr)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

func (_Multicall *MulticallSession) IsContract(addr common.Address) (bool, error) {
	return _Multicall.Contract.IsContract(&_Multicall.CallOpts, addr)
}

func (_Multicall *MulticallCallerSession) IsContract(addr common.Address) (bool, error) {
	return _Multicall.Contract.IsContract(&_Multicall.CallOpts, addr)
}

func (_Multicall *MulticallTransactor) Aggregate(opts *bind.TransactOpts, calls []MulticallCall) (*types.Transaction, error) {
	return _Multicall.contract.Transact(opts, "aggregate", calls)
}

func (_Multicall *MulticallSession) Aggregate(calls []MulticallCall) (*types.Transaction, error) {
	return _Multicall.Contract.Aggregate(&_Multicall.TransactOpts, calls)
}

func (_Multicall *MulticallTransactorSession) Aggregate(calls []MulticallCall) (*types.Transaction, error) {
	return _Multicall.Contract.Aggregate(&_Multicall.TransactOpts, calls)
}

func (_Multicall *Multicall) Address() common.Address {
	return _Multicall.address
}

type MulticallInterface interface {
	GetBlockHash(opts *bind.CallOpts, blockNumber *big.Int) ([32]byte, error)

	GetCurrentBlockCoinbase(opts *bind.CallOpts) (common.Address, error)

	GetCurrentBlockDifficulty(opts *bind.CallOpts) (*big.Int, error)

	GetCurrentBlockGasLimit(opts *bind.CallOpts) (*big.Int, error)

	GetCurrentBlockTimestamp(opts *bind.CallOpts) (*big.Int, error)

	GetEthBalance(opts *bind.CallOpts, addr common.Address) (*big.Int, error)

	GetLastBlockHash(opts *bind.CallOpts) ([32]byte, error)

	IsContract(opts *bind.CallOpts, addr common.Address) (bool, error)

	Aggregate(opts *bind.TransactOpts, calls []MulticallCall) (*types.Transaction, error)

	Address() common.Address
}
