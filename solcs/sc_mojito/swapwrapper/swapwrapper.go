// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package swapwrapper

import (
	"errors"
	"fmt"
	"math/big"
	"strings"

	"gitee.com/tfxq/swap-go-sdk/internal/gethwrappers/generated"
	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

var SwapWrapperMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"OwnershipTransferRequested\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"tokenIn\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amountIn\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"tokenOut\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amountOut\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"TokenSwitched\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"trader\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"traderState\",\"type\":\"bool\"}],\"name\":\"TraderSet\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"acceptOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"claimRewardAndLPP\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractIERC20\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"getERC20BalanceAndLPP\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getRewardAndLPP\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_router\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_mining\",\"type\":\"address\"}],\"name\":\"initialize\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"s_mining\",\"outputs\":[{\"internalType\":\"contractISwapMining\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"s_router\",\"outputs\":[{\"internalType\":\"contractIMojitoRouter\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"s_traderList\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractIERC20\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"safeApproveAndLPP\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractIERC20\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"safeTransferAndLPP\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contractIERC20\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"safeTransferFromAndLPP\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"trader\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"state\",\"type\":\"bool\"}],\"name\":\"setTraderAndLPP\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amountOut\",\"type\":\"uint256\"},{\"internalType\":\"address[]\",\"name\":\"path\",\"type\":\"address[]\"},{\"internalType\":\"uint256\",\"name\":\"amountInMax\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"swapETHForExactTokensAndLPP\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address[]\",\"name\":\"path\",\"type\":\"address[]\"},{\"internalType\":\"uint256\",\"name\":\"amountOutMin\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"swapExactETHForTokensAndLPP\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amountIn\",\"type\":\"uint256\"},{\"internalType\":\"address[]\",\"name\":\"path\",\"type\":\"address[]\"},{\"internalType\":\"uint256\",\"name\":\"amountOutMin\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"swapExactTokensForETHAndLPP\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amountIn\",\"type\":\"uint256\"},{\"internalType\":\"address[]\",\"name\":\"path\",\"type\":\"address[]\"},{\"internalType\":\"uint256\",\"name\":\"amountOutMin\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"swapExactTokensForTokensAndLPP\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amountOut\",\"type\":\"uint256\"},{\"internalType\":\"address[]\",\"name\":\"path\",\"type\":\"address[]\"},{\"internalType\":\"uint256\",\"name\":\"amountInMax\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"swapTokensForExactETHAndLPP\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amountOut\",\"type\":\"uint256\"},{\"internalType\":\"address[]\",\"name\":\"path\",\"type\":\"address[]\"},{\"internalType\":\"uint256\",\"name\":\"amountInMax\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"swapTokensForExactTokensAndLPP\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"withdrawETHAndLPP\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
	Bin: "0x608060405234801561001057600080fd5b50600080546001600160a01b03191633179055612be6806100326000396000f3fe60806040526004361061016e5760003560e01c80637a3266fa116100cb578063e16e632c1161007f578063ef3b7fa111610059578063ef3b7fa1146103a9578063f2fde38b146103be578063f6481604146103de57610175565b8063e16e632c1461035f578063e886329114610374578063ebd6ebb61461038957610175565b8063892029e7116100b0578063892029e71461030a5780638cc250221461031d5780638da5cb5b1461033d57610175565b80637a3266fa146102ca5780638484263e146102ea57610175565b80634d209513116101225780635b75d4d8116101075780635b75d4d814610275578063671d2257146102a257806379ba5097146102b557610175565b80634d2095131461022857806356fdb86f1461024857610175565b8063156aff7b11610153578063156aff7b146101c85780633f6f7cfc146101e8578063485cc9551461020857610175565b80630c6d00d21461017a578063106b54b81461019157610175565b3661017557005b600080fd5b34801561018657600080fd5b5061018f6103fe565b005b34801561019d57600080fd5b506101b16101ac366004612477565b6104c2565b6040516101bf929190612a64565b60405180910390f35b3480156101d457600080fd5b506101b16101e3366004612477565b61081f565b3480156101f457600080fd5b5061018f610203366004612407565b610c64565b34801561021457600080fd5b5061018f61022336600461220d565b610cc1565b34801561023457600080fd5b5061018f6102433660046123b7565b610dc4565b34801561025457600080fd5b506102686102633660046121f1565b610e1d565b6040516101bf9190612655565b34801561028157600080fd5b506102956102903660046123a5565b610e32565b6040516101bf91906129f4565b6101b16102b0366004612272565b610ede565b3480156102c157600080fd5b5061018f611185565b3480156102d657600080fd5b5061018f6102e5366004612245565b611238565b3480156102f657600080fd5b506101b1610305366004612477565b6112fc565b6101b1610318366004612477565b611529565b34801561032957600080fd5b5061018f610338366004612447565b611970565b34801561034957600080fd5b506103526119d8565b6040516101bf9190612551565b34801561036b57600080fd5b506103526119f4565b34801561038057600080fd5b50610295611a10565b34801561039557600080fd5b5061018f6103a4366004612407565b611b93565b3480156103b557600080fd5b50610352611beb565b3480156103ca57600080fd5b5061018f6103d93660046121f1565b611c07565b3480156103ea57600080fd5b506101b16103f9366004612477565b611cea565b60005473ffffffffffffffffffffffffffffffffffffffff16331461043e5760405162461bcd60e51b8152600401610435906126ff565b60405180910390fd5b600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16633ccfd60b6040518163ffffffff1660e01b8152600401600060405180830381600087803b1580156104a857600080fd5b505af11580156104bc573d6000803e3d6000fd5b50505050565b33600090815260046020526040812054819060ff166104f35760405162461bcd60e51b81526004016104359061285e565b6000871180156105035750600185115b61051f5760405162461bcd60e51b81526004016104359061276d565b6105793330898989600081811061054657634e487b7160e01b600052603260045260246000fd5b905060200201602081019061055b91906121f1565b73ffffffffffffffffffffffffffffffffffffffff16929190611e0c565b6002546105e89073ffffffffffffffffffffffffffffffffffffffff168888886000816105b657634e487b7160e01b600052603260045260246000fd5b90506020020160208101906105cb91906121f1565b73ffffffffffffffffffffffffffffffffffffffff169190611e91565b6002546040517f18cbafe500000000000000000000000000000000000000000000000000000000815260009173ffffffffffffffffffffffffffffffffffffffff16906318cbafe590610649908b9089908c908c908b904290600401612a72565b600060405180830381600087803b15801561066357600080fd5b505af1158015610677573d6000803e3d6000fd5b505050506040513d6000823e601f3d908101601f1916820160405261069f91908101906122cf565b90507facbe630c79ea05c0e9f63dec03783d986a1a5970a9f10269512f1b7a3f31a57a878760008181106106e357634e487b7160e01b600052603260045260246000fd5b90506020020160208101906106f891906121f1565b8260008151811061071957634e487b7160e01b600052603260045260246000fd5b60209081029190910101518989610731600182612ad5565b81811061074e57634e487b7160e01b600052603260045260246000fd5b905060200201602081019061076391906121f1565b84600186516107729190612ad5565b8151811061079057634e487b7160e01b600052603260045260246000fd5b6020026020010151886040516107aa959493929190612618565b60405180910390a1806000815181106107d357634e487b7160e01b600052603260045260246000fd5b602002602001015181600183516107ea9190612ad5565b8151811061080857634e487b7160e01b600052603260045260246000fd5b602002602001015192509250509550959350505050565b33600090815260046020526040812054819060ff166108505760405162461bcd60e51b81526004016104359061285e565b6000871180156108605750600185115b61087c5760405162461bcd60e51b81526004016104359061276d565b6002546040517f1f00ca7400000000000000000000000000000000000000000000000000000000815260009173ffffffffffffffffffffffffffffffffffffffff1690631f00ca74906108d7908b908b908b906004016129fd565b60006040518083038186803b1580156108ef57600080fd5b505afa158015610903573d6000803e3d6000fd5b505050506040513d6000823e601f3d908101601f1916820160405261092b91908101906122cf565b9050848160008151811061094f57634e487b7160e01b600052603260045260246000fd5b602002602001015111156109755760405162461bcd60e51b815260040161043590612736565b6109c433308360008151811061099b57634e487b7160e01b600052603260045260246000fd5b60200260200101518a8a600081811061054657634e487b7160e01b600052603260045260246000fd5b6002548151610a2c9173ffffffffffffffffffffffffffffffffffffffff16908390600090610a0357634e487b7160e01b600052603260045260246000fd5b6020026020010151898960008181106105b657634e487b7160e01b600052603260045260246000fd5b6002546040517f8803dbee00000000000000000000000000000000000000000000000000000000815260009173ffffffffffffffffffffffffffffffffffffffff1690638803dbee90610a8d908c908a908d908d908c904290600401612a72565b600060405180830381600087803b158015610aa757600080fd5b505af1158015610abb573d6000803e3d6000fd5b505050506040513d6000823e601f3d908101601f19168201604052610ae391908101906122cf565b90507facbe630c79ea05c0e9f63dec03783d986a1a5970a9f10269512f1b7a3f31a57a88886000818110610b2757634e487b7160e01b600052603260045260246000fd5b9050602002016020810190610b3c91906121f1565b82600081518110610b5d57634e487b7160e01b600052603260045260246000fd5b60209081029190910101518a8a610b75600182612ad5565b818110610b9257634e487b7160e01b600052603260045260246000fd5b9050602002016020810190610ba791906121f1565b8460018651610bb69190612ad5565b81518110610bd457634e487b7160e01b600052603260045260246000fd5b602002602001015189604051610bee959493929190612618565b60405180910390a180600081518110610c1757634e487b7160e01b600052603260045260246000fd5b60200260200101518160018351610c2e9190612ad5565b81518110610c4c57634e487b7160e01b600052603260045260246000fd5b60200260200101519350935050509550959350505050565b60005473ffffffffffffffffffffffffffffffffffffffff163314610c9b5760405162461bcd60e51b8152600401610435906126ff565b610cbc73ffffffffffffffffffffffffffffffffffffffff84168383611e91565b505050565b60005473ffffffffffffffffffffffffffffffffffffffff163314610cf85760405162461bcd60e51b8152600401610435906126ff565b60055460ff1615610d1b5760405162461bcd60e51b815260040161043590612801565b73ffffffffffffffffffffffffffffffffffffffff821615801590610d55575073ffffffffffffffffffffffffffffffffffffffff811615155b610d715760405162461bcd60e51b81526004016104359061276d565b6002805473ffffffffffffffffffffffffffffffffffffffff9384167fffffffffffffffffffffffff00000000000000000000000000000000000000009182161790915560038054929093169116179055565b60005473ffffffffffffffffffffffffffffffffffffffff163314610dfb5760405162461bcd60e51b8152600401610435906126ff565b6104bc73ffffffffffffffffffffffffffffffffffffffff8516848484611e0c565b60046020526000908152604090205460ff1681565b6040517f70a0823100000000000000000000000000000000000000000000000000000000815260009073ffffffffffffffffffffffffffffffffffffffff8416906370a0823190610e87908590600401612551565b60206040518083038186803b158015610e9f57600080fd5b505afa158015610eb3573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610ed7919061245f565b9392505050565b33600090815260046020526040812054819060ff16610f0f5760405162461bcd60e51b81526004016104359061285e565b60018511610f2f5760405162461bcd60e51b81526004016104359061276d565b60003411610f4f5760405162461bcd60e51b815260040161043590612895565b6002546040517f7ff36ab500000000000000000000000000000000000000000000000000000000815260009173ffffffffffffffffffffffffffffffffffffffff1690637ff36ab5903490610fb09089908c908c908b904290600401612a20565b6000604051808303818588803b158015610fc957600080fd5b505af1158015610fdd573d6000803e3d6000fd5b50505050506040513d6000823e601f3d908101601f1916820160405261100691908101906122cf565b90507facbe630c79ea05c0e9f63dec03783d986a1a5970a9f10269512f1b7a3f31a57a8787600081811061104a57634e487b7160e01b600052603260045260246000fd5b905060200201602081019061105f91906121f1565b8260008151811061108057634e487b7160e01b600052603260045260246000fd5b60209081029190910101518989611098600182612ad5565b8181106110b557634e487b7160e01b600052603260045260246000fd5b90506020020160208101906110ca91906121f1565b84600186516110d99190612ad5565b815181106110f757634e487b7160e01b600052603260045260246000fd5b602002602001015188604051611111959493929190612618565b60405180910390a18060008151811061113a57634e487b7160e01b600052603260045260246000fd5b602002602001015181600183516111519190612ad5565b8151811061116f57634e487b7160e01b600052603260045260246000fd5b6020026020010151925092505094509492505050565b60015473ffffffffffffffffffffffffffffffffffffffff1633146111bc5760405162461bcd60e51b8152600401610435906126c8565b60008054337fffffffffffffffffffffffff00000000000000000000000000000000000000008083168217845560018054909116905560405173ffffffffffffffffffffffffffffffffffffffff90921692909183917f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e091a350565b60005473ffffffffffffffffffffffffffffffffffffffff16331461126f5760405162461bcd60e51b8152600401610435906126ff565b73ffffffffffffffffffffffffffffffffffffffff82166000908152600460205260409081902080547fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0016831515179055517fc042e64e64db64529dc6e20916a47711d598cadb3f399d5999261d46eff744ea906112f090849084906125ca565b60405180910390a15050565b33600090815260046020526040812054819060ff1661132d5760405162461bcd60e51b81526004016104359061285e565b60008711801561133d5750600185115b6113595760405162461bcd60e51b81526004016104359061276d565b6002546040517f1f00ca7400000000000000000000000000000000000000000000000000000000815260009173ffffffffffffffffffffffffffffffffffffffff1690631f00ca74906113b4908b908b908b906004016129fd565b60006040518083038186803b1580156113cc57600080fd5b505afa1580156113e0573d6000803e3d6000fd5b505050506040513d6000823e601f3d908101601f1916820160405261140891908101906122cf565b9050848160008151811061142c57634e487b7160e01b600052603260045260246000fd5b602002602001015111156114525760405162461bcd60e51b815260040161043590612736565b61147833308360008151811061099b57634e487b7160e01b600052603260045260246000fd5b60025481516114b79173ffffffffffffffffffffffffffffffffffffffff16908390600090610a0357634e487b7160e01b600052603260045260246000fd5b600254815160009173ffffffffffffffffffffffffffffffffffffffff1690634a25d94a908b90859085906114fc57634e487b7160e01b600052603260045260246000fd5b60200260200101518b8b8a426040518763ffffffff1660e01b8152600401610a8d96959493929190612a72565b33600090815260046020526040812054819060ff1661155a5760405162461bcd60e51b81526004016104359061285e565b60008711801561156a5750600185115b6115865760405162461bcd60e51b81526004016104359061276d565b6002546040517f1f00ca7400000000000000000000000000000000000000000000000000000000815260009173ffffffffffffffffffffffffffffffffffffffff1690631f00ca74906115e1908b908b908b906004016129fd565b60006040518083038186803b1580156115f957600080fd5b505afa15801561160d573d6000803e3d6000fd5b505050506040513d6000823e601f3d908101601f1916820160405261163591908101906122cf565b9050848160008151811061165957634e487b7160e01b600052603260045260246000fd5b6020026020010151111561167f5760405162461bcd60e51b815260040161043590612736565b806000815181106116a057634e487b7160e01b600052603260045260246000fd5b60200260200101513410156116c75760405162461bcd60e51b815260040161043590612693565b600254815160009173ffffffffffffffffffffffffffffffffffffffff169063fb3bdb41908490849061170a57634e487b7160e01b600052603260045260246000fd5b60200260200101518b8b8b33426040518763ffffffff1660e01b8152600401611737959493929190612a20565b6000604051808303818588803b15801561175057600080fd5b505af1158015611764573d6000803e3d6000fd5b50505050506040513d6000823e601f3d908101601f1916820160405261178d91908101906122cf565b90507facbe630c79ea05c0e9f63dec03783d986a1a5970a9f10269512f1b7a3f31a57a888860008181106117d157634e487b7160e01b600052603260045260246000fd5b90506020020160208101906117e691906121f1565b8260008151811061180757634e487b7160e01b600052603260045260246000fd5b60209081029190910101518a8a61181f600182612ad5565b81811061183c57634e487b7160e01b600052603260045260246000fd5b905060200201602081019061185191906121f1565b84600186516118609190612ad5565b8151811061187e57634e487b7160e01b600052603260045260246000fd5b602002602001015189604051611898959493929190612618565b60405180910390a134816000815181106118c257634e487b7160e01b600052603260045260246000fd5b6020026020010151101561194f573373ffffffffffffffffffffffffffffffffffffffff166108fc6119258360008151811061190e57634e487b7160e01b600052603260045260246000fd5b602002602001015134611f7a90919063ffffffff16565b6040518115909202916000818181858888f1935050505015801561194d573d6000803e3d6000fd5b505b80600081518110610c1757634e487b7160e01b600052603260045260246000fd5b60005473ffffffffffffffffffffffffffffffffffffffff1633146119a75760405162461bcd60e51b8152600401610435906126ff565b604051339082156108fc029083906000818181858888f193505050501580156119d4573d6000803e3d6000fd5b5050565b60005473ffffffffffffffffffffffffffffffffffffffff1690565b60025473ffffffffffffffffffffffffffffffffffffffff1681565b6000806000600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1663081e3eda6040518163ffffffff1660e01b815260040160206040518083038186803b158015611a7d57600080fd5b505afa158015611a91573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190611ab5919061245f565b905060005b81811015611b8b576003546040517f63fb2fe500000000000000000000000000000000000000000000000000000000815260009173ffffffffffffffffffffffffffffffffffffffff16906363fb2fe590611b199085906004016129f4565b60206040518083038186803b158015611b3157600080fd5b505afa158015611b45573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190611b69919061245f565b9050611b758482611f86565b9350508080611b8390612b18565b915050611aba565b509091505090565b60005473ffffffffffffffffffffffffffffffffffffffff163314611bca5760405162461bcd60e51b8152600401610435906126ff565b610cbc73ffffffffffffffffffffffffffffffffffffffff84168383611f92565b60035473ffffffffffffffffffffffffffffffffffffffff1681565b60005473ffffffffffffffffffffffffffffffffffffffff163314611c3e5760405162461bcd60e51b8152600401610435906126ff565b73ffffffffffffffffffffffffffffffffffffffff8116331415611c745760405162461bcd60e51b815260040161043590612903565b600180547fffffffffffffffffffffffff00000000000000000000000000000000000000001673ffffffffffffffffffffffffffffffffffffffff83811691821790925560008054604051929316917fed8889f560326eb138920d842192f0eb3dd22b4f139c87a2c57538e05bae12789190a350565b33600090815260046020526040812054819060ff16611d1b5760405162461bcd60e51b81526004016104359061285e565b600087118015611d2b5750600185115b611d475760405162461bcd60e51b81526004016104359061276d565b611d6e3330898989600081811061054657634e487b7160e01b600052603260045260246000fd5b600254611dab9073ffffffffffffffffffffffffffffffffffffffff168888886000816105b657634e487b7160e01b600052603260045260246000fd5b6002546040517f38ed173900000000000000000000000000000000000000000000000000000000815260009173ffffffffffffffffffffffffffffffffffffffff16906338ed173990610649908b9089908c908c908b904290600401612a72565b6104bc846323b872dd60e01b858585604051602401611e2d93929190612599565b60408051601f198184030181529190526020810180517bffffffffffffffffffffffffffffffffffffffffffffffffffffffff167fffffffff0000000000000000000000000000000000000000000000000000000090931692909217909152611fb1565b801580611f3f57506040517fdd62ed3e00000000000000000000000000000000000000000000000000000000815273ffffffffffffffffffffffffffffffffffffffff84169063dd62ed3e90611eed9030908690600401612572565b60206040518083038186803b158015611f0557600080fd5b505afa158015611f19573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190611f3d919061245f565b155b611f5b5760405162461bcd60e51b815260040161043590612997565b610cbc8363095ea7b360e01b8484604051602401611e2d9291906125f2565b6000610ed78284612ad5565b6000610ed78284612abd565b610cbc8363a9059cbb60e01b8484604051602401611e2d9291906125f2565b6000612013826040518060400160405280602081526020017f5361666545524332303a206c6f772d6c6576656c2063616c6c206661696c65648152508573ffffffffffffffffffffffffffffffffffffffff1661204d9092919063ffffffff16565b805190915015610cbc57808060200190518101906120319190612389565b610cbc5760405162461bcd60e51b81526004016104359061293a565b606061205c8484600085612064565b949350505050565b6060824710156120865760405162461bcd60e51b8152600401610435906127a4565b61208f85612131565b6120ab5760405162461bcd60e51b8152600401610435906128cc565b6000808673ffffffffffffffffffffffffffffffffffffffff1685876040516120d49190612535565b60006040518083038185875af1925050503d8060008114612111576040519150601f19603f3d011682016040523d82523d6000602084013e612116565b606091505b509150915061212682828661216f565b979650505050505050565b6000808273ffffffffffffffffffffffffffffffffffffffff16803b806020016040519081016040528181526000908060200190933c511192915050565b6060831561217e575081610ed7565b82511561218e5782518084602001fd5b8160405162461bcd60e51b81526004016104359190612660565b60008083601f8401126121b9578081fd5b50813567ffffffffffffffff8111156121d0578182fd5b60208301915083602080830285010111156121ea57600080fd5b9250929050565b600060208284031215612202578081fd5b8135610ed781612b7d565b6000806040838503121561221f578081fd5b823561222a81612b7d565b9150602083013561223a81612b7d565b809150509250929050565b60008060408385031215612257578182fd5b823561226281612b7d565b9150602083013561223a81612ba2565b60008060008060608587031215612287578182fd5b843567ffffffffffffffff81111561229d578283fd5b6122a9878288016121a8565b9095509350506020850135915060408501356122c481612b7d565b939692955090935050565b600060208083850312156122e1578182fd5b825167ffffffffffffffff808211156122f8578384fd5b818501915085601f83011261230b578384fd5b81518181111561231d5761231d612b67565b8381026040518582820101818110858211171561233c5761233c612b67565b604052828152858101935084860182860187018a101561235a578788fd5b8795505b8386101561237c57805185526001959095019493860193860161235e565b5098975050505050505050565b60006020828403121561239a578081fd5b8151610ed781612ba2565b6000806040838503121561221f578182fd5b600080600080608085870312156123cc578384fd5b84356123d781612b7d565b935060208501356123e781612b7d565b925060408501356123f781612b7d565b9396929550929360600135925050565b60008060006060848603121561241b578283fd5b833561242681612b7d565b9250602084013561243681612b7d565b929592945050506040919091013590565b600060208284031215612458578081fd5b5035919050565b600060208284031215612470578081fd5b5051919050565b60008060008060006080868803121561248e578081fd5b85359450602086013567ffffffffffffffff8111156124ab578182fd5b6124b7888289016121a8565b9095509350506040860135915060608601356124d281612b7d565b809150509295509295909350565b60008284526020808501945082825b8581101561252a57813561250281612b7d565b73ffffffffffffffffffffffffffffffffffffffff16875295820195908201906001016124ef565b509495945050505050565b60008251612547818460208701612aec565b9190910192915050565b73ffffffffffffffffffffffffffffffffffffffff91909116815260200190565b73ffffffffffffffffffffffffffffffffffffffff92831681529116602082015260400190565b73ffffffffffffffffffffffffffffffffffffffff9384168152919092166020820152604081019190915260600190565b73ffffffffffffffffffffffffffffffffffffffff9290921682521515602082015260400190565b73ffffffffffffffffffffffffffffffffffffffff929092168252602082015260400190565b73ffffffffffffffffffffffffffffffffffffffff9586168152602081019490945291841660408401526060830152909116608082015260a00190565b901515815260200190565b600060208252825180602084015261267f816040850160208701612aec565b601f01601f19169190910160400192915050565b6020808252818101527f53776170577261707065723a2076616c7565206973206e6f7420656e6f756768604082015260600190565b60208082526016908201527f4d7573742062652070726f706f736564206f776e657200000000000000000000604082015260600190565b60208082526016908201527f4f6e6c792063616c6c61626c65206279206f776e657200000000000000000000604082015260600190565b6020808252601f908201527f53776170577261707065723a2065786365656420616d6f756e74496e4d617800604082015260600190565b6020808252601e908201527f53776170577261707065723a20496e76616c696420706172616d657465720000604082015260600190565b60208082526026908201527f416464726573733a20696e73756666696369656e742062616c616e636520666f60408201527f722063616c6c0000000000000000000000000000000000000000000000000000606082015260800190565b6020808252602e908201527f436f6e747261637420696e7374616e63652068617320616c726561647920626560408201527f656e20696e697469616c697a6564000000000000000000000000000000000000606082015260800190565b60208082526017908201527f4f6e6c792063616c6c61626c6520627920747261646572000000000000000000604082015260600190565b60208082526017908201527f53776170577261707065723a2076616c75652069732030000000000000000000604082015260600190565b6020808252601d908201527f416464726573733a2063616c6c20746f206e6f6e2d636f6e7472616374000000604082015260600190565b60208082526017908201527f43616e6e6f74207472616e7366657220746f2073656c66000000000000000000604082015260600190565b6020808252602a908201527f5361666545524332303a204552433230206f7065726174696f6e20646964206e60408201527f6f74207375636365656400000000000000000000000000000000000000000000606082015260800190565b60208082526036908201527f5361666545524332303a20617070726f76652066726f6d206e6f6e2d7a65726f60408201527f20746f206e6f6e2d7a65726f20616c6c6f77616e636500000000000000000000606082015260800190565b90815260200190565b600084825260406020830152612a176040830184866124e0565b95945050505050565b600086825260806020830152612a3a6080830186886124e0565b73ffffffffffffffffffffffffffffffffffffffff94909416604083015250606001529392505050565b918252602082015260400190565b600087825286602083015260a06040830152612a9260a0830186886124e0565b73ffffffffffffffffffffffffffffffffffffffff9490941660608301525060800152949350505050565b60008219821115612ad057612ad0612b51565b500190565b600082821015612ae757612ae7612b51565b500390565b60005b83811015612b07578181015183820152602001612aef565b838111156104bc5750506000910152565b60007fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff821415612b4a57612b4a612b51565b5060010190565b634e487b7160e01b600052601160045260246000fd5b634e487b7160e01b600052604160045260246000fd5b73ffffffffffffffffffffffffffffffffffffffff81168114612b9f57600080fd5b50565b8015158114612b9f57600080fdfea2646970667358221220c732a14bdd66144d6b549154aeaa2dee8ffda012aa5d5a7e0152ecd2888defe464736f6c63430008000033",
}

var SwapWrapperABI = SwapWrapperMetaData.ABI

var SwapWrapperBin = SwapWrapperMetaData.Bin

func DeploySwapWrapper(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *SwapWrapper, error) {
	parsed, err := SwapWrapperMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(SwapWrapperBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &SwapWrapper{SwapWrapperCaller: SwapWrapperCaller{contract: contract}, SwapWrapperTransactor: SwapWrapperTransactor{contract: contract}, SwapWrapperFilterer: SwapWrapperFilterer{contract: contract}}, nil
}

type SwapWrapper struct {
	address common.Address
	abi     abi.ABI
	SwapWrapperCaller
	SwapWrapperTransactor
	SwapWrapperFilterer
}

type SwapWrapperCaller struct {
	contract *bind.BoundContract
}

type SwapWrapperTransactor struct {
	contract *bind.BoundContract
}

type SwapWrapperFilterer struct {
	contract *bind.BoundContract
}

type SwapWrapperSession struct {
	Contract     *SwapWrapper
	CallOpts     bind.CallOpts
	TransactOpts bind.TransactOpts
}

type SwapWrapperCallerSession struct {
	Contract *SwapWrapperCaller
	CallOpts bind.CallOpts
}

type SwapWrapperTransactorSession struct {
	Contract     *SwapWrapperTransactor
	TransactOpts bind.TransactOpts
}

type SwapWrapperRaw struct {
	Contract *SwapWrapper
}

type SwapWrapperCallerRaw struct {
	Contract *SwapWrapperCaller
}

type SwapWrapperTransactorRaw struct {
	Contract *SwapWrapperTransactor
}

func NewSwapWrapper(address common.Address, backend bind.ContractBackend) (*SwapWrapper, error) {
	abi, err := abi.JSON(strings.NewReader(SwapWrapperABI))
	if err != nil {
		return nil, err
	}
	contract, err := bindSwapWrapper(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &SwapWrapper{address: address, abi: abi, SwapWrapperCaller: SwapWrapperCaller{contract: contract}, SwapWrapperTransactor: SwapWrapperTransactor{contract: contract}, SwapWrapperFilterer: SwapWrapperFilterer{contract: contract}}, nil
}

func NewSwapWrapperCaller(address common.Address, caller bind.ContractCaller) (*SwapWrapperCaller, error) {
	contract, err := bindSwapWrapper(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &SwapWrapperCaller{contract: contract}, nil
}

func NewSwapWrapperTransactor(address common.Address, transactor bind.ContractTransactor) (*SwapWrapperTransactor, error) {
	contract, err := bindSwapWrapper(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &SwapWrapperTransactor{contract: contract}, nil
}

func NewSwapWrapperFilterer(address common.Address, filterer bind.ContractFilterer) (*SwapWrapperFilterer, error) {
	contract, err := bindSwapWrapper(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &SwapWrapperFilterer{contract: contract}, nil
}

func bindSwapWrapper(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(SwapWrapperABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

func (_SwapWrapper *SwapWrapperRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SwapWrapper.Contract.SwapWrapperCaller.contract.Call(opts, result, method, params...)
}

func (_SwapWrapper *SwapWrapperRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapWrapperTransactor.contract.Transfer(opts)
}

func (_SwapWrapper *SwapWrapperRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapWrapperTransactor.contract.Transact(opts, method, params...)
}

func (_SwapWrapper *SwapWrapperCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SwapWrapper.Contract.contract.Call(opts, result, method, params...)
}

func (_SwapWrapper *SwapWrapperTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SwapWrapper.Contract.contract.Transfer(opts)
}

func (_SwapWrapper *SwapWrapperTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SwapWrapper.Contract.contract.Transact(opts, method, params...)
}

func (_SwapWrapper *SwapWrapperCaller) GetERC20BalanceAndLPP(opts *bind.CallOpts, token common.Address, account common.Address) (*big.Int, error) {
	var out []interface{}
	err := _SwapWrapper.contract.Call(opts, &out, "getERC20BalanceAndLPP", token, account)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_SwapWrapper *SwapWrapperSession) GetERC20BalanceAndLPP(token common.Address, account common.Address) (*big.Int, error) {
	return _SwapWrapper.Contract.GetERC20BalanceAndLPP(&_SwapWrapper.CallOpts, token, account)
}

func (_SwapWrapper *SwapWrapperCallerSession) GetERC20BalanceAndLPP(token common.Address, account common.Address) (*big.Int, error) {
	return _SwapWrapper.Contract.GetERC20BalanceAndLPP(&_SwapWrapper.CallOpts, token, account)
}

func (_SwapWrapper *SwapWrapperCaller) GetRewardAndLPP(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _SwapWrapper.contract.Call(opts, &out, "getRewardAndLPP")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_SwapWrapper *SwapWrapperSession) GetRewardAndLPP() (*big.Int, error) {
	return _SwapWrapper.Contract.GetRewardAndLPP(&_SwapWrapper.CallOpts)
}

func (_SwapWrapper *SwapWrapperCallerSession) GetRewardAndLPP() (*big.Int, error) {
	return _SwapWrapper.Contract.GetRewardAndLPP(&_SwapWrapper.CallOpts)
}

func (_SwapWrapper *SwapWrapperCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _SwapWrapper.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_SwapWrapper *SwapWrapperSession) Owner() (common.Address, error) {
	return _SwapWrapper.Contract.Owner(&_SwapWrapper.CallOpts)
}

func (_SwapWrapper *SwapWrapperCallerSession) Owner() (common.Address, error) {
	return _SwapWrapper.Contract.Owner(&_SwapWrapper.CallOpts)
}

func (_SwapWrapper *SwapWrapperCaller) SMining(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _SwapWrapper.contract.Call(opts, &out, "s_mining")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_SwapWrapper *SwapWrapperSession) SMining() (common.Address, error) {
	return _SwapWrapper.Contract.SMining(&_SwapWrapper.CallOpts)
}

func (_SwapWrapper *SwapWrapperCallerSession) SMining() (common.Address, error) {
	return _SwapWrapper.Contract.SMining(&_SwapWrapper.CallOpts)
}

func (_SwapWrapper *SwapWrapperCaller) SRouter(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _SwapWrapper.contract.Call(opts, &out, "s_router")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_SwapWrapper *SwapWrapperSession) SRouter() (common.Address, error) {
	return _SwapWrapper.Contract.SRouter(&_SwapWrapper.CallOpts)
}

func (_SwapWrapper *SwapWrapperCallerSession) SRouter() (common.Address, error) {
	return _SwapWrapper.Contract.SRouter(&_SwapWrapper.CallOpts)
}

func (_SwapWrapper *SwapWrapperCaller) STraderList(opts *bind.CallOpts, arg0 common.Address) (bool, error) {
	var out []interface{}
	err := _SwapWrapper.contract.Call(opts, &out, "s_traderList", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

func (_SwapWrapper *SwapWrapperSession) STraderList(arg0 common.Address) (bool, error) {
	return _SwapWrapper.Contract.STraderList(&_SwapWrapper.CallOpts, arg0)
}

func (_SwapWrapper *SwapWrapperCallerSession) STraderList(arg0 common.Address) (bool, error) {
	return _SwapWrapper.Contract.STraderList(&_SwapWrapper.CallOpts, arg0)
}

func (_SwapWrapper *SwapWrapperTransactor) AcceptOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "acceptOwnership")
}

func (_SwapWrapper *SwapWrapperSession) AcceptOwnership() (*types.Transaction, error) {
	return _SwapWrapper.Contract.AcceptOwnership(&_SwapWrapper.TransactOpts)
}

func (_SwapWrapper *SwapWrapperTransactorSession) AcceptOwnership() (*types.Transaction, error) {
	return _SwapWrapper.Contract.AcceptOwnership(&_SwapWrapper.TransactOpts)
}

func (_SwapWrapper *SwapWrapperTransactor) ClaimRewardAndLPP(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "claimRewardAndLPP")
}

func (_SwapWrapper *SwapWrapperSession) ClaimRewardAndLPP() (*types.Transaction, error) {
	return _SwapWrapper.Contract.ClaimRewardAndLPP(&_SwapWrapper.TransactOpts)
}

func (_SwapWrapper *SwapWrapperTransactorSession) ClaimRewardAndLPP() (*types.Transaction, error) {
	return _SwapWrapper.Contract.ClaimRewardAndLPP(&_SwapWrapper.TransactOpts)
}

func (_SwapWrapper *SwapWrapperTransactor) Initialize(opts *bind.TransactOpts, _router common.Address, _mining common.Address) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "initialize", _router, _mining)
}

func (_SwapWrapper *SwapWrapperSession) Initialize(_router common.Address, _mining common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.Initialize(&_SwapWrapper.TransactOpts, _router, _mining)
}

func (_SwapWrapper *SwapWrapperTransactorSession) Initialize(_router common.Address, _mining common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.Initialize(&_SwapWrapper.TransactOpts, _router, _mining)
}

func (_SwapWrapper *SwapWrapperTransactor) SafeApproveAndLPP(opts *bind.TransactOpts, token common.Address, spender common.Address, value *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "safeApproveAndLPP", token, spender, value)
}

func (_SwapWrapper *SwapWrapperSession) SafeApproveAndLPP(token common.Address, spender common.Address, value *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SafeApproveAndLPP(&_SwapWrapper.TransactOpts, token, spender, value)
}

func (_SwapWrapper *SwapWrapperTransactorSession) SafeApproveAndLPP(token common.Address, spender common.Address, value *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SafeApproveAndLPP(&_SwapWrapper.TransactOpts, token, spender, value)
}

func (_SwapWrapper *SwapWrapperTransactor) SafeTransferAndLPP(opts *bind.TransactOpts, token common.Address, to common.Address, value *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "safeTransferAndLPP", token, to, value)
}

func (_SwapWrapper *SwapWrapperSession) SafeTransferAndLPP(token common.Address, to common.Address, value *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SafeTransferAndLPP(&_SwapWrapper.TransactOpts, token, to, value)
}

func (_SwapWrapper *SwapWrapperTransactorSession) SafeTransferAndLPP(token common.Address, to common.Address, value *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SafeTransferAndLPP(&_SwapWrapper.TransactOpts, token, to, value)
}

func (_SwapWrapper *SwapWrapperTransactor) SafeTransferFromAndLPP(opts *bind.TransactOpts, token common.Address, from common.Address, to common.Address, value *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "safeTransferFromAndLPP", token, from, to, value)
}

func (_SwapWrapper *SwapWrapperSession) SafeTransferFromAndLPP(token common.Address, from common.Address, to common.Address, value *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SafeTransferFromAndLPP(&_SwapWrapper.TransactOpts, token, from, to, value)
}

func (_SwapWrapper *SwapWrapperTransactorSession) SafeTransferFromAndLPP(token common.Address, from common.Address, to common.Address, value *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SafeTransferFromAndLPP(&_SwapWrapper.TransactOpts, token, from, to, value)
}

func (_SwapWrapper *SwapWrapperTransactor) SetTraderAndLPP(opts *bind.TransactOpts, trader common.Address, state bool) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "setTraderAndLPP", trader, state)
}

func (_SwapWrapper *SwapWrapperSession) SetTraderAndLPP(trader common.Address, state bool) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SetTraderAndLPP(&_SwapWrapper.TransactOpts, trader, state)
}

func (_SwapWrapper *SwapWrapperTransactorSession) SetTraderAndLPP(trader common.Address, state bool) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SetTraderAndLPP(&_SwapWrapper.TransactOpts, trader, state)
}

func (_SwapWrapper *SwapWrapperTransactor) SwapETHForExactTokensAndLPP(opts *bind.TransactOpts, amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "swapETHForExactTokensAndLPP", amountOut, path, amountInMax, to)
}

func (_SwapWrapper *SwapWrapperSession) SwapETHForExactTokensAndLPP(amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapETHForExactTokensAndLPP(&_SwapWrapper.TransactOpts, amountOut, path, amountInMax, to)
}

func (_SwapWrapper *SwapWrapperTransactorSession) SwapETHForExactTokensAndLPP(amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapETHForExactTokensAndLPP(&_SwapWrapper.TransactOpts, amountOut, path, amountInMax, to)
}

func (_SwapWrapper *SwapWrapperTransactor) SwapExactETHForTokensAndLPP(opts *bind.TransactOpts, path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "swapExactETHForTokensAndLPP", path, amountOutMin, to)
}

func (_SwapWrapper *SwapWrapperSession) SwapExactETHForTokensAndLPP(path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapExactETHForTokensAndLPP(&_SwapWrapper.TransactOpts, path, amountOutMin, to)
}

func (_SwapWrapper *SwapWrapperTransactorSession) SwapExactETHForTokensAndLPP(path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapExactETHForTokensAndLPP(&_SwapWrapper.TransactOpts, path, amountOutMin, to)
}

func (_SwapWrapper *SwapWrapperTransactor) SwapExactTokensForETHAndLPP(opts *bind.TransactOpts, amountIn *big.Int, path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "swapExactTokensForETHAndLPP", amountIn, path, amountOutMin, to)
}

func (_SwapWrapper *SwapWrapperSession) SwapExactTokensForETHAndLPP(amountIn *big.Int, path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapExactTokensForETHAndLPP(&_SwapWrapper.TransactOpts, amountIn, path, amountOutMin, to)
}

func (_SwapWrapper *SwapWrapperTransactorSession) SwapExactTokensForETHAndLPP(amountIn *big.Int, path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapExactTokensForETHAndLPP(&_SwapWrapper.TransactOpts, amountIn, path, amountOutMin, to)
}

func (_SwapWrapper *SwapWrapperTransactor) SwapExactTokensForTokensAndLPP(opts *bind.TransactOpts, amountIn *big.Int, path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "swapExactTokensForTokensAndLPP", amountIn, path, amountOutMin, to)
}

func (_SwapWrapper *SwapWrapperSession) SwapExactTokensForTokensAndLPP(amountIn *big.Int, path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapExactTokensForTokensAndLPP(&_SwapWrapper.TransactOpts, amountIn, path, amountOutMin, to)
}

func (_SwapWrapper *SwapWrapperTransactorSession) SwapExactTokensForTokensAndLPP(amountIn *big.Int, path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapExactTokensForTokensAndLPP(&_SwapWrapper.TransactOpts, amountIn, path, amountOutMin, to)
}

func (_SwapWrapper *SwapWrapperTransactor) SwapTokensForExactETHAndLPP(opts *bind.TransactOpts, amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "swapTokensForExactETHAndLPP", amountOut, path, amountInMax, to)
}

func (_SwapWrapper *SwapWrapperSession) SwapTokensForExactETHAndLPP(amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapTokensForExactETHAndLPP(&_SwapWrapper.TransactOpts, amountOut, path, amountInMax, to)
}

func (_SwapWrapper *SwapWrapperTransactorSession) SwapTokensForExactETHAndLPP(amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapTokensForExactETHAndLPP(&_SwapWrapper.TransactOpts, amountOut, path, amountInMax, to)
}

func (_SwapWrapper *SwapWrapperTransactor) SwapTokensForExactTokensAndLPP(opts *bind.TransactOpts, amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "swapTokensForExactTokensAndLPP", amountOut, path, amountInMax, to)
}

func (_SwapWrapper *SwapWrapperSession) SwapTokensForExactTokensAndLPP(amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapTokensForExactTokensAndLPP(&_SwapWrapper.TransactOpts, amountOut, path, amountInMax, to)
}

func (_SwapWrapper *SwapWrapperTransactorSession) SwapTokensForExactTokensAndLPP(amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.SwapTokensForExactTokensAndLPP(&_SwapWrapper.TransactOpts, amountOut, path, amountInMax, to)
}

func (_SwapWrapper *SwapWrapperTransactor) TransferOwnership(opts *bind.TransactOpts, to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "transferOwnership", to)
}

func (_SwapWrapper *SwapWrapperSession) TransferOwnership(to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.TransferOwnership(&_SwapWrapper.TransactOpts, to)
}

func (_SwapWrapper *SwapWrapperTransactorSession) TransferOwnership(to common.Address) (*types.Transaction, error) {
	return _SwapWrapper.Contract.TransferOwnership(&_SwapWrapper.TransactOpts, to)
}

func (_SwapWrapper *SwapWrapperTransactor) WithdrawETHAndLPP(opts *bind.TransactOpts, _amount *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.contract.Transact(opts, "withdrawETHAndLPP", _amount)
}

func (_SwapWrapper *SwapWrapperSession) WithdrawETHAndLPP(_amount *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.Contract.WithdrawETHAndLPP(&_SwapWrapper.TransactOpts, _amount)
}

func (_SwapWrapper *SwapWrapperTransactorSession) WithdrawETHAndLPP(_amount *big.Int) (*types.Transaction, error) {
	return _SwapWrapper.Contract.WithdrawETHAndLPP(&_SwapWrapper.TransactOpts, _amount)
}

func (_SwapWrapper *SwapWrapperTransactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SwapWrapper.contract.RawTransact(opts, nil)
}

func (_SwapWrapper *SwapWrapperSession) Receive() (*types.Transaction, error) {
	return _SwapWrapper.Contract.Receive(&_SwapWrapper.TransactOpts)
}

func (_SwapWrapper *SwapWrapperTransactorSession) Receive() (*types.Transaction, error) {
	return _SwapWrapper.Contract.Receive(&_SwapWrapper.TransactOpts)
}

type SwapWrapperOwnershipTransferRequestedIterator struct {
	Event *SwapWrapperOwnershipTransferRequested

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *SwapWrapperOwnershipTransferRequestedIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(SwapWrapperOwnershipTransferRequested)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(SwapWrapperOwnershipTransferRequested)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *SwapWrapperOwnershipTransferRequestedIterator) Error() error {
	return it.fail
}

func (it *SwapWrapperOwnershipTransferRequestedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type SwapWrapperOwnershipTransferRequested struct {
	From common.Address
	To   common.Address
	Raw  types.Log
}

func (_SwapWrapper *SwapWrapperFilterer) FilterOwnershipTransferRequested(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*SwapWrapperOwnershipTransferRequestedIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _SwapWrapper.contract.FilterLogs(opts, "OwnershipTransferRequested", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &SwapWrapperOwnershipTransferRequestedIterator{contract: _SwapWrapper.contract, event: "OwnershipTransferRequested", logs: logs, sub: sub}, nil
}

func (_SwapWrapper *SwapWrapperFilterer) WatchOwnershipTransferRequested(opts *bind.WatchOpts, sink chan<- *SwapWrapperOwnershipTransferRequested, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _SwapWrapper.contract.WatchLogs(opts, "OwnershipTransferRequested", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(SwapWrapperOwnershipTransferRequested)
				if err := _SwapWrapper.contract.UnpackLog(event, "OwnershipTransferRequested", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_SwapWrapper *SwapWrapperFilterer) ParseOwnershipTransferRequested(log types.Log) (*SwapWrapperOwnershipTransferRequested, error) {
	event := new(SwapWrapperOwnershipTransferRequested)
	if err := _SwapWrapper.contract.UnpackLog(event, "OwnershipTransferRequested", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type SwapWrapperOwnershipTransferredIterator struct {
	Event *SwapWrapperOwnershipTransferred

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *SwapWrapperOwnershipTransferredIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(SwapWrapperOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(SwapWrapperOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *SwapWrapperOwnershipTransferredIterator) Error() error {
	return it.fail
}

func (it *SwapWrapperOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type SwapWrapperOwnershipTransferred struct {
	From common.Address
	To   common.Address
	Raw  types.Log
}

func (_SwapWrapper *SwapWrapperFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*SwapWrapperOwnershipTransferredIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _SwapWrapper.contract.FilterLogs(opts, "OwnershipTransferred", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &SwapWrapperOwnershipTransferredIterator{contract: _SwapWrapper.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

func (_SwapWrapper *SwapWrapperFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *SwapWrapperOwnershipTransferred, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _SwapWrapper.contract.WatchLogs(opts, "OwnershipTransferred", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(SwapWrapperOwnershipTransferred)
				if err := _SwapWrapper.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_SwapWrapper *SwapWrapperFilterer) ParseOwnershipTransferred(log types.Log) (*SwapWrapperOwnershipTransferred, error) {
	event := new(SwapWrapperOwnershipTransferred)
	if err := _SwapWrapper.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type SwapWrapperTokenSwitchedIterator struct {
	Event *SwapWrapperTokenSwitched

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *SwapWrapperTokenSwitchedIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(SwapWrapperTokenSwitched)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(SwapWrapperTokenSwitched)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *SwapWrapperTokenSwitchedIterator) Error() error {
	return it.fail
}

func (it *SwapWrapperTokenSwitchedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type SwapWrapperTokenSwitched struct {
	TokenIn   common.Address
	AmountIn  *big.Int
	TokenOut  common.Address
	AmountOut *big.Int
	To        common.Address
	Raw       types.Log
}

func (_SwapWrapper *SwapWrapperFilterer) FilterTokenSwitched(opts *bind.FilterOpts) (*SwapWrapperTokenSwitchedIterator, error) {

	logs, sub, err := _SwapWrapper.contract.FilterLogs(opts, "TokenSwitched")
	if err != nil {
		return nil, err
	}
	return &SwapWrapperTokenSwitchedIterator{contract: _SwapWrapper.contract, event: "TokenSwitched", logs: logs, sub: sub}, nil
}

func (_SwapWrapper *SwapWrapperFilterer) WatchTokenSwitched(opts *bind.WatchOpts, sink chan<- *SwapWrapperTokenSwitched) (event.Subscription, error) {

	logs, sub, err := _SwapWrapper.contract.WatchLogs(opts, "TokenSwitched")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(SwapWrapperTokenSwitched)
				if err := _SwapWrapper.contract.UnpackLog(event, "TokenSwitched", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_SwapWrapper *SwapWrapperFilterer) ParseTokenSwitched(log types.Log) (*SwapWrapperTokenSwitched, error) {
	event := new(SwapWrapperTokenSwitched)
	if err := _SwapWrapper.contract.UnpackLog(event, "TokenSwitched", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type SwapWrapperTraderSetIterator struct {
	Event *SwapWrapperTraderSet

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *SwapWrapperTraderSetIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(SwapWrapperTraderSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(SwapWrapperTraderSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *SwapWrapperTraderSetIterator) Error() error {
	return it.fail
}

func (it *SwapWrapperTraderSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type SwapWrapperTraderSet struct {
	Trader      common.Address
	TraderState bool
	Raw         types.Log
}

func (_SwapWrapper *SwapWrapperFilterer) FilterTraderSet(opts *bind.FilterOpts) (*SwapWrapperTraderSetIterator, error) {

	logs, sub, err := _SwapWrapper.contract.FilterLogs(opts, "TraderSet")
	if err != nil {
		return nil, err
	}
	return &SwapWrapperTraderSetIterator{contract: _SwapWrapper.contract, event: "TraderSet", logs: logs, sub: sub}, nil
}

func (_SwapWrapper *SwapWrapperFilterer) WatchTraderSet(opts *bind.WatchOpts, sink chan<- *SwapWrapperTraderSet) (event.Subscription, error) {

	logs, sub, err := _SwapWrapper.contract.WatchLogs(opts, "TraderSet")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(SwapWrapperTraderSet)
				if err := _SwapWrapper.contract.UnpackLog(event, "TraderSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_SwapWrapper *SwapWrapperFilterer) ParseTraderSet(log types.Log) (*SwapWrapperTraderSet, error) {
	event := new(SwapWrapperTraderSet)
	if err := _SwapWrapper.contract.UnpackLog(event, "TraderSet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

func (_SwapWrapper *SwapWrapper) ParseLog(log types.Log) (generated.AbigenLog, error) {
	switch log.Topics[0] {
	case _SwapWrapper.abi.Events["OwnershipTransferRequested"].ID:
		return _SwapWrapper.ParseOwnershipTransferRequested(log)
	case _SwapWrapper.abi.Events["OwnershipTransferred"].ID:
		return _SwapWrapper.ParseOwnershipTransferred(log)
	case _SwapWrapper.abi.Events["TokenSwitched"].ID:
		return _SwapWrapper.ParseTokenSwitched(log)
	case _SwapWrapper.abi.Events["TraderSet"].ID:
		return _SwapWrapper.ParseTraderSet(log)

	default:
		return nil, fmt.Errorf("abigen wrapper received unknown log topic: %v", log.Topics[0])
	}
}

func (SwapWrapperOwnershipTransferRequested) Topic() common.Hash {
	return common.HexToHash("0xed8889f560326eb138920d842192f0eb3dd22b4f139c87a2c57538e05bae1278")
}

func (SwapWrapperOwnershipTransferred) Topic() common.Hash {
	return common.HexToHash("0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0")
}

func (SwapWrapperTokenSwitched) Topic() common.Hash {
	return common.HexToHash("0xacbe630c79ea05c0e9f63dec03783d986a1a5970a9f10269512f1b7a3f31a57a")
}

func (SwapWrapperTraderSet) Topic() common.Hash {
	return common.HexToHash("0xc042e64e64db64529dc6e20916a47711d598cadb3f399d5999261d46eff744ea")
}

func (_SwapWrapper *SwapWrapper) Address() common.Address {
	return _SwapWrapper.address
}

type SwapWrapperInterface interface {
	GetERC20BalanceAndLPP(opts *bind.CallOpts, token common.Address, account common.Address) (*big.Int, error)

	GetRewardAndLPP(opts *bind.CallOpts) (*big.Int, error)

	Owner(opts *bind.CallOpts) (common.Address, error)

	SMining(opts *bind.CallOpts) (common.Address, error)

	SRouter(opts *bind.CallOpts) (common.Address, error)

	STraderList(opts *bind.CallOpts, arg0 common.Address) (bool, error)

	AcceptOwnership(opts *bind.TransactOpts) (*types.Transaction, error)

	ClaimRewardAndLPP(opts *bind.TransactOpts) (*types.Transaction, error)

	Initialize(opts *bind.TransactOpts, _router common.Address, _mining common.Address) (*types.Transaction, error)

	SafeApproveAndLPP(opts *bind.TransactOpts, token common.Address, spender common.Address, value *big.Int) (*types.Transaction, error)

	SafeTransferAndLPP(opts *bind.TransactOpts, token common.Address, to common.Address, value *big.Int) (*types.Transaction, error)

	SafeTransferFromAndLPP(opts *bind.TransactOpts, token common.Address, from common.Address, to common.Address, value *big.Int) (*types.Transaction, error)

	SetTraderAndLPP(opts *bind.TransactOpts, trader common.Address, state bool) (*types.Transaction, error)

	SwapETHForExactTokensAndLPP(opts *bind.TransactOpts, amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error)

	SwapExactETHForTokensAndLPP(opts *bind.TransactOpts, path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error)

	SwapExactTokensForETHAndLPP(opts *bind.TransactOpts, amountIn *big.Int, path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error)

	SwapExactTokensForTokensAndLPP(opts *bind.TransactOpts, amountIn *big.Int, path []common.Address, amountOutMin *big.Int, to common.Address) (*types.Transaction, error)

	SwapTokensForExactETHAndLPP(opts *bind.TransactOpts, amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error)

	SwapTokensForExactTokensAndLPP(opts *bind.TransactOpts, amountOut *big.Int, path []common.Address, amountInMax *big.Int, to common.Address) (*types.Transaction, error)

	TransferOwnership(opts *bind.TransactOpts, to common.Address) (*types.Transaction, error)

	WithdrawETHAndLPP(opts *bind.TransactOpts, _amount *big.Int) (*types.Transaction, error)

	Receive(opts *bind.TransactOpts) (*types.Transaction, error)

	FilterOwnershipTransferRequested(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*SwapWrapperOwnershipTransferRequestedIterator, error)

	WatchOwnershipTransferRequested(opts *bind.WatchOpts, sink chan<- *SwapWrapperOwnershipTransferRequested, from []common.Address, to []common.Address) (event.Subscription, error)

	ParseOwnershipTransferRequested(log types.Log) (*SwapWrapperOwnershipTransferRequested, error)

	FilterOwnershipTransferred(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*SwapWrapperOwnershipTransferredIterator, error)

	WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *SwapWrapperOwnershipTransferred, from []common.Address, to []common.Address) (event.Subscription, error)

	ParseOwnershipTransferred(log types.Log) (*SwapWrapperOwnershipTransferred, error)

	FilterTokenSwitched(opts *bind.FilterOpts) (*SwapWrapperTokenSwitchedIterator, error)

	WatchTokenSwitched(opts *bind.WatchOpts, sink chan<- *SwapWrapperTokenSwitched) (event.Subscription, error)

	ParseTokenSwitched(log types.Log) (*SwapWrapperTokenSwitched, error)

	FilterTraderSet(opts *bind.FilterOpts) (*SwapWrapperTraderSetIterator, error)

	WatchTraderSet(opts *bind.WatchOpts, sink chan<- *SwapWrapperTraderSet) (event.Subscription, error)

	ParseTraderSet(log types.Log) (*SwapWrapperTraderSet, error)

	ParseLog(log types.Log) (generated.AbigenLog, error)

	Address() common.Address
}
