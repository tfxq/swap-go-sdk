// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package imojitofactory

import (
	"errors"
	"fmt"
	"math/big"
	"strings"

	"gitee.com/tfxq/swap-go-sdk/internal/gethwrappers/generated"
	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

var IMojitoFactoryMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"token0\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"token1\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"pair\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"PairCreated\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"allPairs\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"pair\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"allPairsLength\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"tokenA\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"tokenB\",\"type\":\"address\"}],\"name\":\"createPair\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"pair\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"feeTo\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"feeToSetter\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"tokenA\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"tokenB\",\"type\":\"address\"}],\"name\":\"getPair\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"pair\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"setFeeTo\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_pair\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_feeToDenominator\",\"type\":\"uint256\"}],\"name\":\"setFeeToDenominator\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"setFeeToSetter\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_pair\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_swapFeeNumerator\",\"type\":\"uint256\"}],\"name\":\"setSwapFeeNumerator\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

var IMojitoFactoryABI = IMojitoFactoryMetaData.ABI

type IMojitoFactory struct {
	address common.Address
	abi     abi.ABI
	IMojitoFactoryCaller
	IMojitoFactoryTransactor
	IMojitoFactoryFilterer
}

type IMojitoFactoryCaller struct {
	contract *bind.BoundContract
}

type IMojitoFactoryTransactor struct {
	contract *bind.BoundContract
}

type IMojitoFactoryFilterer struct {
	contract *bind.BoundContract
}

type IMojitoFactorySession struct {
	Contract     *IMojitoFactory
	CallOpts     bind.CallOpts
	TransactOpts bind.TransactOpts
}

type IMojitoFactoryCallerSession struct {
	Contract *IMojitoFactoryCaller
	CallOpts bind.CallOpts
}

type IMojitoFactoryTransactorSession struct {
	Contract     *IMojitoFactoryTransactor
	TransactOpts bind.TransactOpts
}

type IMojitoFactoryRaw struct {
	Contract *IMojitoFactory
}

type IMojitoFactoryCallerRaw struct {
	Contract *IMojitoFactoryCaller
}

type IMojitoFactoryTransactorRaw struct {
	Contract *IMojitoFactoryTransactor
}

func NewIMojitoFactory(address common.Address, backend bind.ContractBackend) (*IMojitoFactory, error) {
	abi, err := abi.JSON(strings.NewReader(IMojitoFactoryABI))
	if err != nil {
		return nil, err
	}
	contract, err := bindIMojitoFactory(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IMojitoFactory{address: address, abi: abi, IMojitoFactoryCaller: IMojitoFactoryCaller{contract: contract}, IMojitoFactoryTransactor: IMojitoFactoryTransactor{contract: contract}, IMojitoFactoryFilterer: IMojitoFactoryFilterer{contract: contract}}, nil
}

func NewIMojitoFactoryCaller(address common.Address, caller bind.ContractCaller) (*IMojitoFactoryCaller, error) {
	contract, err := bindIMojitoFactory(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IMojitoFactoryCaller{contract: contract}, nil
}

func NewIMojitoFactoryTransactor(address common.Address, transactor bind.ContractTransactor) (*IMojitoFactoryTransactor, error) {
	contract, err := bindIMojitoFactory(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IMojitoFactoryTransactor{contract: contract}, nil
}

func NewIMojitoFactoryFilterer(address common.Address, filterer bind.ContractFilterer) (*IMojitoFactoryFilterer, error) {
	contract, err := bindIMojitoFactory(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IMojitoFactoryFilterer{contract: contract}, nil
}

func bindIMojitoFactory(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IMojitoFactoryABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

func (_IMojitoFactory *IMojitoFactoryRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IMojitoFactory.Contract.IMojitoFactoryCaller.contract.Call(opts, result, method, params...)
}

func (_IMojitoFactory *IMojitoFactoryRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.IMojitoFactoryTransactor.contract.Transfer(opts)
}

func (_IMojitoFactory *IMojitoFactoryRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.IMojitoFactoryTransactor.contract.Transact(opts, method, params...)
}

func (_IMojitoFactory *IMojitoFactoryCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IMojitoFactory.Contract.contract.Call(opts, result, method, params...)
}

func (_IMojitoFactory *IMojitoFactoryTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.contract.Transfer(opts)
}

func (_IMojitoFactory *IMojitoFactoryTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.contract.Transact(opts, method, params...)
}

func (_IMojitoFactory *IMojitoFactoryCaller) AllPairs(opts *bind.CallOpts, arg0 *big.Int) (common.Address, error) {
	var out []interface{}
	err := _IMojitoFactory.contract.Call(opts, &out, "allPairs", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_IMojitoFactory *IMojitoFactorySession) AllPairs(arg0 *big.Int) (common.Address, error) {
	return _IMojitoFactory.Contract.AllPairs(&_IMojitoFactory.CallOpts, arg0)
}

func (_IMojitoFactory *IMojitoFactoryCallerSession) AllPairs(arg0 *big.Int) (common.Address, error) {
	return _IMojitoFactory.Contract.AllPairs(&_IMojitoFactory.CallOpts, arg0)
}

func (_IMojitoFactory *IMojitoFactoryCaller) AllPairsLength(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoFactory.contract.Call(opts, &out, "allPairsLength")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoFactory *IMojitoFactorySession) AllPairsLength() (*big.Int, error) {
	return _IMojitoFactory.Contract.AllPairsLength(&_IMojitoFactory.CallOpts)
}

func (_IMojitoFactory *IMojitoFactoryCallerSession) AllPairsLength() (*big.Int, error) {
	return _IMojitoFactory.Contract.AllPairsLength(&_IMojitoFactory.CallOpts)
}

func (_IMojitoFactory *IMojitoFactoryCaller) FeeTo(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IMojitoFactory.contract.Call(opts, &out, "feeTo")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_IMojitoFactory *IMojitoFactorySession) FeeTo() (common.Address, error) {
	return _IMojitoFactory.Contract.FeeTo(&_IMojitoFactory.CallOpts)
}

func (_IMojitoFactory *IMojitoFactoryCallerSession) FeeTo() (common.Address, error) {
	return _IMojitoFactory.Contract.FeeTo(&_IMojitoFactory.CallOpts)
}

func (_IMojitoFactory *IMojitoFactoryCaller) FeeToSetter(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IMojitoFactory.contract.Call(opts, &out, "feeToSetter")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_IMojitoFactory *IMojitoFactorySession) FeeToSetter() (common.Address, error) {
	return _IMojitoFactory.Contract.FeeToSetter(&_IMojitoFactory.CallOpts)
}

func (_IMojitoFactory *IMojitoFactoryCallerSession) FeeToSetter() (common.Address, error) {
	return _IMojitoFactory.Contract.FeeToSetter(&_IMojitoFactory.CallOpts)
}

func (_IMojitoFactory *IMojitoFactoryCaller) GetPair(opts *bind.CallOpts, tokenA common.Address, tokenB common.Address) (common.Address, error) {
	var out []interface{}
	err := _IMojitoFactory.contract.Call(opts, &out, "getPair", tokenA, tokenB)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_IMojitoFactory *IMojitoFactorySession) GetPair(tokenA common.Address, tokenB common.Address) (common.Address, error) {
	return _IMojitoFactory.Contract.GetPair(&_IMojitoFactory.CallOpts, tokenA, tokenB)
}

func (_IMojitoFactory *IMojitoFactoryCallerSession) GetPair(tokenA common.Address, tokenB common.Address) (common.Address, error) {
	return _IMojitoFactory.Contract.GetPair(&_IMojitoFactory.CallOpts, tokenA, tokenB)
}

func (_IMojitoFactory *IMojitoFactoryTransactor) CreatePair(opts *bind.TransactOpts, tokenA common.Address, tokenB common.Address) (*types.Transaction, error) {
	return _IMojitoFactory.contract.Transact(opts, "createPair", tokenA, tokenB)
}

func (_IMojitoFactory *IMojitoFactorySession) CreatePair(tokenA common.Address, tokenB common.Address) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.CreatePair(&_IMojitoFactory.TransactOpts, tokenA, tokenB)
}

func (_IMojitoFactory *IMojitoFactoryTransactorSession) CreatePair(tokenA common.Address, tokenB common.Address) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.CreatePair(&_IMojitoFactory.TransactOpts, tokenA, tokenB)
}

func (_IMojitoFactory *IMojitoFactoryTransactor) SetFeeTo(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error) {
	return _IMojitoFactory.contract.Transact(opts, "setFeeTo", arg0)
}

func (_IMojitoFactory *IMojitoFactorySession) SetFeeTo(arg0 common.Address) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.SetFeeTo(&_IMojitoFactory.TransactOpts, arg0)
}

func (_IMojitoFactory *IMojitoFactoryTransactorSession) SetFeeTo(arg0 common.Address) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.SetFeeTo(&_IMojitoFactory.TransactOpts, arg0)
}

func (_IMojitoFactory *IMojitoFactoryTransactor) SetFeeToDenominator(opts *bind.TransactOpts, _pair common.Address, _feeToDenominator *big.Int) (*types.Transaction, error) {
	return _IMojitoFactory.contract.Transact(opts, "setFeeToDenominator", _pair, _feeToDenominator)
}

func (_IMojitoFactory *IMojitoFactorySession) SetFeeToDenominator(_pair common.Address, _feeToDenominator *big.Int) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.SetFeeToDenominator(&_IMojitoFactory.TransactOpts, _pair, _feeToDenominator)
}

func (_IMojitoFactory *IMojitoFactoryTransactorSession) SetFeeToDenominator(_pair common.Address, _feeToDenominator *big.Int) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.SetFeeToDenominator(&_IMojitoFactory.TransactOpts, _pair, _feeToDenominator)
}

func (_IMojitoFactory *IMojitoFactoryTransactor) SetFeeToSetter(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error) {
	return _IMojitoFactory.contract.Transact(opts, "setFeeToSetter", arg0)
}

func (_IMojitoFactory *IMojitoFactorySession) SetFeeToSetter(arg0 common.Address) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.SetFeeToSetter(&_IMojitoFactory.TransactOpts, arg0)
}

func (_IMojitoFactory *IMojitoFactoryTransactorSession) SetFeeToSetter(arg0 common.Address) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.SetFeeToSetter(&_IMojitoFactory.TransactOpts, arg0)
}

func (_IMojitoFactory *IMojitoFactoryTransactor) SetSwapFeeNumerator(opts *bind.TransactOpts, _pair common.Address, _swapFeeNumerator *big.Int) (*types.Transaction, error) {
	return _IMojitoFactory.contract.Transact(opts, "setSwapFeeNumerator", _pair, _swapFeeNumerator)
}

func (_IMojitoFactory *IMojitoFactorySession) SetSwapFeeNumerator(_pair common.Address, _swapFeeNumerator *big.Int) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.SetSwapFeeNumerator(&_IMojitoFactory.TransactOpts, _pair, _swapFeeNumerator)
}

func (_IMojitoFactory *IMojitoFactoryTransactorSession) SetSwapFeeNumerator(_pair common.Address, _swapFeeNumerator *big.Int) (*types.Transaction, error) {
	return _IMojitoFactory.Contract.SetSwapFeeNumerator(&_IMojitoFactory.TransactOpts, _pair, _swapFeeNumerator)
}

type IMojitoFactoryPairCreatedIterator struct {
	Event *IMojitoFactoryPairCreated

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *IMojitoFactoryPairCreatedIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IMojitoFactoryPairCreated)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(IMojitoFactoryPairCreated)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *IMojitoFactoryPairCreatedIterator) Error() error {
	return it.fail
}

func (it *IMojitoFactoryPairCreatedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type IMojitoFactoryPairCreated struct {
	Token0 common.Address
	Token1 common.Address
	Pair   common.Address
	Arg3   *big.Int
	Raw    types.Log
}

func (_IMojitoFactory *IMojitoFactoryFilterer) FilterPairCreated(opts *bind.FilterOpts, token0 []common.Address, token1 []common.Address) (*IMojitoFactoryPairCreatedIterator, error) {

	var token0Rule []interface{}
	for _, token0Item := range token0 {
		token0Rule = append(token0Rule, token0Item)
	}
	var token1Rule []interface{}
	for _, token1Item := range token1 {
		token1Rule = append(token1Rule, token1Item)
	}

	logs, sub, err := _IMojitoFactory.contract.FilterLogs(opts, "PairCreated", token0Rule, token1Rule)
	if err != nil {
		return nil, err
	}
	return &IMojitoFactoryPairCreatedIterator{contract: _IMojitoFactory.contract, event: "PairCreated", logs: logs, sub: sub}, nil
}

func (_IMojitoFactory *IMojitoFactoryFilterer) WatchPairCreated(opts *bind.WatchOpts, sink chan<- *IMojitoFactoryPairCreated, token0 []common.Address, token1 []common.Address) (event.Subscription, error) {

	var token0Rule []interface{}
	for _, token0Item := range token0 {
		token0Rule = append(token0Rule, token0Item)
	}
	var token1Rule []interface{}
	for _, token1Item := range token1 {
		token1Rule = append(token1Rule, token1Item)
	}

	logs, sub, err := _IMojitoFactory.contract.WatchLogs(opts, "PairCreated", token0Rule, token1Rule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(IMojitoFactoryPairCreated)
				if err := _IMojitoFactory.contract.UnpackLog(event, "PairCreated", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_IMojitoFactory *IMojitoFactoryFilterer) ParsePairCreated(log types.Log) (*IMojitoFactoryPairCreated, error) {
	event := new(IMojitoFactoryPairCreated)
	if err := _IMojitoFactory.contract.UnpackLog(event, "PairCreated", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

func (_IMojitoFactory *IMojitoFactory) ParseLog(log types.Log) (generated.AbigenLog, error) {
	switch log.Topics[0] {
	case _IMojitoFactory.abi.Events["PairCreated"].ID:
		return _IMojitoFactory.ParsePairCreated(log)

	default:
		return nil, fmt.Errorf("abigen wrapper received unknown log topic: %v", log.Topics[0])
	}
}

func (IMojitoFactoryPairCreated) Topic() common.Hash {
	return common.HexToHash("0x0d3648bd0f6ba80134a33ba9275ac585d9d315f0ad8355cddefde31afa28d0e9")
}

func (_IMojitoFactory *IMojitoFactory) Address() common.Address {
	return _IMojitoFactory.address
}

type IMojitoFactoryInterface interface {
	AllPairs(opts *bind.CallOpts, arg0 *big.Int) (common.Address, error)

	AllPairsLength(opts *bind.CallOpts) (*big.Int, error)

	FeeTo(opts *bind.CallOpts) (common.Address, error)

	FeeToSetter(opts *bind.CallOpts) (common.Address, error)

	GetPair(opts *bind.CallOpts, tokenA common.Address, tokenB common.Address) (common.Address, error)

	CreatePair(opts *bind.TransactOpts, tokenA common.Address, tokenB common.Address) (*types.Transaction, error)

	SetFeeTo(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error)

	SetFeeToDenominator(opts *bind.TransactOpts, _pair common.Address, _feeToDenominator *big.Int) (*types.Transaction, error)

	SetFeeToSetter(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error)

	SetSwapFeeNumerator(opts *bind.TransactOpts, _pair common.Address, _swapFeeNumerator *big.Int) (*types.Transaction, error)

	FilterPairCreated(opts *bind.FilterOpts, token0 []common.Address, token1 []common.Address) (*IMojitoFactoryPairCreatedIterator, error)

	WatchPairCreated(opts *bind.WatchOpts, sink chan<- *IMojitoFactoryPairCreated, token0 []common.Address, token1 []common.Address) (event.Subscription, error)

	ParsePairCreated(log types.Log) (*IMojitoFactoryPairCreated, error)

	ParseLog(log types.Log) (generated.AbigenLog, error)

	Address() common.Address
}
