// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package wkcs

import (
	"errors"
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/internal/gethwrappers/generated"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

var WKCSMetaData = &bind.MetaData{
	ABI: "[{\"constant\":true,\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"guy\",\"type\":\"address\"},{\"name\":\"wad\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"src\",\"type\":\"address\"},{\"name\":\"dst\",\"type\":\"address\"},{\"name\":\"wad\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"wad\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"decimals\",\"outputs\":[{\"name\":\"\",\"type\":\"uint8\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"dst\",\"type\":\"address\"},{\"name\":\"wad\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"deposit\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"address\"},{\"name\":\"\",\"type\":\"address\"}],\"name\":\"allowance\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"fallback\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"src\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"guy\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"wad\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"src\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"dst\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"wad\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"dst\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"wad\",\"type\":\"uint256\"}],\"name\":\"Deposit\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"src\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"wad\",\"type\":\"uint256\"}],\"name\":\"Withdrawal\",\"type\":\"event\"}]",
	Bin: "0x606060405260408051908101604052600b81527f57726170706564204b43530000000000000000000000000000000000000000006020820152600090805161004b9291602001906100b1565b5060408051908101604052600481527f574b435300000000000000000000000000000000000000000000000000000000602082015260019080516100939291602001906100b1565b506002805460ff1916601217905534156100ac57600080fd5b61014c565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106100f257805160ff191683800117855561011f565b8280016001018555821561011f579182015b8281111561011f578251825591602001919060010190610104565b5061012b92915061012f565b5090565b61014991905b8082111561012b5760008155600101610135565b90565b6107ca8061015b6000396000f3006060604052600436106100ae5763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166306fdde0381146100b8578063095ea7b31461014257806318160ddd1461018557806323b872dd146101aa5780632e1a7d4d146101df578063313ce567146101f557806370a082311461021e57806395d89b411461024a578063a9059cbb1461025d578063d0e30db0146100ae578063dd62ed3e1461028c575b6100b66102be565b005b34156100c357600080fd5b6100cb610321565b60405160208082528190810183818151815260200191508051906020019080838360005b838110156101075780820151838201526020016100ef565b50505050905090810190601f1680156101345780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561014d57600080fd5b61017173ffffffffffffffffffffffffffffffffffffffff600435166024356103bf565b604051901515815260200160405180910390f35b341561019057600080fd5b610198610438565b60405190815260200160405180910390f35b34156101b557600080fd5b61017173ffffffffffffffffffffffffffffffffffffffff60043581169060243516604435610453565b34156101ea57600080fd5b6100b6600435610612565b341561020057600080fd5b6102086106e7565b60405160ff909116815260200160405180910390f35b341561022957600080fd5b61019873ffffffffffffffffffffffffffffffffffffffff600435166106f0565b341561025557600080fd5b6100cb610702565b341561026857600080fd5b61017173ffffffffffffffffffffffffffffffffffffffff6004351660243561076d565b341561029757600080fd5b61019873ffffffffffffffffffffffffffffffffffffffff60043581169060243516610781565b73ffffffffffffffffffffffffffffffffffffffff3316600081815260036020526040908190208054349081019091557fe1fffcc4923d04b559f4d29a8bfc6cda04eb5b0d3c460751c2402c5c5cc9109c915190815260200160405180910390a2565b60008054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156103b75780601f1061038c576101008083540402835291602001916103b7565b820191906000526020600020905b81548152906001019060200180831161039a57829003601f168201915b505050505081565b73ffffffffffffffffffffffffffffffffffffffff338116600081815260046020908152604080832094871680845294909152808220859055909291907f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b9259085905190815260200160405180910390a350600192915050565b73ffffffffffffffffffffffffffffffffffffffff30163190565b73ffffffffffffffffffffffffffffffffffffffff83166000908152600360205260408120548290101561048657600080fd5b3373ffffffffffffffffffffffffffffffffffffffff168473ffffffffffffffffffffffffffffffffffffffff1614158015610515575073ffffffffffffffffffffffffffffffffffffffff808516600090815260046020908152604080832033909416835292905220547fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff14155b156105965773ffffffffffffffffffffffffffffffffffffffff808516600090815260046020908152604080832033909416835292905220548290101561055b57600080fd5b73ffffffffffffffffffffffffffffffffffffffff808516600090815260046020908152604080832033909416835292905220805483900390555b73ffffffffffffffffffffffffffffffffffffffff8085166000818152600360205260408082208054879003905592861680825290839020805486019055917fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9085905190815260200160405180910390a35060019392505050565b73ffffffffffffffffffffffffffffffffffffffff33166000908152600360205260409020548190101561064557600080fd5b73ffffffffffffffffffffffffffffffffffffffff3316600081815260036020526040908190208054849003905582156108fc0290839051600060405180830381858888f19350505050151561069a57600080fd5b3373ffffffffffffffffffffffffffffffffffffffff167f7fcf532c15f0a6db0bd6d0e038bea71d30d808c7d98cb3bf7268a95bf5081b658260405190815260200160405180910390a250565b60025460ff1681565b60036020526000908152604090205481565b60018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156103b75780601f1061038c576101008083540402835291602001916103b7565b600061077a338484610453565b9392505050565b6004602090815260009283526040808420909152908252902054815600a165627a7a72305820e1b450529d0ebf9df7065d42c94931b4d638376a2e005c3a9a2fa748a46497910029",
}

var WKCSABI = WKCSMetaData.ABI

var WKCSBin = WKCSMetaData.Bin

func DeployWKCS(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *WKCS, error) {
	parsed, err := WKCSMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(WKCSBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &WKCS{WKCSCaller: WKCSCaller{contract: contract}, WKCSTransactor: WKCSTransactor{contract: contract}, WKCSFilterer: WKCSFilterer{contract: contract}}, nil
}

type WKCS struct {
	address common.Address
	abi     abi.ABI
	WKCSCaller
	WKCSTransactor
	WKCSFilterer
}

type WKCSCaller struct {
	contract *bind.BoundContract
}

type WKCSTransactor struct {
	contract *bind.BoundContract
}

type WKCSFilterer struct {
	contract *bind.BoundContract
}

type WKCSSession struct {
	Contract     *WKCS
	CallOpts     bind.CallOpts
	TransactOpts bind.TransactOpts
}

type WKCSCallerSession struct {
	Contract *WKCSCaller
	CallOpts bind.CallOpts
}

type WKCSTransactorSession struct {
	Contract     *WKCSTransactor
	TransactOpts bind.TransactOpts
}

type WKCSRaw struct {
	Contract *WKCS
}

type WKCSCallerRaw struct {
	Contract *WKCSCaller
}

type WKCSTransactorRaw struct {
	Contract *WKCSTransactor
}

func NewWKCS(address common.Address, backend bind.ContractBackend) (*WKCS, error) {
	abi, err := abi.JSON(strings.NewReader(WKCSABI))
	if err != nil {
		return nil, err
	}
	contract, err := bindWKCS(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &WKCS{address: address, abi: abi, WKCSCaller: WKCSCaller{contract: contract}, WKCSTransactor: WKCSTransactor{contract: contract}, WKCSFilterer: WKCSFilterer{contract: contract}}, nil
}

func NewWKCSCaller(address common.Address, caller bind.ContractCaller) (*WKCSCaller, error) {
	contract, err := bindWKCS(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &WKCSCaller{contract: contract}, nil
}

func NewWKCSTransactor(address common.Address, transactor bind.ContractTransactor) (*WKCSTransactor, error) {
	contract, err := bindWKCS(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &WKCSTransactor{contract: contract}, nil
}

func NewWKCSFilterer(address common.Address, filterer bind.ContractFilterer) (*WKCSFilterer, error) {
	contract, err := bindWKCS(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &WKCSFilterer{contract: contract}, nil
}

func bindWKCS(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(WKCSABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

func (_WKCS *WKCSRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _WKCS.Contract.WKCSCaller.contract.Call(opts, result, method, params...)
}

func (_WKCS *WKCSRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _WKCS.Contract.WKCSTransactor.contract.Transfer(opts)
}

func (_WKCS *WKCSRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _WKCS.Contract.WKCSTransactor.contract.Transact(opts, method, params...)
}

func (_WKCS *WKCSCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _WKCS.Contract.contract.Call(opts, result, method, params...)
}

func (_WKCS *WKCSTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _WKCS.Contract.contract.Transfer(opts)
}

func (_WKCS *WKCSTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _WKCS.Contract.contract.Transact(opts, method, params...)
}

func (_WKCS *WKCSCaller) Allowance(opts *bind.CallOpts, arg0 common.Address, arg1 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _WKCS.contract.Call(opts, &out, "allowance", arg0, arg1)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_WKCS *WKCSSession) Allowance(arg0 common.Address, arg1 common.Address) (*big.Int, error) {
	return _WKCS.Contract.Allowance(&_WKCS.CallOpts, arg0, arg1)
}

func (_WKCS *WKCSCallerSession) Allowance(arg0 common.Address, arg1 common.Address) (*big.Int, error) {
	return _WKCS.Contract.Allowance(&_WKCS.CallOpts, arg0, arg1)
}

func (_WKCS *WKCSCaller) BalanceOf(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _WKCS.contract.Call(opts, &out, "balanceOf", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_WKCS *WKCSSession) BalanceOf(arg0 common.Address) (*big.Int, error) {
	return _WKCS.Contract.BalanceOf(&_WKCS.CallOpts, arg0)
}

func (_WKCS *WKCSCallerSession) BalanceOf(arg0 common.Address) (*big.Int, error) {
	return _WKCS.Contract.BalanceOf(&_WKCS.CallOpts, arg0)
}

func (_WKCS *WKCSCaller) Decimals(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _WKCS.contract.Call(opts, &out, "decimals")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

func (_WKCS *WKCSSession) Decimals() (uint8, error) {
	return _WKCS.Contract.Decimals(&_WKCS.CallOpts)
}

func (_WKCS *WKCSCallerSession) Decimals() (uint8, error) {
	return _WKCS.Contract.Decimals(&_WKCS.CallOpts)
}

func (_WKCS *WKCSCaller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _WKCS.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

func (_WKCS *WKCSSession) Name() (string, error) {
	return _WKCS.Contract.Name(&_WKCS.CallOpts)
}

func (_WKCS *WKCSCallerSession) Name() (string, error) {
	return _WKCS.Contract.Name(&_WKCS.CallOpts)
}

func (_WKCS *WKCSCaller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _WKCS.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

func (_WKCS *WKCSSession) Symbol() (string, error) {
	return _WKCS.Contract.Symbol(&_WKCS.CallOpts)
}

func (_WKCS *WKCSCallerSession) Symbol() (string, error) {
	return _WKCS.Contract.Symbol(&_WKCS.CallOpts)
}

func (_WKCS *WKCSCaller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _WKCS.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_WKCS *WKCSSession) TotalSupply() (*big.Int, error) {
	return _WKCS.Contract.TotalSupply(&_WKCS.CallOpts)
}

func (_WKCS *WKCSCallerSession) TotalSupply() (*big.Int, error) {
	return _WKCS.Contract.TotalSupply(&_WKCS.CallOpts)
}

func (_WKCS *WKCSTransactor) Approve(opts *bind.TransactOpts, guy common.Address, wad *big.Int) (*types.Transaction, error) {
	return _WKCS.contract.Transact(opts, "approve", guy, wad)
}

func (_WKCS *WKCSSession) Approve(guy common.Address, wad *big.Int) (*types.Transaction, error) {
	return _WKCS.Contract.Approve(&_WKCS.TransactOpts, guy, wad)
}

func (_WKCS *WKCSTransactorSession) Approve(guy common.Address, wad *big.Int) (*types.Transaction, error) {
	return _WKCS.Contract.Approve(&_WKCS.TransactOpts, guy, wad)
}

func (_WKCS *WKCSTransactor) Deposit(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _WKCS.contract.Transact(opts, "deposit")
}

func (_WKCS *WKCSSession) Deposit() (*types.Transaction, error) {
	return _WKCS.Contract.Deposit(&_WKCS.TransactOpts)
}

func (_WKCS *WKCSTransactorSession) Deposit() (*types.Transaction, error) {
	return _WKCS.Contract.Deposit(&_WKCS.TransactOpts)
}

func (_WKCS *WKCSTransactor) Transfer(opts *bind.TransactOpts, dst common.Address, wad *big.Int) (*types.Transaction, error) {
	return _WKCS.contract.Transact(opts, "transfer", dst, wad)
}

func (_WKCS *WKCSSession) Transfer(dst common.Address, wad *big.Int) (*types.Transaction, error) {
	return _WKCS.Contract.Transfer(&_WKCS.TransactOpts, dst, wad)
}

func (_WKCS *WKCSTransactorSession) Transfer(dst common.Address, wad *big.Int) (*types.Transaction, error) {
	return _WKCS.Contract.Transfer(&_WKCS.TransactOpts, dst, wad)
}

func (_WKCS *WKCSTransactor) TransferFrom(opts *bind.TransactOpts, src common.Address, dst common.Address, wad *big.Int) (*types.Transaction, error) {
	return _WKCS.contract.Transact(opts, "transferFrom", src, dst, wad)
}

func (_WKCS *WKCSSession) TransferFrom(src common.Address, dst common.Address, wad *big.Int) (*types.Transaction, error) {
	return _WKCS.Contract.TransferFrom(&_WKCS.TransactOpts, src, dst, wad)
}

func (_WKCS *WKCSTransactorSession) TransferFrom(src common.Address, dst common.Address, wad *big.Int) (*types.Transaction, error) {
	return _WKCS.Contract.TransferFrom(&_WKCS.TransactOpts, src, dst, wad)
}

func (_WKCS *WKCSTransactor) Withdraw(opts *bind.TransactOpts, wad *big.Int) (*types.Transaction, error) {
	return _WKCS.contract.Transact(opts, "withdraw", wad)
}

func (_WKCS *WKCSSession) Withdraw(wad *big.Int) (*types.Transaction, error) {
	return _WKCS.Contract.Withdraw(&_WKCS.TransactOpts, wad)
}

func (_WKCS *WKCSTransactorSession) Withdraw(wad *big.Int) (*types.Transaction, error) {
	return _WKCS.Contract.Withdraw(&_WKCS.TransactOpts, wad)
}

func (_WKCS *WKCSTransactor) Fallback(opts *bind.TransactOpts, calldata []byte) (*types.Transaction, error) {
	return _WKCS.contract.RawTransact(opts, calldata)
}

func (_WKCS *WKCSSession) Fallback(calldata []byte) (*types.Transaction, error) {
	return _WKCS.Contract.Fallback(&_WKCS.TransactOpts, calldata)
}

func (_WKCS *WKCSTransactorSession) Fallback(calldata []byte) (*types.Transaction, error) {
	return _WKCS.Contract.Fallback(&_WKCS.TransactOpts, calldata)
}

type WKCSApprovalIterator struct {
	Event *WKCSApproval

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *WKCSApprovalIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(WKCSApproval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(WKCSApproval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *WKCSApprovalIterator) Error() error {
	return it.fail
}

func (it *WKCSApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type WKCSApproval struct {
	Src common.Address
	Guy common.Address
	Wad *big.Int
	Raw types.Log
}

func (_WKCS *WKCSFilterer) FilterApproval(opts *bind.FilterOpts, src []common.Address, guy []common.Address) (*WKCSApprovalIterator, error) {

	var srcRule []interface{}
	for _, srcItem := range src {
		srcRule = append(srcRule, srcItem)
	}
	var guyRule []interface{}
	for _, guyItem := range guy {
		guyRule = append(guyRule, guyItem)
	}

	logs, sub, err := _WKCS.contract.FilterLogs(opts, "Approval", srcRule, guyRule)
	if err != nil {
		return nil, err
	}
	return &WKCSApprovalIterator{contract: _WKCS.contract, event: "Approval", logs: logs, sub: sub}, nil
}

func (_WKCS *WKCSFilterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *WKCSApproval, src []common.Address, guy []common.Address) (event.Subscription, error) {

	var srcRule []interface{}
	for _, srcItem := range src {
		srcRule = append(srcRule, srcItem)
	}
	var guyRule []interface{}
	for _, guyItem := range guy {
		guyRule = append(guyRule, guyItem)
	}

	logs, sub, err := _WKCS.contract.WatchLogs(opts, "Approval", srcRule, guyRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(WKCSApproval)
				if err := _WKCS.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_WKCS *WKCSFilterer) ParseApproval(log types.Log) (*WKCSApproval, error) {
	event := new(WKCSApproval)
	if err := _WKCS.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type WKCSDepositIterator struct {
	Event *WKCSDeposit

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *WKCSDepositIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(WKCSDeposit)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(WKCSDeposit)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *WKCSDepositIterator) Error() error {
	return it.fail
}

func (it *WKCSDepositIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type WKCSDeposit struct {
	Dst common.Address
	Wad *big.Int
	Raw types.Log
}

func (_WKCS *WKCSFilterer) FilterDeposit(opts *bind.FilterOpts, dst []common.Address) (*WKCSDepositIterator, error) {

	var dstRule []interface{}
	for _, dstItem := range dst {
		dstRule = append(dstRule, dstItem)
	}

	logs, sub, err := _WKCS.contract.FilterLogs(opts, "Deposit", dstRule)
	if err != nil {
		return nil, err
	}
	return &WKCSDepositIterator{contract: _WKCS.contract, event: "Deposit", logs: logs, sub: sub}, nil
}

func (_WKCS *WKCSFilterer) WatchDeposit(opts *bind.WatchOpts, sink chan<- *WKCSDeposit, dst []common.Address) (event.Subscription, error) {

	var dstRule []interface{}
	for _, dstItem := range dst {
		dstRule = append(dstRule, dstItem)
	}

	logs, sub, err := _WKCS.contract.WatchLogs(opts, "Deposit", dstRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(WKCSDeposit)
				if err := _WKCS.contract.UnpackLog(event, "Deposit", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_WKCS *WKCSFilterer) ParseDeposit(log types.Log) (*WKCSDeposit, error) {
	event := new(WKCSDeposit)
	if err := _WKCS.contract.UnpackLog(event, "Deposit", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type WKCSTransferIterator struct {
	Event *WKCSTransfer

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *WKCSTransferIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(WKCSTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(WKCSTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *WKCSTransferIterator) Error() error {
	return it.fail
}

func (it *WKCSTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type WKCSTransfer struct {
	Src common.Address
	Dst common.Address
	Wad *big.Int
	Raw types.Log
}

func (_WKCS *WKCSFilterer) FilterTransfer(opts *bind.FilterOpts, src []common.Address, dst []common.Address) (*WKCSTransferIterator, error) {

	var srcRule []interface{}
	for _, srcItem := range src {
		srcRule = append(srcRule, srcItem)
	}
	var dstRule []interface{}
	for _, dstItem := range dst {
		dstRule = append(dstRule, dstItem)
	}

	logs, sub, err := _WKCS.contract.FilterLogs(opts, "Transfer", srcRule, dstRule)
	if err != nil {
		return nil, err
	}
	return &WKCSTransferIterator{contract: _WKCS.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

func (_WKCS *WKCSFilterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *WKCSTransfer, src []common.Address, dst []common.Address) (event.Subscription, error) {

	var srcRule []interface{}
	for _, srcItem := range src {
		srcRule = append(srcRule, srcItem)
	}
	var dstRule []interface{}
	for _, dstItem := range dst {
		dstRule = append(dstRule, dstItem)
	}

	logs, sub, err := _WKCS.contract.WatchLogs(opts, "Transfer", srcRule, dstRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(WKCSTransfer)
				if err := _WKCS.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_WKCS *WKCSFilterer) ParseTransfer(log types.Log) (*WKCSTransfer, error) {
	event := new(WKCSTransfer)
	if err := _WKCS.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type WKCSWithdrawalIterator struct {
	Event *WKCSWithdrawal

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *WKCSWithdrawalIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(WKCSWithdrawal)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(WKCSWithdrawal)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *WKCSWithdrawalIterator) Error() error {
	return it.fail
}

func (it *WKCSWithdrawalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type WKCSWithdrawal struct {
	Src common.Address
	Wad *big.Int
	Raw types.Log
}

func (_WKCS *WKCSFilterer) FilterWithdrawal(opts *bind.FilterOpts, src []common.Address) (*WKCSWithdrawalIterator, error) {

	var srcRule []interface{}
	for _, srcItem := range src {
		srcRule = append(srcRule, srcItem)
	}

	logs, sub, err := _WKCS.contract.FilterLogs(opts, "Withdrawal", srcRule)
	if err != nil {
		return nil, err
	}
	return &WKCSWithdrawalIterator{contract: _WKCS.contract, event: "Withdrawal", logs: logs, sub: sub}, nil
}

func (_WKCS *WKCSFilterer) WatchWithdrawal(opts *bind.WatchOpts, sink chan<- *WKCSWithdrawal, src []common.Address) (event.Subscription, error) {

	var srcRule []interface{}
	for _, srcItem := range src {
		srcRule = append(srcRule, srcItem)
	}

	logs, sub, err := _WKCS.contract.WatchLogs(opts, "Withdrawal", srcRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(WKCSWithdrawal)
				if err := _WKCS.contract.UnpackLog(event, "Withdrawal", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_WKCS *WKCSFilterer) ParseWithdrawal(log types.Log) (*WKCSWithdrawal, error) {
	event := new(WKCSWithdrawal)
	if err := _WKCS.contract.UnpackLog(event, "Withdrawal", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

func (_WKCS *WKCS) ParseLog(log types.Log) (generated.AbigenLog, error) {
	switch log.Topics[0] {
	case _WKCS.abi.Events["Approval"].ID:
		return _WKCS.ParseApproval(log)
	case _WKCS.abi.Events["Deposit"].ID:
		return _WKCS.ParseDeposit(log)
	case _WKCS.abi.Events["Transfer"].ID:
		return _WKCS.ParseTransfer(log)
	case _WKCS.abi.Events["Withdrawal"].ID:
		return _WKCS.ParseWithdrawal(log)

	default:
		return nil, fmt.Errorf("abigen wrapper received unknown log topic: %v", log.Topics[0])
	}
}

func (WKCSApproval) Topic() common.Hash {
	return common.HexToHash("0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925")
}

func (WKCSDeposit) Topic() common.Hash {
	return common.HexToHash("0xe1fffcc4923d04b559f4d29a8bfc6cda04eb5b0d3c460751c2402c5c5cc9109c")
}

func (WKCSTransfer) Topic() common.Hash {
	return common.HexToHash("0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef")
}

func (WKCSWithdrawal) Topic() common.Hash {
	return common.HexToHash("0x7fcf532c15f0a6db0bd6d0e038bea71d30d808c7d98cb3bf7268a95bf5081b65")
}

func (_WKCS *WKCS) Address() common.Address {
	return _WKCS.address
}

type WKCSInterface interface {
	Allowance(opts *bind.CallOpts, arg0 common.Address, arg1 common.Address) (*big.Int, error)

	BalanceOf(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error)

	Decimals(opts *bind.CallOpts) (uint8, error)

	Name(opts *bind.CallOpts) (string, error)

	Symbol(opts *bind.CallOpts) (string, error)

	TotalSupply(opts *bind.CallOpts) (*big.Int, error)

	Approve(opts *bind.TransactOpts, guy common.Address, wad *big.Int) (*types.Transaction, error)

	Deposit(opts *bind.TransactOpts) (*types.Transaction, error)

	Transfer(opts *bind.TransactOpts, dst common.Address, wad *big.Int) (*types.Transaction, error)

	TransferFrom(opts *bind.TransactOpts, src common.Address, dst common.Address, wad *big.Int) (*types.Transaction, error)

	Withdraw(opts *bind.TransactOpts, wad *big.Int) (*types.Transaction, error)

	Fallback(opts *bind.TransactOpts, calldata []byte) (*types.Transaction, error)

	FilterApproval(opts *bind.FilterOpts, src []common.Address, guy []common.Address) (*WKCSApprovalIterator, error)

	WatchApproval(opts *bind.WatchOpts, sink chan<- *WKCSApproval, src []common.Address, guy []common.Address) (event.Subscription, error)

	ParseApproval(log types.Log) (*WKCSApproval, error)

	FilterDeposit(opts *bind.FilterOpts, dst []common.Address) (*WKCSDepositIterator, error)

	WatchDeposit(opts *bind.WatchOpts, sink chan<- *WKCSDeposit, dst []common.Address) (event.Subscription, error)

	ParseDeposit(log types.Log) (*WKCSDeposit, error)

	FilterTransfer(opts *bind.FilterOpts, src []common.Address, dst []common.Address) (*WKCSTransferIterator, error)

	WatchTransfer(opts *bind.WatchOpts, sink chan<- *WKCSTransfer, src []common.Address, dst []common.Address) (event.Subscription, error)

	ParseTransfer(log types.Log) (*WKCSTransfer, error)

	FilterWithdrawal(opts *bind.FilterOpts, src []common.Address) (*WKCSWithdrawalIterator, error)

	WatchWithdrawal(opts *bind.WatchOpts, sink chan<- *WKCSWithdrawal, src []common.Address) (event.Subscription, error)

	ParseWithdrawal(log types.Log) (*WKCSWithdrawal, error)

	ParseLog(log types.Log) (generated.AbigenLog, error)

	Address() common.Address
}
