// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package imojitopair

import (
	"errors"
	"fmt"
	"math/big"
	"strings"

	"gitee.com/tfxq/swap-go-sdk/internal/gethwrappers/generated"
	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

var IMojitoPairMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"sender\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount0\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount1\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"Burn\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"sender\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount0\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount1\",\"type\":\"uint256\"}],\"name\":\"Mint\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"sender\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount0In\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount1In\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount0Out\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount1Out\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"Swap\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint112\",\"name\":\"reserve0\",\"type\":\"uint112\"},{\"indexed\":false,\"internalType\":\"uint112\",\"name\":\"reserve1\",\"type\":\"uint112\"}],\"name\":\"Sync\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"DOMAIN_SEPARATOR\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"MINIMUM_LIQUIDITY\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"PERMIT_TYPEHASH\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"}],\"name\":\"allowance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"burn\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"amount0\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount1\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"decimals\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"factory\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"feeToDenominator\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getReserves\",\"outputs\":[{\"internalType\":\"uint112\",\"name\":\"reserve0\",\"type\":\"uint112\"},{\"internalType\":\"uint112\",\"name\":\"reserve1\",\"type\":\"uint112\"},{\"internalType\":\"uint32\",\"name\":\"blockTimestampLast\",\"type\":\"uint32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"initialize\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"kLast\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"mint\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"liquidity\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"name\":\"nonces\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"deadline\",\"type\":\"uint256\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"name\":\"permit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"price0CumulativeLast\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"price1CumulativeLast\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_feeToDenominator\",\"type\":\"uint256\"}],\"name\":\"setFeeToDenominator\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_swapFeeNumerator\",\"type\":\"uint256\"}],\"name\":\"setSwapFeeNumerator\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"}],\"name\":\"skim\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amount0Out\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount1Out\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"data\",\"type\":\"bytes\"}],\"name\":\"swap\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"swapFeeNumerator\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"sync\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"token0\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"token1\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

var IMojitoPairABI = IMojitoPairMetaData.ABI

type IMojitoPair struct {
	address common.Address
	abi     abi.ABI
	IMojitoPairCaller
	IMojitoPairTransactor
	IMojitoPairFilterer
}

type IMojitoPairCaller struct {
	contract *bind.BoundContract
}

type IMojitoPairTransactor struct {
	contract *bind.BoundContract
}

type IMojitoPairFilterer struct {
	contract *bind.BoundContract
}

type IMojitoPairSession struct {
	Contract     *IMojitoPair
	CallOpts     bind.CallOpts
	TransactOpts bind.TransactOpts
}

type IMojitoPairCallerSession struct {
	Contract *IMojitoPairCaller
	CallOpts bind.CallOpts
}

type IMojitoPairTransactorSession struct {
	Contract     *IMojitoPairTransactor
	TransactOpts bind.TransactOpts
}

type IMojitoPairRaw struct {
	Contract *IMojitoPair
}

type IMojitoPairCallerRaw struct {
	Contract *IMojitoPairCaller
}

type IMojitoPairTransactorRaw struct {
	Contract *IMojitoPairTransactor
}

func NewIMojitoPair(address common.Address, backend bind.ContractBackend) (*IMojitoPair, error) {
	abi, err := abi.JSON(strings.NewReader(IMojitoPairABI))
	if err != nil {
		return nil, err
	}
	contract, err := bindIMojitoPair(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IMojitoPair{address: address, abi: abi, IMojitoPairCaller: IMojitoPairCaller{contract: contract}, IMojitoPairTransactor: IMojitoPairTransactor{contract: contract}, IMojitoPairFilterer: IMojitoPairFilterer{contract: contract}}, nil
}

func NewIMojitoPairCaller(address common.Address, caller bind.ContractCaller) (*IMojitoPairCaller, error) {
	contract, err := bindIMojitoPair(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IMojitoPairCaller{contract: contract}, nil
}

func NewIMojitoPairTransactor(address common.Address, transactor bind.ContractTransactor) (*IMojitoPairTransactor, error) {
	contract, err := bindIMojitoPair(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IMojitoPairTransactor{contract: contract}, nil
}

func NewIMojitoPairFilterer(address common.Address, filterer bind.ContractFilterer) (*IMojitoPairFilterer, error) {
	contract, err := bindIMojitoPair(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IMojitoPairFilterer{contract: contract}, nil
}

func bindIMojitoPair(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IMojitoPairABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

func (_IMojitoPair *IMojitoPairRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IMojitoPair.Contract.IMojitoPairCaller.contract.Call(opts, result, method, params...)
}

func (_IMojitoPair *IMojitoPairRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IMojitoPair.Contract.IMojitoPairTransactor.contract.Transfer(opts)
}

func (_IMojitoPair *IMojitoPairRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IMojitoPair.Contract.IMojitoPairTransactor.contract.Transact(opts, method, params...)
}

func (_IMojitoPair *IMojitoPairCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IMojitoPair.Contract.contract.Call(opts, result, method, params...)
}

func (_IMojitoPair *IMojitoPairTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IMojitoPair.Contract.contract.Transfer(opts)
}

func (_IMojitoPair *IMojitoPairTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IMojitoPair.Contract.contract.Transact(opts, method, params...)
}

func (_IMojitoPair *IMojitoPairCaller) DOMAINSEPARATOR(opts *bind.CallOpts) ([32]byte, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "DOMAIN_SEPARATOR")

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) DOMAINSEPARATOR() ([32]byte, error) {
	return _IMojitoPair.Contract.DOMAINSEPARATOR(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) DOMAINSEPARATOR() ([32]byte, error) {
	return _IMojitoPair.Contract.DOMAINSEPARATOR(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) MINIMUMLIQUIDITY(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "MINIMUM_LIQUIDITY")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) MINIMUMLIQUIDITY() (*big.Int, error) {
	return _IMojitoPair.Contract.MINIMUMLIQUIDITY(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) MINIMUMLIQUIDITY() (*big.Int, error) {
	return _IMojitoPair.Contract.MINIMUMLIQUIDITY(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) PERMITTYPEHASH(opts *bind.CallOpts) ([32]byte, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "PERMIT_TYPEHASH")

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) PERMITTYPEHASH() ([32]byte, error) {
	return _IMojitoPair.Contract.PERMITTYPEHASH(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) PERMITTYPEHASH() ([32]byte, error) {
	return _IMojitoPair.Contract.PERMITTYPEHASH(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) Allowance(opts *bind.CallOpts, owner common.Address, spender common.Address) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "allowance", owner, spender)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) Allowance(owner common.Address, spender common.Address) (*big.Int, error) {
	return _IMojitoPair.Contract.Allowance(&_IMojitoPair.CallOpts, owner, spender)
}

func (_IMojitoPair *IMojitoPairCallerSession) Allowance(owner common.Address, spender common.Address) (*big.Int, error) {
	return _IMojitoPair.Contract.Allowance(&_IMojitoPair.CallOpts, owner, spender)
}

func (_IMojitoPair *IMojitoPairCaller) BalanceOf(opts *bind.CallOpts, owner common.Address) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "balanceOf", owner)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) BalanceOf(owner common.Address) (*big.Int, error) {
	return _IMojitoPair.Contract.BalanceOf(&_IMojitoPair.CallOpts, owner)
}

func (_IMojitoPair *IMojitoPairCallerSession) BalanceOf(owner common.Address) (*big.Int, error) {
	return _IMojitoPair.Contract.BalanceOf(&_IMojitoPair.CallOpts, owner)
}

func (_IMojitoPair *IMojitoPairCaller) Decimals(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "decimals")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) Decimals() (uint8, error) {
	return _IMojitoPair.Contract.Decimals(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) Decimals() (uint8, error) {
	return _IMojitoPair.Contract.Decimals(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) Factory(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "factory")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) Factory() (common.Address, error) {
	return _IMojitoPair.Contract.Factory(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) Factory() (common.Address, error) {
	return _IMojitoPair.Contract.Factory(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) FeeToDenominator(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "feeToDenominator")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) FeeToDenominator() (*big.Int, error) {
	return _IMojitoPair.Contract.FeeToDenominator(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) FeeToDenominator() (*big.Int, error) {
	return _IMojitoPair.Contract.FeeToDenominator(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) GetReserves(opts *bind.CallOpts) (GetReserves,

	error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "getReserves")

	outstruct := new(GetReserves)
	if err != nil {
		return *outstruct, err
	}

	outstruct.Reserve0 = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.Reserve1 = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.BlockTimestampLast = *abi.ConvertType(out[2], new(uint32)).(*uint32)

	return *outstruct, err

}

func (_IMojitoPair *IMojitoPairSession) GetReserves() (GetReserves,

	error) {
	return _IMojitoPair.Contract.GetReserves(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) GetReserves() (GetReserves,

	error) {
	return _IMojitoPair.Contract.GetReserves(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) KLast(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "kLast")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) KLast() (*big.Int, error) {
	return _IMojitoPair.Contract.KLast(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) KLast() (*big.Int, error) {
	return _IMojitoPair.Contract.KLast(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) Name() (string, error) {
	return _IMojitoPair.Contract.Name(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) Name() (string, error) {
	return _IMojitoPair.Contract.Name(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) Nonces(opts *bind.CallOpts, owner common.Address) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "nonces", owner)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) Nonces(owner common.Address) (*big.Int, error) {
	return _IMojitoPair.Contract.Nonces(&_IMojitoPair.CallOpts, owner)
}

func (_IMojitoPair *IMojitoPairCallerSession) Nonces(owner common.Address) (*big.Int, error) {
	return _IMojitoPair.Contract.Nonces(&_IMojitoPair.CallOpts, owner)
}

func (_IMojitoPair *IMojitoPairCaller) Price0CumulativeLast(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "price0CumulativeLast")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) Price0CumulativeLast() (*big.Int, error) {
	return _IMojitoPair.Contract.Price0CumulativeLast(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) Price0CumulativeLast() (*big.Int, error) {
	return _IMojitoPair.Contract.Price0CumulativeLast(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) Price1CumulativeLast(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "price1CumulativeLast")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) Price1CumulativeLast() (*big.Int, error) {
	return _IMojitoPair.Contract.Price1CumulativeLast(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) Price1CumulativeLast() (*big.Int, error) {
	return _IMojitoPair.Contract.Price1CumulativeLast(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) SwapFeeNumerator(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "swapFeeNumerator")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) SwapFeeNumerator() (*big.Int, error) {
	return _IMojitoPair.Contract.SwapFeeNumerator(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) SwapFeeNumerator() (*big.Int, error) {
	return _IMojitoPair.Contract.SwapFeeNumerator(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) Symbol() (string, error) {
	return _IMojitoPair.Contract.Symbol(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) Symbol() (string, error) {
	return _IMojitoPair.Contract.Symbol(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) Token0(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "token0")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) Token0() (common.Address, error) {
	return _IMojitoPair.Contract.Token0(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) Token0() (common.Address, error) {
	return _IMojitoPair.Contract.Token0(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) Token1(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "token1")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) Token1() (common.Address, error) {
	return _IMojitoPair.Contract.Token1(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) Token1() (common.Address, error) {
	return _IMojitoPair.Contract.Token1(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCaller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IMojitoPair.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

func (_IMojitoPair *IMojitoPairSession) TotalSupply() (*big.Int, error) {
	return _IMojitoPair.Contract.TotalSupply(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairCallerSession) TotalSupply() (*big.Int, error) {
	return _IMojitoPair.Contract.TotalSupply(&_IMojitoPair.CallOpts)
}

func (_IMojitoPair *IMojitoPairTransactor) Approve(opts *bind.TransactOpts, spender common.Address, value *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "approve", spender, value)
}

func (_IMojitoPair *IMojitoPairSession) Approve(spender common.Address, value *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Approve(&_IMojitoPair.TransactOpts, spender, value)
}

func (_IMojitoPair *IMojitoPairTransactorSession) Approve(spender common.Address, value *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Approve(&_IMojitoPair.TransactOpts, spender, value)
}

func (_IMojitoPair *IMojitoPairTransactor) Burn(opts *bind.TransactOpts, to common.Address) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "burn", to)
}

func (_IMojitoPair *IMojitoPairSession) Burn(to common.Address) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Burn(&_IMojitoPair.TransactOpts, to)
}

func (_IMojitoPair *IMojitoPairTransactorSession) Burn(to common.Address) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Burn(&_IMojitoPair.TransactOpts, to)
}

func (_IMojitoPair *IMojitoPairTransactor) Initialize(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "initialize", arg0, arg1)
}

func (_IMojitoPair *IMojitoPairSession) Initialize(arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Initialize(&_IMojitoPair.TransactOpts, arg0, arg1)
}

func (_IMojitoPair *IMojitoPairTransactorSession) Initialize(arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Initialize(&_IMojitoPair.TransactOpts, arg0, arg1)
}

func (_IMojitoPair *IMojitoPairTransactor) Mint(opts *bind.TransactOpts, to common.Address) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "mint", to)
}

func (_IMojitoPair *IMojitoPairSession) Mint(to common.Address) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Mint(&_IMojitoPair.TransactOpts, to)
}

func (_IMojitoPair *IMojitoPairTransactorSession) Mint(to common.Address) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Mint(&_IMojitoPair.TransactOpts, to)
}

func (_IMojitoPair *IMojitoPairTransactor) Permit(opts *bind.TransactOpts, owner common.Address, spender common.Address, value *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "permit", owner, spender, value, deadline, v, r, s)
}

func (_IMojitoPair *IMojitoPairSession) Permit(owner common.Address, spender common.Address, value *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Permit(&_IMojitoPair.TransactOpts, owner, spender, value, deadline, v, r, s)
}

func (_IMojitoPair *IMojitoPairTransactorSession) Permit(owner common.Address, spender common.Address, value *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Permit(&_IMojitoPair.TransactOpts, owner, spender, value, deadline, v, r, s)
}

func (_IMojitoPair *IMojitoPairTransactor) SetFeeToDenominator(opts *bind.TransactOpts, _feeToDenominator *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "setFeeToDenominator", _feeToDenominator)
}

func (_IMojitoPair *IMojitoPairSession) SetFeeToDenominator(_feeToDenominator *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.Contract.SetFeeToDenominator(&_IMojitoPair.TransactOpts, _feeToDenominator)
}

func (_IMojitoPair *IMojitoPairTransactorSession) SetFeeToDenominator(_feeToDenominator *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.Contract.SetFeeToDenominator(&_IMojitoPair.TransactOpts, _feeToDenominator)
}

func (_IMojitoPair *IMojitoPairTransactor) SetSwapFeeNumerator(opts *bind.TransactOpts, _swapFeeNumerator *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "setSwapFeeNumerator", _swapFeeNumerator)
}

func (_IMojitoPair *IMojitoPairSession) SetSwapFeeNumerator(_swapFeeNumerator *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.Contract.SetSwapFeeNumerator(&_IMojitoPair.TransactOpts, _swapFeeNumerator)
}

func (_IMojitoPair *IMojitoPairTransactorSession) SetSwapFeeNumerator(_swapFeeNumerator *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.Contract.SetSwapFeeNumerator(&_IMojitoPair.TransactOpts, _swapFeeNumerator)
}

func (_IMojitoPair *IMojitoPairTransactor) Skim(opts *bind.TransactOpts, to common.Address) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "skim", to)
}

func (_IMojitoPair *IMojitoPairSession) Skim(to common.Address) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Skim(&_IMojitoPair.TransactOpts, to)
}

func (_IMojitoPair *IMojitoPairTransactorSession) Skim(to common.Address) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Skim(&_IMojitoPair.TransactOpts, to)
}

func (_IMojitoPair *IMojitoPairTransactor) Swap(opts *bind.TransactOpts, amount0Out *big.Int, amount1Out *big.Int, to common.Address, data []byte) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "swap", amount0Out, amount1Out, to, data)
}

func (_IMojitoPair *IMojitoPairSession) Swap(amount0Out *big.Int, amount1Out *big.Int, to common.Address, data []byte) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Swap(&_IMojitoPair.TransactOpts, amount0Out, amount1Out, to, data)
}

func (_IMojitoPair *IMojitoPairTransactorSession) Swap(amount0Out *big.Int, amount1Out *big.Int, to common.Address, data []byte) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Swap(&_IMojitoPair.TransactOpts, amount0Out, amount1Out, to, data)
}

func (_IMojitoPair *IMojitoPairTransactor) Sync(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "sync")
}

func (_IMojitoPair *IMojitoPairSession) Sync() (*types.Transaction, error) {
	return _IMojitoPair.Contract.Sync(&_IMojitoPair.TransactOpts)
}

func (_IMojitoPair *IMojitoPairTransactorSession) Sync() (*types.Transaction, error) {
	return _IMojitoPair.Contract.Sync(&_IMojitoPair.TransactOpts)
}

func (_IMojitoPair *IMojitoPairTransactor) Transfer(opts *bind.TransactOpts, to common.Address, value *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "transfer", to, value)
}

func (_IMojitoPair *IMojitoPairSession) Transfer(to common.Address, value *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Transfer(&_IMojitoPair.TransactOpts, to, value)
}

func (_IMojitoPair *IMojitoPairTransactorSession) Transfer(to common.Address, value *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.Contract.Transfer(&_IMojitoPair.TransactOpts, to, value)
}

func (_IMojitoPair *IMojitoPairTransactor) TransferFrom(opts *bind.TransactOpts, from common.Address, to common.Address, value *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.contract.Transact(opts, "transferFrom", from, to, value)
}

func (_IMojitoPair *IMojitoPairSession) TransferFrom(from common.Address, to common.Address, value *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.Contract.TransferFrom(&_IMojitoPair.TransactOpts, from, to, value)
}

func (_IMojitoPair *IMojitoPairTransactorSession) TransferFrom(from common.Address, to common.Address, value *big.Int) (*types.Transaction, error) {
	return _IMojitoPair.Contract.TransferFrom(&_IMojitoPair.TransactOpts, from, to, value)
}

type IMojitoPairApprovalIterator struct {
	Event *IMojitoPairApproval

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *IMojitoPairApprovalIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IMojitoPairApproval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(IMojitoPairApproval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *IMojitoPairApprovalIterator) Error() error {
	return it.fail
}

func (it *IMojitoPairApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type IMojitoPairApproval struct {
	Owner   common.Address
	Spender common.Address
	Value   *big.Int
	Raw     types.Log
}

func (_IMojitoPair *IMojitoPairFilterer) FilterApproval(opts *bind.FilterOpts, owner []common.Address, spender []common.Address) (*IMojitoPairApprovalIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var spenderRule []interface{}
	for _, spenderItem := range spender {
		spenderRule = append(spenderRule, spenderItem)
	}

	logs, sub, err := _IMojitoPair.contract.FilterLogs(opts, "Approval", ownerRule, spenderRule)
	if err != nil {
		return nil, err
	}
	return &IMojitoPairApprovalIterator{contract: _IMojitoPair.contract, event: "Approval", logs: logs, sub: sub}, nil
}

func (_IMojitoPair *IMojitoPairFilterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *IMojitoPairApproval, owner []common.Address, spender []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var spenderRule []interface{}
	for _, spenderItem := range spender {
		spenderRule = append(spenderRule, spenderItem)
	}

	logs, sub, err := _IMojitoPair.contract.WatchLogs(opts, "Approval", ownerRule, spenderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(IMojitoPairApproval)
				if err := _IMojitoPair.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_IMojitoPair *IMojitoPairFilterer) ParseApproval(log types.Log) (*IMojitoPairApproval, error) {
	event := new(IMojitoPairApproval)
	if err := _IMojitoPair.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type IMojitoPairBurnIterator struct {
	Event *IMojitoPairBurn

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *IMojitoPairBurnIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IMojitoPairBurn)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(IMojitoPairBurn)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *IMojitoPairBurnIterator) Error() error {
	return it.fail
}

func (it *IMojitoPairBurnIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type IMojitoPairBurn struct {
	Sender  common.Address
	Amount0 *big.Int
	Amount1 *big.Int
	To      common.Address
	Raw     types.Log
}

func (_IMojitoPair *IMojitoPairFilterer) FilterBurn(opts *bind.FilterOpts, sender []common.Address, to []common.Address) (*IMojitoPairBurnIterator, error) {

	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}

	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _IMojitoPair.contract.FilterLogs(opts, "Burn", senderRule, toRule)
	if err != nil {
		return nil, err
	}
	return &IMojitoPairBurnIterator{contract: _IMojitoPair.contract, event: "Burn", logs: logs, sub: sub}, nil
}

func (_IMojitoPair *IMojitoPairFilterer) WatchBurn(opts *bind.WatchOpts, sink chan<- *IMojitoPairBurn, sender []common.Address, to []common.Address) (event.Subscription, error) {

	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}

	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _IMojitoPair.contract.WatchLogs(opts, "Burn", senderRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(IMojitoPairBurn)
				if err := _IMojitoPair.contract.UnpackLog(event, "Burn", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_IMojitoPair *IMojitoPairFilterer) ParseBurn(log types.Log) (*IMojitoPairBurn, error) {
	event := new(IMojitoPairBurn)
	if err := _IMojitoPair.contract.UnpackLog(event, "Burn", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type IMojitoPairMintIterator struct {
	Event *IMojitoPairMint

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *IMojitoPairMintIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IMojitoPairMint)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(IMojitoPairMint)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *IMojitoPairMintIterator) Error() error {
	return it.fail
}

func (it *IMojitoPairMintIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type IMojitoPairMint struct {
	Sender  common.Address
	Amount0 *big.Int
	Amount1 *big.Int
	Raw     types.Log
}

func (_IMojitoPair *IMojitoPairFilterer) FilterMint(opts *bind.FilterOpts, sender []common.Address) (*IMojitoPairMintIterator, error) {

	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}

	logs, sub, err := _IMojitoPair.contract.FilterLogs(opts, "Mint", senderRule)
	if err != nil {
		return nil, err
	}
	return &IMojitoPairMintIterator{contract: _IMojitoPair.contract, event: "Mint", logs: logs, sub: sub}, nil
}

func (_IMojitoPair *IMojitoPairFilterer) WatchMint(opts *bind.WatchOpts, sink chan<- *IMojitoPairMint, sender []common.Address) (event.Subscription, error) {

	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}

	logs, sub, err := _IMojitoPair.contract.WatchLogs(opts, "Mint", senderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(IMojitoPairMint)
				if err := _IMojitoPair.contract.UnpackLog(event, "Mint", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_IMojitoPair *IMojitoPairFilterer) ParseMint(log types.Log) (*IMojitoPairMint, error) {
	event := new(IMojitoPairMint)
	if err := _IMojitoPair.contract.UnpackLog(event, "Mint", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type IMojitoPairSwapIterator struct {
	Event *IMojitoPairSwap

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *IMojitoPairSwapIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IMojitoPairSwap)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(IMojitoPairSwap)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *IMojitoPairSwapIterator) Error() error {
	return it.fail
}

func (it *IMojitoPairSwapIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type IMojitoPairSwap struct {
	Sender     common.Address
	Amount0In  *big.Int
	Amount1In  *big.Int
	Amount0Out *big.Int
	Amount1Out *big.Int
	To         common.Address
	Raw        types.Log
}

func (_IMojitoPair *IMojitoPairFilterer) FilterSwap(opts *bind.FilterOpts, sender []common.Address, to []common.Address) (*IMojitoPairSwapIterator, error) {

	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}

	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _IMojitoPair.contract.FilterLogs(opts, "Swap", senderRule, toRule)
	if err != nil {
		return nil, err
	}
	return &IMojitoPairSwapIterator{contract: _IMojitoPair.contract, event: "Swap", logs: logs, sub: sub}, nil
}

func (_IMojitoPair *IMojitoPairFilterer) WatchSwap(opts *bind.WatchOpts, sink chan<- *IMojitoPairSwap, sender []common.Address, to []common.Address) (event.Subscription, error) {

	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}

	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _IMojitoPair.contract.WatchLogs(opts, "Swap", senderRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(IMojitoPairSwap)
				if err := _IMojitoPair.contract.UnpackLog(event, "Swap", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_IMojitoPair *IMojitoPairFilterer) ParseSwap(log types.Log) (*IMojitoPairSwap, error) {
	event := new(IMojitoPairSwap)
	if err := _IMojitoPair.contract.UnpackLog(event, "Swap", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type IMojitoPairSyncIterator struct {
	Event *IMojitoPairSync

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *IMojitoPairSyncIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IMojitoPairSync)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(IMojitoPairSync)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *IMojitoPairSyncIterator) Error() error {
	return it.fail
}

func (it *IMojitoPairSyncIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type IMojitoPairSync struct {
	Reserve0 *big.Int
	Reserve1 *big.Int
	Raw      types.Log
}

func (_IMojitoPair *IMojitoPairFilterer) FilterSync(opts *bind.FilterOpts) (*IMojitoPairSyncIterator, error) {

	logs, sub, err := _IMojitoPair.contract.FilterLogs(opts, "Sync")
	if err != nil {
		return nil, err
	}
	return &IMojitoPairSyncIterator{contract: _IMojitoPair.contract, event: "Sync", logs: logs, sub: sub}, nil
}

func (_IMojitoPair *IMojitoPairFilterer) WatchSync(opts *bind.WatchOpts, sink chan<- *IMojitoPairSync) (event.Subscription, error) {

	logs, sub, err := _IMojitoPair.contract.WatchLogs(opts, "Sync")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(IMojitoPairSync)
				if err := _IMojitoPair.contract.UnpackLog(event, "Sync", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_IMojitoPair *IMojitoPairFilterer) ParseSync(log types.Log) (*IMojitoPairSync, error) {
	event := new(IMojitoPairSync)
	if err := _IMojitoPair.contract.UnpackLog(event, "Sync", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type IMojitoPairTransferIterator struct {
	Event *IMojitoPairTransfer

	contract *bind.BoundContract
	event    string

	logs chan types.Log
	sub  ethereum.Subscription
	done bool
	fail error
}

func (it *IMojitoPairTransferIterator) Next() bool {

	if it.fail != nil {
		return false
	}

	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IMojitoPairTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}

	select {
	case log := <-it.logs:
		it.Event = new(IMojitoPairTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

func (it *IMojitoPairTransferIterator) Error() error {
	return it.fail
}

func (it *IMojitoPairTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

type IMojitoPairTransfer struct {
	From  common.Address
	To    common.Address
	Value *big.Int
	Raw   types.Log
}

func (_IMojitoPair *IMojitoPairFilterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*IMojitoPairTransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _IMojitoPair.contract.FilterLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &IMojitoPairTransferIterator{contract: _IMojitoPair.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

func (_IMojitoPair *IMojitoPairFilterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *IMojitoPairTransfer, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _IMojitoPair.contract.WatchLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:

				event := new(IMojitoPairTransfer)
				if err := _IMojitoPair.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

func (_IMojitoPair *IMojitoPairFilterer) ParseTransfer(log types.Log) (*IMojitoPairTransfer, error) {
	event := new(IMojitoPairTransfer)
	if err := _IMojitoPair.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

type GetReserves struct {
	Reserve0           *big.Int
	Reserve1           *big.Int
	BlockTimestampLast uint32
}

func (_IMojitoPair *IMojitoPair) ParseLog(log types.Log) (generated.AbigenLog, error) {
	switch log.Topics[0] {
	case _IMojitoPair.abi.Events["Approval"].ID:
		return _IMojitoPair.ParseApproval(log)
	case _IMojitoPair.abi.Events["Burn"].ID:
		return _IMojitoPair.ParseBurn(log)
	case _IMojitoPair.abi.Events["Mint"].ID:
		return _IMojitoPair.ParseMint(log)
	case _IMojitoPair.abi.Events["Swap"].ID:
		return _IMojitoPair.ParseSwap(log)
	case _IMojitoPair.abi.Events["Sync"].ID:
		return _IMojitoPair.ParseSync(log)
	case _IMojitoPair.abi.Events["Transfer"].ID:
		return _IMojitoPair.ParseTransfer(log)

	default:
		return nil, fmt.Errorf("abigen wrapper received unknown log topic: %v", log.Topics[0])
	}
}

func (IMojitoPairApproval) Topic() common.Hash {
	return common.HexToHash("0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925")
}

func (IMojitoPairBurn) Topic() common.Hash {
	return common.HexToHash("0xdccd412f0b1252819cb1fd330b93224ca42612892bb3f4f789976e6d81936496")
}

func (IMojitoPairMint) Topic() common.Hash {
	return common.HexToHash("0x4c209b5fc8ad50758f13e2e1088ba56a560dff690a1c6fef26394f4c03821c4f")
}

func (IMojitoPairSwap) Topic() common.Hash {
	return common.HexToHash("0xd78ad95fa46c994b6551d0da85fc275fe613ce37657fb8d5e3d130840159d822")
}

func (IMojitoPairSync) Topic() common.Hash {
	return common.HexToHash("0x1c411e9a96e071241c2f21f7726b17ae89e3cab4c78be50e062b03a9fffbbad1")
}

func (IMojitoPairTransfer) Topic() common.Hash {
	return common.HexToHash("0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef")
}

func (_IMojitoPair *IMojitoPair) Address() common.Address {
	return _IMojitoPair.address
}

type IMojitoPairInterface interface {
	DOMAINSEPARATOR(opts *bind.CallOpts) ([32]byte, error)

	MINIMUMLIQUIDITY(opts *bind.CallOpts) (*big.Int, error)

	PERMITTYPEHASH(opts *bind.CallOpts) ([32]byte, error)

	Allowance(opts *bind.CallOpts, owner common.Address, spender common.Address) (*big.Int, error)

	BalanceOf(opts *bind.CallOpts, owner common.Address) (*big.Int, error)

	Decimals(opts *bind.CallOpts) (uint8, error)

	Factory(opts *bind.CallOpts) (common.Address, error)

	FeeToDenominator(opts *bind.CallOpts) (*big.Int, error)

	GetReserves(opts *bind.CallOpts) (GetReserves,

		error)

	KLast(opts *bind.CallOpts) (*big.Int, error)

	Name(opts *bind.CallOpts) (string, error)

	Nonces(opts *bind.CallOpts, owner common.Address) (*big.Int, error)

	Price0CumulativeLast(opts *bind.CallOpts) (*big.Int, error)

	Price1CumulativeLast(opts *bind.CallOpts) (*big.Int, error)

	SwapFeeNumerator(opts *bind.CallOpts) (*big.Int, error)

	Symbol(opts *bind.CallOpts) (string, error)

	Token0(opts *bind.CallOpts) (common.Address, error)

	Token1(opts *bind.CallOpts) (common.Address, error)

	TotalSupply(opts *bind.CallOpts) (*big.Int, error)

	Approve(opts *bind.TransactOpts, spender common.Address, value *big.Int) (*types.Transaction, error)

	Burn(opts *bind.TransactOpts, to common.Address) (*types.Transaction, error)

	Initialize(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address) (*types.Transaction, error)

	Mint(opts *bind.TransactOpts, to common.Address) (*types.Transaction, error)

	Permit(opts *bind.TransactOpts, owner common.Address, spender common.Address, value *big.Int, deadline *big.Int, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error)

	SetFeeToDenominator(opts *bind.TransactOpts, _feeToDenominator *big.Int) (*types.Transaction, error)

	SetSwapFeeNumerator(opts *bind.TransactOpts, _swapFeeNumerator *big.Int) (*types.Transaction, error)

	Skim(opts *bind.TransactOpts, to common.Address) (*types.Transaction, error)

	Swap(opts *bind.TransactOpts, amount0Out *big.Int, amount1Out *big.Int, to common.Address, data []byte) (*types.Transaction, error)

	Sync(opts *bind.TransactOpts) (*types.Transaction, error)

	Transfer(opts *bind.TransactOpts, to common.Address, value *big.Int) (*types.Transaction, error)

	TransferFrom(opts *bind.TransactOpts, from common.Address, to common.Address, value *big.Int) (*types.Transaction, error)

	FilterApproval(opts *bind.FilterOpts, owner []common.Address, spender []common.Address) (*IMojitoPairApprovalIterator, error)

	WatchApproval(opts *bind.WatchOpts, sink chan<- *IMojitoPairApproval, owner []common.Address, spender []common.Address) (event.Subscription, error)

	ParseApproval(log types.Log) (*IMojitoPairApproval, error)

	FilterBurn(opts *bind.FilterOpts, sender []common.Address, to []common.Address) (*IMojitoPairBurnIterator, error)

	WatchBurn(opts *bind.WatchOpts, sink chan<- *IMojitoPairBurn, sender []common.Address, to []common.Address) (event.Subscription, error)

	ParseBurn(log types.Log) (*IMojitoPairBurn, error)

	FilterMint(opts *bind.FilterOpts, sender []common.Address) (*IMojitoPairMintIterator, error)

	WatchMint(opts *bind.WatchOpts, sink chan<- *IMojitoPairMint, sender []common.Address) (event.Subscription, error)

	ParseMint(log types.Log) (*IMojitoPairMint, error)

	FilterSwap(opts *bind.FilterOpts, sender []common.Address, to []common.Address) (*IMojitoPairSwapIterator, error)

	WatchSwap(opts *bind.WatchOpts, sink chan<- *IMojitoPairSwap, sender []common.Address, to []common.Address) (event.Subscription, error)

	ParseSwap(log types.Log) (*IMojitoPairSwap, error)

	FilterSync(opts *bind.FilterOpts) (*IMojitoPairSyncIterator, error)

	WatchSync(opts *bind.WatchOpts, sink chan<- *IMojitoPairSync) (event.Subscription, error)

	ParseSync(log types.Log) (*IMojitoPairSync, error)

	FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*IMojitoPairTransferIterator, error)

	WatchTransfer(opts *bind.WatchOpts, sink chan<- *IMojitoPairTransfer, from []common.Address, to []common.Address) (event.Subscription, error)

	ParseTransfer(log types.Log) (*IMojitoPairTransfer, error)

	ParseLog(log types.Log) (generated.AbigenLog, error)

	Address() common.Address
}
