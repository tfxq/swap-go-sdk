package info

import "math/big"

var (
	GasPriceMultiPoint float64
	ChainId            *big.Int
	// GasPrice 直接指定gasPrice
	GasPrice int64
)
