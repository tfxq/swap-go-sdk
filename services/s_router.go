package services

import (
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/services/s_common"
	"gitee.com/tfxq/swap-go-sdk/services/s_mojito"
	"github.com/ethereum/go-ethereum/common"
	"math/big"
)

type GetAmountsOutResultModel struct {
	Out []string `json:"out"`
}

// GetAmountsOut 通过输入代币以及数量获取可输出多少
func (as *AppService) GetAmountsOut(contractAddr string, amountIn *big.Int, path []common.Address) (*ApiResponse, error) {

	err, out := s_mojito.GetAmountsOut(as.rpcClient, contractAddr, amountIn, path)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	var outString []string
	for _, o := range out {
		outString = append(outString, o.String())
	}
	return OkWithData(GetAmountsOutResultModel{
		Out: outString,
	})
}

type GetAmountsInResultModel struct {
	Out []string `json:"out"`
}

// GetAmountsIn 通过指定输出代币以及数量获取输入数量
func (as *AppService) GetAmountsIn(contractAddr string, amountOut *big.Int, path []common.Address) (*ApiResponse, error) {

	err, out := s_mojito.GetAmountsIn(as.rpcClient, contractAddr, amountOut, path)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	var outString []string
	for _, o := range out {
		outString = append(outString, o.String())
	}
	return OkWithData(GetAmountsInResultModel{
		Out: outString,
	})
}

func (as *AppService) GetAmountsOutAndAmountsInByMulticall(amount *big.Int, path1, path2 []common.Address) (*big.Int, *big.Int, error) {
	buy, sell, err := s_mojito.GetAmountsOutAndAmountsInByMulticall(as.rpcClient, as.Info.Chain.Multicall2, as.Info.Mojito.MojitoRouter, amount, path1, path2)
	if err != nil {
		return nil, nil, err
	}
	return buy, sell, nil
}

func (as *AppService) GetAmountsOutAndAmountsInByMultiCallAndAmountArray(amount []*big.Int, path1, path2 []common.Address) ([][]*big.Int, error) {
	array, err := s_mojito.GetAmountsOutAndAmountsInByMultiCallAndAmountArray(as.rpcClient, as.Info.Chain.Multicall2, as.Info.Mojito.MojitoRouter, amount, path1, path2)
	if err != nil {
		return nil, err
	}
	return array, nil
}

type SwapExactTokensForTokensResultModel struct {
	TxHash string `json:"txHash"`
}

// SwapExactTokensForTokens 用准确数量的token0代币换token1代币
func (as *AppService) SwapExactTokensForTokens(amountIn, amountOutMin *big.Int, path []common.Address, userAddr string) (*ApiResponse, error) {

	err, txHash := s_mojito.SwapExactTokensForTokens(as.rpcClient, as.privateKey, as.Info.Mojito.MojitoRouter, amountIn, amountOutMin, path, common.HexToAddress(userAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactTokensForTokensAndVerify 用准确数量的token0代币换token1代币，并校验用户token余额
func (as *AppService) SwapExactTokensForTokensAndVerify(amountIn, amountOutMin *big.Int, path []common.Address, userAddr string) (*ApiResponse, error) {
	// 判断输出数量固定的token1时，输入token0的🈷余额
	err, balance := s_common.GetErc20Balance(as.rpcClient, path[0].Hex(), userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	if balance.Cmp(big.NewInt(0)) <= 0 {
		m := fmt.Sprintf("insufficient erc20 balance,addr: %v", path[0].Hex())
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}

	err, txHash := s_mojito.SwapExactTokensForTokens(as.rpcClient, as.privateKey, as.Info.Mojito.MojitoRouter, amountIn, amountOutMin, path, common.HexToAddress(userAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactTokensForTokensAndTo 用准确数量的token0代币换token1代币
func (as *AppService) SwapExactTokensForTokensAndTo(amountIn, amountOutMin *big.Int, path []common.Address, userAddr, toAddr string) (*ApiResponse, error) {

	err, txHash := s_mojito.SwapExactTokensForTokens(as.rpcClient, as.privateKey, as.Info.Mojito.MojitoRouter, amountIn, amountOutMin, path, common.HexToAddress(toAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactTokensForTokensAndToAndVerify 用准确数量的token0代币换token1代币，并校验用户token余额
func (as *AppService) SwapExactTokensForTokensAndToAndVerify(amountIn, amountOutMin *big.Int, path []common.Address, userAddr, toAddr string) (*ApiResponse, error) {
	// 判断输出数量固定的token1时，输入token0的🈷余额
	err, balance := s_common.GetErc20Balance(as.rpcClient, path[0].Hex(), userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	if balance.Cmp(big.NewInt(0)) <= 0 {
		m := fmt.Sprintf("insufficient erc20 balance,addr: %v", path[0].Hex())
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}

	err, txHash := s_mojito.SwapExactTokensForTokens(as.rpcClient, as.privateKey, as.Info.Mojito.MojitoRouter, amountIn, amountOutMin, path, common.HexToAddress(toAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapTokensForExactTokens 用的token0代币换准确数量的token1币
func (as *AppService) SwapTokensForExactTokens(amountOut, amountInMax *big.Int, path []common.Address, userAddr string) (*ApiResponse, error) {

	// 判断输出数量固定的token1时，输入token0的🈷余额
	err, balance := s_common.GetErc20Balance(as.rpcClient, path[0].Hex(), userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	if balance.Cmp(big.NewInt(0)) <= 0 {
		m := fmt.Sprintf("insufficient erc20 balance,addr: %v", path[0].Hex())
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}
	// 输入最大花费多少
	err, txHash := s_mojito.SwapTokensForExactTokens(as.rpcClient, as.privateKey, as.Info.Mojito.MojitoRouter, amountOut, amountInMax, path, common.HexToAddress(userAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapTokensForExactTokensAndTo 用的token0代币换准确数量的token1币
func (as *AppService) SwapTokensForExactTokensAndTo(amountOut, amountInMax *big.Int, path []common.Address, userAddr, toAddr string) (*ApiResponse, error) {

	// 判断输出数量固定的token1时，输入token0的🈷余额
	err, balance := s_common.GetErc20Balance(as.rpcClient, path[0].Hex(), userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	if balance.Cmp(big.NewInt(0)) <= 0 {
		m := fmt.Sprintf("insufficient erc20 balance,addr: %v", path[0].Hex())
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}
	// 输入最大花费多少
	err, txHash := s_mojito.SwapTokensForExactTokens(as.rpcClient, as.privateKey, as.Info.Mojito.MojitoRouter, amountOut, amountInMax, path, common.HexToAddress(toAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapTokensForExactETH 用的token0代币换准确数量的原生币
func (as *AppService) SwapTokensForExactETH(amountOut, amountInMax *big.Int, path []common.Address, userAddr string) (*ApiResponse, error) {

	// 判断输出数量固定的token1时，输入token0的🈷余额
	err, balance := s_common.GetErc20Balance(as.rpcClient, path[0].Hex(), userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	if balance.Cmp(big.NewInt(0)) <= 0 {
		m := fmt.Sprintf("insufficient erc20 balance,addr: %v", path[0].Hex())
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}
	// 输入最大花费多少
	err, txHash := s_mojito.SwapTokensForExactETH(as.rpcClient, as.privateKey, as.Info.Mojito.MojitoRouter, amountOut, amountInMax, path, common.HexToAddress(userAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactETHForTokens 用的准确数量的原生币换token1代币
func (as *AppService) SwapExactETHForTokens(value, amountOutMin *big.Int, path []common.Address, userAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.SwapExactETHForTokens(as.rpcClient, as.privateKey, as.Info.Mojito.MojitoRouter, value, amountOutMin, path, common.HexToAddress(userAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactETHForTokensAndTo 用的准确数量的原生币换token1代币
func (as *AppService) SwapExactETHForTokensAndTo(value, amountOutMin *big.Int, path []common.Address, toAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.SwapExactETHForTokens(as.rpcClient, as.privateKey, as.Info.Mojito.MojitoRouter, value, amountOutMin, path, common.HexToAddress(toAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactETHForTokensAndToAndVerify 用的准确数量的原生币换token1代币
func (as *AppService) SwapExactETHForTokensAndToAndVerify(value, amountOutMin *big.Int, path []common.Address, userAddr, toAddr string) (*ApiResponse, error) {

	// 判断输出数量固定的token1时，输入token0的🈷余额
	balance, err := as.GetNativeBalance(userAddr)
	if err != nil {
		m := fmt.Sprintf("chain inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeChainInquiryException, m)
	}
	if balance.Cmp(value) <= 0 {
		m := fmt.Sprintf("insufficient native token balance")
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}
	// 输入最大花费多少
	err, txHash := s_mojito.SwapExactETHForTokens(as.rpcClient, as.privateKey, as.Info.Mojito.MojitoRouter, value, amountOutMin, path, common.HexToAddress(toAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// 带router的

// SwapExactTokensForTokensAndToRouter 用准确数量的token0代币换token1代币
func (as *AppService) SwapExactTokensForTokensAndToRouter(amountIn, amountOutMin *big.Int, path []common.Address, userAddr, toAddr, routerAddr string) (*ApiResponse, error) {

	err, txHash := s_mojito.SwapExactTokensForTokens(as.rpcClient, as.privateKey, routerAddr, amountIn, amountOutMin, path, common.HexToAddress(toAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactETHForTokensAndToRouter 用的准确数量的原生币换token1代币
func (as *AppService) SwapExactETHForTokensAndToRouter(value, amountOutMin *big.Int, path []common.Address, toAddr, routerAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.SwapExactETHForTokens(as.rpcClient, as.privateKey, routerAddr, value, amountOutMin, path, common.HexToAddress(toAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapTokensForExactTokensAndToRouter 用的token0代币换准确数量的token1币
func (as *AppService) SwapTokensForExactTokensAndToRouter(amountOut, amountInMax *big.Int, path []common.Address, userAddr, toAddr, routerAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.SwapTokensForExactTokens(as.rpcClient, as.privateKey, routerAddr, amountOut, amountInMax, path, common.HexToAddress(toAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapTokensForExactETHAndRouter 用的token0代币换准确数量的原生币
func (as *AppService) SwapTokensForExactETHAndRouter(amountOut, amountInMax *big.Int, path []common.Address, userAddr, routerAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.SwapTokensForExactETH(as.rpcClient, as.privateKey, routerAddr, amountOut, amountInMax, path, common.HexToAddress(userAddr), GetDeadline().BigInt())
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

func (as *AppService) GetAmountsOutAndAmountsInByMultiCallAndAmountArrayAndRouter(amount []*big.Int, path1, path2 []common.Address, routerAddr string) ([][]*big.Int, error) {
	array, err := s_mojito.GetAmountsOutAndAmountsInByMultiCallAndAmountArray(as.rpcClient, as.Info.Chain.Multicall2, routerAddr, amount, path1, path2)
	if err != nil {
		return nil, err
	}
	return array, nil
}
