package services

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
)

const (
	ApiSuccess                   = "200000"
	CodeContractExecutionErr     = "300001"
	CodeContractInquiryException = "300002"
	CodeBalanceInsufficient      = "300003"
	CodeChainInquiryException    = "300004"
	CodeToDecimalErr             = "400001"
	CodeConvertKeyToAddrErr      = "400002"
)

type ApiResponse struct {
	Code    string          `json:"code"`
	RawData json.RawMessage `json:"data"` // delay parsing
	Message string          `json:"msg"`
}

// ApiSuccessful judges the success of API.
func (ar *ApiResponse) ApiSuccessful() bool {
	return ar.Code == ApiSuccess
}

func OkWithData(rawData interface{}) (*ApiResponse, error) {
	marshal, err := json.Marshal(rawData)
	if err != nil {
		return nil, err
	}
	ar := &ApiResponse{Code: ApiSuccess, Message: "", RawData: marshal}
	return ar, err
}

func FailWithCodeAndMessage(code string, msg string) (*ApiResponse, error) {
	ar := &ApiResponse{Code: code, Message: msg}
	return ar, nil
}

// ReadData read the api response `data` as JSON into v.
func (ar *ApiResponse) ReadData(v interface{}) error {

	if !ar.ApiSuccessful() {
		m := fmt.Sprintf("[API]Failure: api code is NOT %s,respond code=%s message=\"%s\" data=%s",
			ApiSuccess,
			ar.Code,
			ar.Message,
			string(ar.RawData),
		)
		return errors.New(m)
	}
	// when input parameter v is nil, read nothing and return nil
	if v == nil {
		return nil
	}

	if len(ar.RawData) == 0 {
		m := fmt.Sprintf("[API]Failure: try to read empty data, respond code=%s message=\"%s\" data=%s",
			ar.Code,
			ar.Message,
			string(ar.RawData),
		)
		return errors.New(m)
	}

	return json.Unmarshal(ar.RawData, v)
}
