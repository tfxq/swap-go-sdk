package services

import (
	"gitee.com/tfxq/swap-go-sdk/services/s_mojito"
	"math/big"
)

func (as *AppService) GetPendingMojitoReward(account string) (*big.Int, error) {
	sumReward, err := s_mojito.GetPendingMojitoReward(as.rpcClient, account, as.Info.Mojito.SwapMiningAddr)
	if err != nil {
		return nil, err
	}
	return sumReward, nil
}

func (as *AppService) ClaimShakerReward() (string, error) {
	err, tx := s_mojito.ClaimShakerReward(as.rpcClient, as.privateKey, as.Info.Mojito.SwapMiningAddr, as.Info.Token.MJTAddress)
	if err != nil {
		return "", err
	}
	return tx, nil
}
