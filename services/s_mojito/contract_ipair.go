package s_mojito

import (
	"gitee.com/tfxq/swap-go-sdk/solcs/sc_mojito/imojitopair"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"math/big"
)

func newMojitoPair(ethClient *ethclient.Client, contractID string) (*imojitopair.IMojitoPair, error) {
	contractAddress := common.HexToAddress(contractID)
	contract, err := imojitopair.NewIMojitoPair(contractAddress, ethClient)
	if err != nil {
		return nil, errors.Wrap(err, "init mojito pair err")
	}
	return contract, nil
}

func GetPairBalance(rpcClient *ethclient.Client, contractAddr string, userAddr string) (error, *big.Int) {

	pairContract, err := newMojitoPair(rpcClient, contractAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoPair err"), big.NewInt(0)
	}

	balance, err := pairContract.BalanceOf(nil, common.HexToAddress(userAddr))
	if err != nil {
		return errors.Wrap(err, "BalanceOf err"), big.NewInt(0)
	}
	return nil, balance
}

func GetPairTotalSupply(rpcClient *ethclient.Client, contractAddr string) (error, *big.Int) {

	pairContract, err := newMojitoPair(rpcClient, contractAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoPair err"), big.NewInt(0)
	}

	totalSupply, err := pairContract.TotalSupply(nil)
	if err != nil {
		return errors.Wrap(err, "TotalSupply err"), big.NewInt(0)
	}
	return nil, totalSupply
}
