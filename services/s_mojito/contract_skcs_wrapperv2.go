package s_mojito

import (
	"context"
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/internal/gethwrappers2/generated/skcswrapperv2"
	"gitee.com/tfxq/swap-go-sdk/services/multicall"
	"gitee.com/tfxq/swap-go-sdk/utils"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"math/big"
	"strconv"
	"strings"
	"time"
)

var skcsWrapperAbi, _ = abi.JSON(strings.NewReader(skcswrapperv2.SKCSWrapperV2ABI))

func newSKCSWrapperV2(ethClient *ethclient.Client, contractID string) (*skcswrapperv2.SKCSWrapperV2, error) {
	contractAddress := common.HexToAddress(contractID)
	contract, err := skcswrapperv2.NewSKCSWrapperV2(contractAddress, ethClient)
	if err != nil {
		return nil, errors.Wrap(err, "init err")
	}
	return contract, nil
}

// SafeSKCSSwap
// https://scan-testnet.kcc.network/tx/0x169b00284c0f9d4d4ed82bda0615ad09490ce2758b608a77c776cae275ea3412
func SafeSKCSSwap(client *ethclient.Client, privateKey, skcsWrapperAddr string, usdtAddr common.Address, borrowAmount *big.Int, path []common.Address, minProfitAmount *big.Int) (error, string) {
	contractInstance, err := newSKCSWrapperV2(client, skcsWrapperAddr)
	if err != nil {
		return err, ""
	}
	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	//0xc9d5c3c9
	//000000000000000000000000c175efeea53d331e8a319c115d9626836bf405ce
	//00000000000000000000000000000000000000000000000000000000000003e8
	//0000000000000000000000000000000000000000000000000000000000000080
	//000000000000000000000000000000000000000000000000000000000000007b
	//0000000000000000000000000000000000000000000000000000000000000002
	//000000000000000000000000311dd61df0e88ddc6803e7353f5d9b71522aeda9
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	methodSig := []byte("safeSKCSSwap(address,uint256,address[],uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	// usdt addr
	data = append(data, common.LeftPadBytes(usdtAddr.Bytes(), 32)...)
	// borrowAmount
	data = append(data, common.LeftPadBytes(borrowAmount.Bytes(), 32)...)
	// 数组
	parseUint, err := strconv.ParseUint(strconv.Itoa(80), 16, 32)
	if err != nil {
		return err, ""
	}
	// 80
	data = append(data, common.LeftPadBytes([]byte{byte(parseUint)}, 32)...)
	// minProfitAmount
	data = append(data, common.LeftPadBytes(minProfitAmount.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}
	sendTo := common.HexToAddress(skcsWrapperAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "estimate gas err"), ""
	}
	auth.GasLimit = gasLimit * 4

	//return nil, ""
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.SafeSKCSSwap(
		auth,
		usdtAddr,
		borrowAmount,
		path,
		minProfitAmount,
	)
	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}

		event, err2 := contractInstance.ParseTokenSwitchedLPP(*l)

		if err2 != nil {
			return errors.Wrap(err2, "parse transfer err"), tx.Hash().Hex()
		}
		if event.DepositAmount.Cmp(borrowAmount) == 0 {
			return nil, tx.Hash().Hex()
		}

	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

func GetSKCSLensArray(rpc string, multicallAddr string, skcsWrapperAddr string, amountArray []*big.Int, router1, router2, router3 common.Address, path1, path2, path3 []common.Address, minProfitAmount *big.Int) ([]*SKCSResult, error) {
	caller := multicall.NewContractBuilder().
		WithChainConfig(multicall.ChainConfig{
			MultiCallAddress: multicallAddr,
			Url:              rpc,
		}).
		AddMethodJson(skcsWrapperAbi)

	for k, v := range amountArray {
		caller.AddCall("path1"+strconv.Itoa(k), skcsWrapperAddr,
			"getLens", big.NewInt(1), router1, v, path1, minProfitAmount) // id router amount path minProfitAmount
		caller.AddCall("path2"+strconv.Itoa(k), skcsWrapperAddr,
			"getLens", big.NewInt(2), router2, v, path2, minProfitAmount)
		caller.AddCall("path3"+strconv.Itoa(k), skcsWrapperAddr,
			"getLens", big.NewInt(3), router3, v, path3, minProfitAmount)
	}

	results, err := caller.FlexibleCall(false)

	if err != nil {
		return nil, err
	}

	var returnValue []*SKCSResult
	for _, result := range results {
		success := result.Success
		if success {
			out := result.ReturnData

			out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
			out1 := *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
			out2 := *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)
			out3 := *abi.ConvertType(out[3], new(bool)).(*bool)
			//result1 := *abi.ConvertType(result.ReturnData[0], new([]SKCSResult)).(*[]SKCSResult)
			//fmt.Println(out0, utils.ParseEther(out1), utils.ParseEther(out2), out3)
			returnValue = append(returnValue, &SKCSResult{
				out0,
				out1,
				out2,
				out3,
			})
		} else {
			return nil, errors.New("call fail")
		}
	}
	return returnValue, nil
}

type SKCSResult struct {
	IdKey  *big.Int
	Before *big.Int
	After  *big.Int
	IsOK   bool
}
