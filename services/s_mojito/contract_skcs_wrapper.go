package s_mojito

import (
	"context"
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/internal/gethwrappers2/generated/skcswrapperv2"
	"gitee.com/tfxq/swap-go-sdk/utils"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"math/big"
	"time"
)

func newSKCSWrapper(ethClient *ethclient.Client, contractID string) (*skcswrapperv2.SKCSWrapperV2, error) {
	contractAddress := common.HexToAddress(contractID)
	contract, err := skcswrapperv2.NewSKCSWrapperV2(contractAddress, ethClient)
	if err != nil {
		return nil, errors.Wrap(err, "init skcswrapper err")
	}
	return contract, nil
}

// DepositKCSAndSwapAndLPP
// https://scan-testnet.kcc.network/tx/0x201832f02d73004ea52a8bea58cd6fc3a7e2d92091ed075764777daf535f3556
func DepositKCSAndSwapAndLPP(client *ethclient.Client, privateKey, skcsWrapperAddr string, value, txFee *big.Int, path []common.Address, to common.Address) (error, string) {
	contractInstance, err := newSKCSWrapper(client, skcsWrapperAddr)
	if err != nil {
		return err, ""
	}
	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	auth.Value = value
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)
	////balance>1e18
	//balance, err := client.BalanceAt(context.Background(), userAddress, nil)
	//if err != nil {
	//	return errors.Wrap(err, "BalanceAt err"), ""
	//}
	////大于0.005个 5e15=0.005e18
	//if balance.Cmp(decimal.NewFromFloat(1.2).Mul(decimal.New(1, 18)).BigInt()) < 0 {
	//	return errors.New("insufficient native balance"), ""
	//}

	//0x5b1a1569
	//0000000000000000000000000000000000000000000000000000000000000060
	//00000000000000000000000084d238ebb0cb0bf5bca49e448dc73bf69b8cda6c
	//000000000000000000000000000000000000000000000000016345785d8a0000
	//0000000000000000000000000000000000000000000000000000000000000002
	//000000000000000000000000311dd61df0e88ddc6803e7353f5d9b71522aeda9
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	methodSig := []byte("depositKCSAndSwapAndLPP(address[],address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	data = append(data, common.LeftPadBytes([]byte{96}, 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// tx fee
	data = append(data, common.LeftPadBytes(txFee.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}
	sendTo := common.HexToAddress(skcsWrapperAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "estimate gas err"), ""
	}
	auth.GasLimit = gasLimit * 4

	//return nil, ""
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.DepositKCSAndSwapAndLPP(
		auth,
		path,
		to,
		txFee,
	)
	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}

		event, err2 := contractInstance.ParseTokenSwitchedLPP(*l)

		if err2 != nil {
			return errors.Wrap(err2, "parse transfer err"), tx.Hash().Hex()
		}
		if event.DepositAmount.Cmp(auth.Value) == 0 {
			return nil, tx.Hash().Hex()
		}

	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

func DepositKCSAndSwapAndLPPV2(client *ethclient.Client, privateKey, skcsWrapperAddr string, value, txFee *big.Int, path []common.Address, to, router common.Address) (error, string) {
	contractInstance, err := newSKCSWrapper(client, skcsWrapperAddr)
	if err != nil {
		return err, ""
	}
	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	auth.Value = value
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)
	////balance>1e18
	//balance, err := client.BalanceAt(context.Background(), userAddress, nil)
	//if err != nil {
	//	return errors.Wrap(err, "BalanceAt err"), ""
	//}
	////大于0.005个 5e15=0.005e18
	//if balance.Cmp(decimal.NewFromFloat(1.2).Mul(decimal.New(1, 18)).BigInt()) < 0 {
	//	return errors.New("insufficient native balance"), ""
	//}

	//0x7b7bcc1a
	//00000000000000000000000059a4210dd69fdde1457905098ff03e0617a548c5
	//0000000000000000000000000000000000000000000000000000000000000080
	//00000000000000000000000084d238ebb0cb0bf5bca49e448dc73bf69b8cda6c
	//0000000000000000000000000000000000000000000000000000000000000000
	//0000000000000000000000000000000000000000000000000000000000000002
	//000000000000000000000000311dd61df0e88ddc6803e7353f5d9b71522aeda9
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	methodSig := []byte("depositKCSAndSwapAndLPPV2(address,address[],address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	data = append(data, common.LeftPadBytes(router.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes([]byte{128}, 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// tx fee
	data = append(data, common.LeftPadBytes(txFee.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}
	sendTo := common.HexToAddress(skcsWrapperAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "estimate gas err"), ""
	}
	auth.GasLimit = gasLimit * 4

	//return nil, ""
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.DepositKCSAndSwapAndLPPV2(
		auth,
		router,
		path,
		to,
		txFee,
	)
	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}

		event, err2 := contractInstance.ParseTokenSwitchedLPP(*l)

		if err2 != nil {
			return errors.Wrap(err2, "parse transfer err"), tx.Hash().Hex()
		}
		if event.DepositAmount.Cmp(auth.Value) == 0 {
			return nil, tx.Hash().Hex()
		}

	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}
