package s_mojito

import (
	"context"
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/services/s_common"
	"gitee.com/tfxq/swap-go-sdk/solcs/sc_mojito/mojitorouter"
	"gitee.com/tfxq/swap-go-sdk/utils"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"math/big"
	"strconv"
	"strings"
	"time"
)

var mojitoRouterAbi, _ = abi.JSON(strings.NewReader(mojitorouter.MojitoRouterABI))

func newMojitoRouter(ethClient *ethclient.Client, contractID string) (*mojitorouter.MojitoRouter, error) {
	contractAddress := common.HexToAddress(contractID)
	contract, err := mojitorouter.NewMojitoRouter(contractAddress, ethClient)
	if err != nil {
		return nil, errors.Wrap(err, "init mojito router err")
	}
	return contract, nil
}

func GetAmountsOutCall(name, targetAddr string, amountIn *big.Int, path1 []common.Address) (s_common.Call, error) {
	out0Data1, err := mojitoRouterAbi.Pack("getAmountsOut", amountIn, path1)
	if err != nil {
		return s_common.Call{}, err
	}
	return s_common.Call{
		Target:   common.HexToAddress(targetAddr),
		CallData: out0Data1,
		Name:     name,
	}, nil
}

func GetAmountsInCall(name, targetAddr string, amountOut *big.Int, path1 []common.Address) (s_common.Call, error) {
	out0Data1, err := mojitoRouterAbi.Pack("getAmountsIn", amountOut, path1)
	if err != nil {
		return s_common.Call{}, err
	}
	return s_common.Call{
		Target:   common.HexToAddress(targetAddr),
		CallData: out0Data1,
		Name:     name,
	}, nil
}

func GetAmountsOutCallResponse(callResponse s_common.CallResponse) ([]*big.Int, error) {
	if callResponse.Success {
		var result1 []*big.Int
		err := mojitoRouterAbi.UnpackIntoInterface(&result1, "getAmountsOut", callResponse.ReturnData)
		if err != nil {
			return nil, err
		}
		return result1, nil
	}
	return nil, errors.New("call response fail")
}

func GetAmountsInCallResponse(callResponse s_common.CallResponse) ([]*big.Int, error) {
	if callResponse.Success {
		var result1 []*big.Int
		err := mojitoRouterAbi.UnpackIntoInterface(&result1, "getAmountsIn", callResponse.ReturnData)
		if err != nil {
			return nil, err
		}
		return result1, nil
	}
	return nil, errors.New("call response fail")
}

func GetAmountsOutAndAmountsInByMultiCallAndAmountArray(client *ethclient.Client, multicallAddr string, mojitoRouterAddr string, amountArray []*big.Int, path1, path2 []common.Address) ([][]*big.Int, error) {
	caller := s_common.NewMulticall2Provider(client, multicallAddr)

	var userCalls = make([]s_common.Call, 2)
	for k, v := range amountArray {
		getAmountsOutCall, err := GetAmountsOutCall("getAmountsOut"+strconv.Itoa(k), mojitoRouterAddr, v, path1)
		if err != nil {
			return nil, err
		}

		getAmountsInCall, err := GetAmountsInCall("getAmountsIn"+strconv.Itoa(k), mojitoRouterAddr, v, path2)
		if err != nil {
			return nil, err
		}

		userCalls = append(userCalls, getAmountsOutCall)
		userCalls = append(userCalls, getAmountsInCall)
	}

	response, err := caller.Execute(userCalls)
	if err != nil {
		return nil, err
	}

	var result [][]*big.Int

	for k := range amountArray {
		result1, err2 := GetAmountsOutCallResponse(response["getAmountsOut"+strconv.Itoa(k)])
		if err2 != nil {
			return nil, err2
		}

		result2, err2 := GetAmountsInCallResponse(response["getAmountsIn"+strconv.Itoa(k)])
		if err2 != nil {
			return nil, err2
		}

		row1 := []*big.Int{result1[len(result1)-1], result2[0]}
		result = append(result, row1)

		if len(result1) <= 1 || len(result2) <= 1 {
			return nil, errors.New("path length error")
		}
	}

	return result, nil
}

func GetAmountsOutAndAmountsInByMulticall(client *ethclient.Client, multicallAddr string, mojitoRouterAddr string, amount *big.Int, path1, path2 []common.Address) (*big.Int, *big.Int, error) {
	caller := s_common.NewMulticall2Provider(client, multicallAddr)

	getAmountsOutCall, err := GetAmountsOutCall("getAmountsOut", mojitoRouterAddr, amount, path1)
	if err != nil {
		return nil, nil, err
	}

	getAmountsInCall, err := GetAmountsInCall("getAmountsIn", mojitoRouterAddr, amount, path2)
	if err != nil {
		return nil, nil, err
	}

	var userCalls = make([]s_common.Call, 2)
	userCalls = append(userCalls, getAmountsOutCall)
	userCalls = append(userCalls, getAmountsInCall)

	response, err := caller.Execute(userCalls)
	if err != nil {
		return nil, nil, err
	}

	result1, err := GetAmountsOutCallResponse(response["getAmountsOut"])
	if err != nil {
		return nil, nil, err
	}

	result2, err := GetAmountsInCallResponse(response["getAmountsIn"])
	if err != nil {
		return nil, nil, err
	}

	if len(result1) <= 1 || len(result2) <= 1 {
		return nil, nil, errors.New("path length error")
	}

	return result1[len(result1)-1], result2[0], nil
}

func GetAmountsOut(client *ethclient.Client, mojitoRouterAddr string, amountIn *big.Int, path []common.Address) (error, []*big.Int) {

	mojitoRouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), nil
	}

	out, err := mojitoRouterContract.GetAmountsOut(nil, amountIn, path)
	if err != nil {
		return errors.Wrap(err, "get amounts out err"), nil
	}

	return nil, out
}

func GetAmountsIn(client *ethclient.Client, mojitoRouterAddr string, amountOut *big.Int, path []common.Address) (error, []*big.Int) {
	mojitoRouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), nil
	}

	out, err := mojitoRouterContract.GetAmountsIn(nil, amountOut, path)
	if err != nil {
		return errors.Wrap(err, "get amounts out err"), nil
	}

	//logger.SugarLogger.Infof("输入数量：%v 输出数量：%v", utils.ParseEther(out[0]), utils.ParseEther(out[1]))
	return nil, out
}

// SwapExactTokensForTokens 2 个币种相互交换
// https://scan.kcc.io/tx/0xfeaaa70317ab9cd1358f6bf16a27972a1dc83ac208a7e724d0f7f6f2488bfcdf/logs
// https://scan.kcc.io/tx/0xe60457fc00161f51f83f5b73483640ab6937e134f98772773455499e403e66f4/logs
func SwapExactTokensForTokens(client *ethclient.Client, privateKey string, mojitoRouterAddr string, amountIn *big.Int, amountOutMin *big.Int, path []common.Address, to common.Address, deadline *big.Int) (error, string) {

	mojitoRouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)
	//balance>1e18
	balance, err := client.BalanceAt(context.Background(), userAddress, nil)
	if err != nil {
		return errors.Wrap(err, "BalanceAt err"), ""
	}
	//大于0.005个 5e15=0.005e18
	if balance.Cmp(decimal.NewFromBigInt(big.NewInt(5), 15).BigInt()) < 0 {
		return errors.New("insufficient native balance"), ""
	}

	methodSig := []byte("swapExactTokensForTokens(uint256,uint256,address[],address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	// amountIn
	data = append(data, common.LeftPadBytes(amountIn.Bytes(), 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountOutMin.Bytes(), 32)...)
	// 160
	data = append(data, common.LeftPadBytes([]byte{160}, 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// deadline
	data = append(data, common.LeftPadBytes(deadline.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(mojitoRouterAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5
	//return nil, "gasLimit"

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := mojitoRouterContract.SwapExactTokensForTokens(
		auth,
		amountIn,
		amountOutMin,
		path,
		to,
		deadline,
	)
	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		//查看是否有path中第一个地址的转账日志，如果有就是交易成功
		pathOne := path[0]
		if l.Address.Hex() != pathOne.Hex() {
			continue
		}

		ercContract, err2 := s_common.NewERC20(client, pathOne.String())
		if err2 != nil {
			return errors.Wrap(err2, "NewERC20 err"), ""
		}
		event, err3 := ercContract.ParseTransfer(*l)
		if err3 != nil {
			return errors.Wrap(err3, "parse transfer err"), tx.Hash().Hex()
		}
		if event.From.Hex() == userAddress.Hex() || event.To.Hex() == userAddress.Hex() {
			return nil, tx.Hash().Hex()
		}

	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// SwapExactTokensForETH token换kcs
// https://scan-testnet.kcc.network/tx/0xd9c20ff0f9c7ef0efcd68214fdc3e8ade2ecd59c8f562f76222ff1d35d0ef7c3/logs
func SwapExactTokensForETH(client *ethclient.Client, mojitoRouterAddr string, amountIn *big.Int, amountOutMin *big.Int, path []common.Address, to common.Address, deadline *big.Int, privateKey string) (error, string) {

	mojitoRouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)
	//balance>1e18
	//balance, err := client.BalanceAt(context.Background(), userAddress)
	//if err != nil {
	//	return errors.Wrap(err,"BalanceAt"),""
	//}
	// 大于0.5个
	//if balance.Cmp(decimal.NewFromBigInt(big.NewInt(5),17).BigInt()) < 0 {
	//	return errors.New("insufficient balance"),""
	//}

	methodSig := []byte("swapExactTokensForETH(uint256,uint256,address[],address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0x18cbafe5
	//00000000000000000000000000000000000000000000000029a2241af62c0000
	//0000000000000000000000000000000000000000000000008483f4d54820aba2
	//00000000000000000000000000000000000000000000000000000000000000a0
	//00000000000000000000000084d238ebb0cb0bf5bca49e448dc73bf69b8cda6c
	//00000000000000000000000000000000000000000000000000000000624d972c
	//0000000000000000000000000000000000000000000000000000000000000002
	//00000000000000000000000067f6a7bbe0da067a747c6b2bedf8abbf7d6f60dc
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	var data []byte
	data = append(data, methodID...)
	// amountIn
	data = append(data, common.LeftPadBytes(amountIn.Bytes(), 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountOutMin.Bytes(), 32)...)
	// 160
	data = append(data, common.LeftPadBytes([]byte{160}, 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// deadline
	data = append(data, common.LeftPadBytes(deadline.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(mojitoRouterAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := mojitoRouterContract.SwapExactTokensForETH(
		auth,
		amountIn,
		amountOutMin,
		path,
		to,
		deadline,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != path[0].Hex() {
			continue
		}
		ercContract, err2 := s_common.NewERC20(client, path[0].Hex())
		if err2 != nil {
			return errors.Wrap(err2, "NewERC20 err"), ""
		}
		event, err3 := ercContract.ParseTransfer(*l)

		if err3 != nil {
			return errors.Wrap(err3, "parse transfer err"), ""
		}
		if event.From.Hex() == userAddress.Hex() {
			return nil, tx.Hash().Hex()
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// SwapTokensForExactETH 用token0换精确的原生币
func SwapTokensForExactETH(client *ethclient.Client, privateKey string, mojitoRouterAddr string, amountOut *big.Int, amountInMax *big.Int, path []common.Address, to common.Address, deadline *big.Int) (error, string) {

	mojitoRouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("swapTokensForExactETH(uint256,uint256,address[],address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0x4a25d94a
	//0000000000000000000000000000000000000000000000000de0b6b3a7640000
	//000000000000000000000000000000000000000000000000054585fb01577aae
	//00000000000000000000000000000000000000000000000000000000000000a0
	//00000000000000000000000084d238ebb0cb0bf5bca49e448dc73bf69b8cda6c
	//000000000000000000000000000000000000000000000000000000006279fada
	//0000000000000000000000000000000000000000000000000000000000000004
	//00000000000000000000000067f6a7bbe0da067a747c6b2bedf8abbf7d6f60dc
	//000000000000000000000000d6c7e27a598714c2226404eb054e0c074c906fc9
	//000000000000000000000000208eecdbc49c137d0174b848def5f8cb74d6951e
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	var data []byte
	data = append(data, methodID...)
	// amountIn
	data = append(data, common.LeftPadBytes(amountOut.Bytes(), 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountInMax.Bytes(), 32)...)
	// 160
	data = append(data, common.LeftPadBytes([]byte{160}, 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// deadline
	data = append(data, common.LeftPadBytes(deadline.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(mojitoRouterAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := mojitoRouterContract.SwapTokensForExactETH(
		auth,
		amountOut,
		amountInMax,
		path,
		to,
		deadline,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != path[0].Hex() {
			continue
		}
		ercContract, err := s_common.NewERC20(client, path[0].Hex())
		if err != nil {
			return errors.Wrap(err, "NewERC20 err"), ""
		}
		event, err := ercContract.ParseTransfer(*l)

		if err != nil {
			return errors.Wrap(err, "parse transfer err"), ""
		}
		if event.From.Hex() == userAddress.Hex() {
			return nil, tx.Hash().Hex()
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// SwapExactETHForTokens 用精确的原生币换token1
// https://scan-testnet.kcc.network/tx/0x952ebf957fbedee6527ec3adc770e8e39a6fb2fef1eaf152da474b1983816eb6/internal-transactions
func SwapExactETHForTokens(client *ethclient.Client, privateKey string, mojitoRouterAddr string, value *big.Int, amountOutMin *big.Int, path []common.Address, to common.Address, deadline *big.Int) (error, string) {

	mojitoRouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("swapExactETHForTokens(uint256,address[],address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountOutMin.Bytes(), 32)...)
	// 160
	data = append(data, common.LeftPadBytes([]byte{128}, 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// deadline
	data = append(data, common.LeftPadBytes(deadline.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(mojitoRouterAddr)

	auth.Value = value
	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := mojitoRouterContract.SwapExactETHForTokens(
		auth,
		amountOutMin,
		path,
		to,
		deadline,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != path[0].Hex() {
			continue
		}
		ercContract, err := s_common.NewWBaseToken(client, path[0].Hex())
		if err != nil {
			return errors.Wrap(err, "NewERC20 err"), ""
		}
		event, err1 := ercContract.ParseDeposit(*l)
		if err1 != nil {
			continue
			//return errors.Wrap(err1, "parse transfer err"), ""
		}
		if event.Dst.Hex() == mojitoRouterContract.Address().Hex() {
			return nil, tx.Hash().Hex()
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

func SwapTokensForExactTokens(client *ethclient.Client, privateKey string, mojitoRouterAddr string, amountOut *big.Int, amountInMax *big.Int, path []common.Address, to common.Address, deadline *big.Int) (error, string) {

	mojitoRouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)
	//balance>1e18
	balance, err := client.BalanceAt(context.Background(), userAddress, nil)
	if err != nil {
		return errors.Wrap(err, "BalanceAt err"), ""
	}
	//大于0.005个 5e15=0.005e18
	if balance.Cmp(decimal.NewFromBigInt(big.NewInt(5), 15).BigInt()) < 0 {
		return errors.New("insufficient native balance"), ""
	}

	methodSig := []byte("swapTokensForExactTokens(uint256,uint256,address[],address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	// amountIn
	data = append(data, common.LeftPadBytes(amountOut.Bytes(), 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountInMax.Bytes(), 32)...)
	// 160
	data = append(data, common.LeftPadBytes([]byte{160}, 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// deadline
	data = append(data, common.LeftPadBytes(deadline.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(mojitoRouterAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5
	//return nil, "gasLimit"

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := mojitoRouterContract.SwapTokensForExactTokens(
		auth,
		amountOut,
		amountInMax,
		path,
		to,
		deadline,
	)
	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		//查看是否有path中第一个地址的转账日志，如果有就是交易成功
		pathOne := path[0]
		if l.Address.Hex() != pathOne.Hex() {
			continue
		}

		ercContract, err := s_common.NewERC20(client, pathOne.String())
		if err != nil {
			return errors.Wrap(err, "NewERC20 err"), ""
		}
		event, err := ercContract.ParseTransfer(*l)

		if err != nil {
			return errors.Wrap(err, "parse transfer err"), tx.Hash().Hex()
		}
		if event.From.Hex() == userAddress.Hex() || event.To.Hex() == userAddress.Hex() {
			return nil, tx.Hash().Hex()
		}

	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// AddLiquidityETH https://scan.kcc.io/tx/0x831aa7a0987cde6c82a04449e0ada9719a4ddafe78cb06603345369da31986ad/logs
func AddLiquidityETH(client *ethclient.Client, mojitoRouterAddr string, token common.Address, amountTokenDesired *big.Int, amountTokenMin *big.Int, amountETHMin *big.Int, to common.Address, deadline, value *big.Int, privateKey string) (error, string) {

	mojitoRouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("addLiquidityETH(address,uint256,uint256,uint256,address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0xf305d719
	//000000000000000000000000208eecdbc49c137d0174b848def5f8cb74d6951e
	//0000000000000000000000000000000000000000000000000a3fd00bd3c0b1c9
	//0000000000000000000000000000000000000000000000000a2ad27a53152b3e
	//0000000000000000000000000000000000000000000000000dc44abe81300000
	//00000000000000000000000084d238ebb0cb0bf5bca49e448dc73bf69b8cda6c
	//00000000000000000000000000000000000000000000000000000000624d9c86

	var data []byte
	data = append(data, methodID...)
	data = append(data, common.LeftPadBytes(token.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountTokenDesired.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountTokenMin.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountETHMin.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(deadline.Bytes(), 32)...)

	sendTo := common.HexToAddress(mojitoRouterAddr)

	auth.Value = value
	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5
	//return nil, ""

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := mojitoRouterContract.AddLiquidityETH(
		auth,
		token,
		amountTokenDesired,
		amountTokenMin,
		amountETHMin,
		to,
		deadline,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().String())
		return errors.Wrap(err, msg), ""
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	tokenContract, err := s_common.NewERC20(client, token.Hex())
	if err != nil {
		return errors.Wrap(err, "init err"), ""
	}
	for _, l := range receipt.Logs {
		//查看是否有path中第一个地址的转账日志，如果有就是交易成功

		if l.Address.Hex() != tokenContract.Address().Hex() {
			continue
		}
		event, err := tokenContract.ParseTransfer(*l)
		if err != nil {
			return errors.Wrap(err, "parse transfer err"), ""
		}

		if event.From.Hex() == to.Hex() {
			return nil, tx.Hash().String()
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// AddLiquidity https://scan.kcc.io/tx/0x831aa7a0987cde6c82a04449e0ada9719a4ddafe78cb06603345369da31986ad/logs
func AddLiquidity(client *ethclient.Client, mojitoRouterAddr string, LpPairAddr common.Address, tokenA common.Address, tokenB common.Address, amountADesired *big.Int, amountBDesired *big.Int, amountAMin *big.Int, amountBMin *big.Int, to common.Address, deadline *big.Int, privateKey string) (error, string) {

	mojitoRouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("addLiquidity(address,address,uint256,uint256,uint256,uint256,address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	data = append(data, common.LeftPadBytes(tokenA.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(tokenB.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountADesired.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountBDesired.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountAMin.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountBMin.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(userAddress.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(deadline.Bytes(), 32)...)

	sendTo := common.HexToAddress(mojitoRouterAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5
	//return nil, ""

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := mojitoRouterContract.AddLiquidity(
		auth,
		tokenA,
		tokenB,
		amountADesired,
		amountBDesired,
		amountAMin,
		amountBMin,
		to,
		deadline,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().String())
		return errors.Wrap(err, msg), ""
	}
	if receipt.Status == 0 {
		return errors.Wrap(err, "request err,receipt status is zero"), ""
	}
	pairContract, err := newMojitoPair(client, LpPairAddr.String())
	if err != nil {
		return errors.Wrap(err, "init err"), ""
	}
	for _, l := range receipt.Logs {
		//查看是否有path中第一个地址的转账日志，如果有就是交易成功

		if l.Address.Hex() != pairContract.Address().Hex() {
			continue
		}
		event, err := pairContract.ParseTransfer(*l)

		if err != nil {
			return errors.Wrap(err, "parse transfer err"), ""
		}
		if event.To.Hex() == userAddress.Hex() {
			return nil, tx.Hash().String()
		}
	}
	return nil, tx.Hash().String()
}

// RemoveLiquidityWithPermit https://scan.kcc.io/tx/0x0a088339dfdcdb62bbe7d85a84a7af36501939bd088324599b4c8972cbfcf2d0/token-transfers
func RemoveLiquidityWithPermit(client *ethclient.Client, mojitoRouterAddr string, LpPairAddr common.Address, tokenA common.Address, tokenB common.Address, liquidity *big.Int, amountAMin *big.Int, amountBMin *big.Int, to common.Address, deadline *big.Int, approveMax bool, v uint8, r [32]byte, s [32]byte, privateKey string) (error, string) {

	mojitorouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("removeLiquidityWithPermit(address,address,uint256,uint256,uint256,address,uint256,bool,uint8,bytes32,bytes32)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	data = append(data, common.LeftPadBytes(tokenA.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(tokenB.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(liquidity.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountAMin.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountBMin.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(deadline.Bytes(), 32)...)
	if approveMax == true {
		data = append(data, common.LeftPadBytes(big.NewInt(1).Bytes(), 32)...)
	} else {
		data = append(data, common.LeftPadBytes(big.NewInt(0).Bytes(), 32)...)
	}

	data = append(data, common.LeftPadBytes(big.NewInt(int64(v)).Bytes(), 32)...)
	data = append(data, r[:]...)
	data = append(data, s[:]...)

	sendTo := common.HexToAddress(mojitoRouterAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := mojitorouterContract.RemoveLiquidityWithPermit(
		auth,
		tokenA,
		tokenB,
		liquidity,
		amountAMin,
		amountBMin,
		to,
		deadline,
		approveMax,
		v,
		r,
		s,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().String())
		return errors.Wrap(err, msg), ""
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		//查看是否有path中第一个地址的转账日志，如果有就是交易成功
		if l.Address.Hex() != LpPairAddr.Hex() {
			continue
		}
		pairContract, err := newMojitoPair(client, LpPairAddr.String())
		if err != nil {
			return errors.Wrap(err, "init err"), ""
		}
		event, err := pairContract.ParseBurn(*l)

		if err != nil {
			return errors.Wrap(err, "parse mint err"), ""
		}
		if event.To.Hex() == userAddress.Hex() {
			return nil, tx.Hash().String()
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// RemoveLiquidityWithPermitNoSign https://scan.kcc.io/tx/0x0a088339dfdcdb62bbe7d85a84a7af36501939bd088324599b4c8972cbfcf2d0/token-transfers
// https://www.cnblogs.com/cqvoip/p/13852705.html
func RemoveLiquidityWithPermitNoSign(client *ethclient.Client, mojitoRouterAddr string, LpPairAddr common.Address, tokenA common.Address, tokenB common.Address, liquidity *big.Int, amountAMin *big.Int, amountBMin *big.Int, to common.Address, deadline *big.Int, approveMax bool, privateKey string) (error, string) {

	mojitoRouterContract, err := newMojitoRouter(client, mojitoRouterAddr)
	if err != nil {
		return errors.Wrap(err, "newMojitoRouter err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	// function permit(address owner, address spender, uint value, uint deadline, uint8 v, bytes32 r, bytes32 s) external {
	var value *big.Int
	if approveMax == false {
		value = liquidity
	}

	pairContract, err := newMojitoPair(client, LpPairAddr.String())
	if err != nil {
		return errors.Wrap(err, "init newMojitoPair err"), ""
	}
	pairNonces, err := pairContract.Nonces(nil, userAddress)
	if err != nil {
		return errors.Wrap(err, "pair Nonces err"), ""
	}

	digest := calcDigest(LpPairAddr, userAddress, mojitoRouterContract.Address(), value, pairNonces, deadline)

	err, r, s, v := utils.Sign(digest.Bytes(), privateKey)
	if err != nil {
		return errors.Wrap(err, "sign err"), ""
	}

	methodSig := []byte("removeLiquidityWithPermit(address,address,uint256,uint256,uint256,address,uint256,bool,uint8,bytes32,bytes32)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	data = append(data, common.LeftPadBytes(tokenA.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(tokenB.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(liquidity.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountAMin.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(amountBMin.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	data = append(data, common.LeftPadBytes(deadline.Bytes(), 32)...)
	if approveMax == true {
		data = append(data, common.LeftPadBytes(big.NewInt(1).Bytes(), 32)...)
	} else {
		data = append(data, common.LeftPadBytes(big.NewInt(0).Bytes(), 32)...)
	}

	data = append(data, common.LeftPadBytes(big.NewInt(int64(v)).Bytes(), 32)...)
	data = append(data, r[:]...)
	data = append(data, s[:]...)

	sendTo := common.HexToAddress(mojitoRouterAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5
	//return nil, ""

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := mojitoRouterContract.RemoveLiquidityWithPermit(
		auth,
		tokenA,
		tokenB,
		liquidity,
		amountAMin,
		amountBMin,
		to,
		deadline,
		approveMax,
		v,
		r,
		s,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().String())
		return errors.Wrap(err, msg), ""
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		//查看是否有path中第一个地址的转账日志，如果有就是交易成功
		if l.Address.Hex() != LpPairAddr.Hex() {
			continue
		}

		event, err := pairContract.ParseApproval(*l)

		if err != nil {
			return errors.Wrap(err, "parse approval err"), ""
		}
		if event.Owner.Hex() == userAddress.Hex() {
			if event.Value.Cmp(value) == 0 {
				return nil, tx.Hash().String()
			}
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

/**
 * verifyingContract:lp地址
 * owner:用户地址
 * spender: mojitoRouter地址
 */
func calcDigest(lpPairAddr, owner, spender common.Address, value, nonce, deadline *big.Int) common.Hash {
	//logger.SugarLogger.Infof("calcDigestPermitFuncHash %v", hexutil.Encode(calcDigestPermitFuncHash(owner, spender, value, nonce, deadline).Bytes()))
	digest := crypto.Keccak256Hash(
		[]byte("\x19\x01"),
		calcDomainSeparatorHash(lpPairAddr).Bytes(),
		calcDigestPermitFuncHash(owner, spender, value, nonce, deadline).Bytes(),
	)
	return digest
}

func calcDomainSeparatorHash(verifyingContract common.Address) common.Hash {
	nameHash := crypto.Keccak256Hash([]byte("Mojito LPs"))
	versionHash := crypto.Keccak256Hash([]byte("1"))

	//TODO
	chainId := big.NewInt(322).Bytes()
	EIP712DomainHash := crypto.Keccak256Hash([]byte("EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)"))
	return crypto.Keccak256Hash(
		EIP712DomainHash.Bytes(),
		nameHash.Bytes(),
		versionHash.Bytes(),
		common.LeftPadBytes(chainId, 32),
		common.LeftPadBytes(verifyingContract.Bytes(), 32),
	)
}

func calcDigestPermitFuncHash(owner, spender common.Address, value *big.Int, nonce *big.Int, deadline *big.Int) common.Hash {
	//TODO kcc 主网和测试网均一样 其他网络请注意
	permitTypeHash := "0x6e71edae12b1b97f4d1f60370fef10105fa2faae0126114a169c64845d6126c9"
	return crypto.Keccak256Hash(
		// keccak256("Permit(address owner,address spender,uint256 value,uint256 nonce,uint256 deadline)");
		common.HexToHash(permitTypeHash).Bytes(),
		common.LeftPadBytes(owner.Bytes(), 32),
		common.LeftPadBytes(spender.Bytes(), 32),
		common.LeftPadBytes(value.Bytes(), 32),
		common.LeftPadBytes(nonce.Bytes(), 32),
		common.LeftPadBytes(deadline.Bytes(), 32),
	)
}
