package s_mojito

import (
	"context"
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/services/s_common"
	"gitee.com/tfxq/swap-go-sdk/solcs/sc_mojito/swapmining"
	"gitee.com/tfxq/swap-go-sdk/utils"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"math/big"
)

func newSwapMining(ethClient *ethclient.Client, contractID string) (*swapmining.SwapMining, error) {
	contractAddress := common.HexToAddress(contractID)
	contract, err := swapmining.NewSwapMining(contractAddress, ethClient)
	if err != nil {
		return nil, errors.Wrap(err, "init newSwapMining err")
	}
	return contract, nil
}

func GetPendingMojitoReward(client *ethclient.Client, userAddr string, swapMiningContractAddr string) (*big.Int, error) {
	swapMiningContract, err := newSwapMining(client, swapMiningContractAddr)
	if err != nil {
		return nil, err
	}
	lpLength, err := swapMiningContract.PoolLength(nil)
	if err != nil {
		return nil, err
	}

	sumReward := decimal.NewFromInt(0)
	for i := int64(0); i < lpLength.Int64(); i++ {
		mojitoReward, err2 := swapMiningContract.PendingMojito(&bind.CallOpts{From: common.HexToAddress(userAddr)}, big.NewInt(i))
		if err2 != nil {
			return nil, err2
		}
		sumReward = sumReward.Add(decimal.NewFromBigInt(mojitoReward, 0))
	}
	return sumReward.BigInt(), nil
}

// ClaimShakerReward 获取交易奖励
//https://scan.kcc.io/tx/0x3904cfab1fd43e96506e8767fb1f8fe0522c20c561891ac59188ca6fef0750d2/logs
func ClaimShakerReward(client *ethclient.Client, privateKey string, swapMiningContractAddr, mjtToken string) (error, string) {

	swapMiningContract, err := newSwapMining(client, swapMiningContractAddr)
	if err != nil {
		return err, ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("withdraw()")
	methodID := crypto.Keccak256(methodSig)[:4]
	var data []byte
	data = append(data, methodID...)
	sendTo := common.HexToAddress(swapMiningContractAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit

	tx, err := swapMiningContract.Withdraw(auth)
	if err != nil {
		return errors.Wrap(err, "Withdraw err"), ""
	}

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().String())
		return errors.Wrap(err, msg), ""
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		//查看调用地址是否一致
		// mjtContract
		mjtTokenAddr := common.HexToAddress(mjtToken)
		if l.Address.Hex() != mjtTokenAddr.Hex() {
			continue
		}
		ercContract, err := s_common.NewERC20(client, mjtTokenAddr.String())
		if err != nil {
			return errors.Wrap(err, "NewERC20 err"), ""
		}
		event, err := ercContract.ParseTransfer(*l)

		if err != nil {
			return errors.Wrap(err, "ParseTransfer err"), ""
		}

		//校验user地址是否一样
		if event.To.Hex() == userAddress.Hex() {
			//logger.SugarLogger.Infof("tx ok , event: %v", event)
			return nil, tx.Hash().String()
		} else {
			continue
		}

	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}
