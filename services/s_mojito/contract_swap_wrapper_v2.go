package s_mojito

import (
	"context"
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/internal/gethwrappers2/generated/swapwrapperv2"
	"gitee.com/tfxq/swap-go-sdk/utils"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"math/big"
	"strconv"
	"time"
)

func newSwapWrapperV2Provider(ethClient *ethclient.Client, contractID string) (*swapwrapperv2.SwapWrapperV2, error) {
	contractAddress := common.HexToAddress(contractID)
	contract, err := swapwrapperv2.NewSwapWrapperV2(contractAddress, ethClient)
	if err != nil {
		return nil, errors.Wrap(err, "init swapwrapper err")
	}
	return contract, nil
}

// SwapTokensForExactTokensAndLPPV2 用tokenA换精确的tokenB
// https://scan-testnet.kcc.network/tx/0xd10c526c8098dcd75d06379ff7b07b893b6a79468cb0ac1c0c23f068c45694ac/token-transfers
func SwapTokensForExactTokensAndLPPV2(client *ethclient.Client, privateKey string, swapWrapperAddr string, amountOut *big.Int, amountInMax *big.Int, path []common.Address, to, router common.Address) (error, string) {

	contractInstance, err := newSwapWrapperV2Provider(client, swapWrapperAddr)
	if err != nil {
		return err, ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)
	//balance>1e18
	balance, err := client.BalanceAt(context.Background(), userAddress, nil)
	if err != nil {
		return errors.Wrap(err, "BalanceAt err"), ""
	}
	//大于0.005个 5e15=0.005e18
	if balance.Cmp(decimal.NewFromBigInt(big.NewInt(5), 15).BigInt()) < 0 {
		return errors.New("insufficient native balance"), ""
	}

	methodSig := []byte("swapTokensForExactTokensAndLPP(address,uint256,address[],uint256,address)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0x156aff7b
	//0000000000000000000000000000000000000000000000000000000000000001
	//0000000000000000000000000000000000000000000000000000000000000080
	//0000000000000000000000000000000000000000000000000000000000989680
	//0000000000000000000000000237ce5fcc88afca1d8a5a752f6aa4f5d77ca64d
	//0000000000000000000000000000000000000000000000000000000000000002
	//000000000000000000000000208eecdbc49c137d0174b848def5f8cb74d6951e
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	var data []byte
	data = append(data, methodID...)
	// router addr
	data = append(data, common.LeftPadBytes(router.Bytes(), 32)...)
	// amountIn
	data = append(data, common.LeftPadBytes(amountOut.Bytes(), 32)...)
	// 128
	data = append(data, common.LeftPadBytes([]byte{160}, 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountInMax.Bytes(), 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(swapWrapperAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "estimate gas err"), ""
	}
	auth.GasLimit = gasLimit * 5
	//return nil, "gasLimit"

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.SwapTokensForExactTokensAndLPP(
		auth,
		router,
		amountOut,
		path,
		amountInMax,
		to,
	)
	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}
		event, err2 := contractInstance.ParseTokenSwitched(*l)

		if err2 != nil {
			return errors.Wrap(err2, "parse token switched err"), tx.Hash().Hex()
		}
		if event.AmountOut.Cmp(amountOut) == 0 {
			return nil, tx.Hash().Hex()
		}

	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// SwapExactTokensForTokensAndLPPV2 精确的token换tokenB
// https://scan-testnet.kcc.network/tx/0x789b4c8f8b99df9d2959e817e57613f517c1d4decf670f244fbd52fe63a8a6b4
func SwapExactTokensForTokensAndLPPV2(client *ethclient.Client, privateKey string, swapWrapperAddr string, amountIn *big.Int, amountOutMin *big.Int, path []common.Address, to, router common.Address) (error, string) {

	contractInstance, err := newSwapWrapperV2Provider(client, swapWrapperAddr)
	if err != nil {
		return err, ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)
	//balance>1e18
	balance, err := client.BalanceAt(context.Background(), userAddress, nil)
	if err != nil {
		return errors.Wrap(err, "BalanceAt err"), ""
	}
	//大于0.005个 5e15=0.005e18
	if balance.Cmp(decimal.NewFromBigInt(big.NewInt(5), 15).BigInt()) < 0 {
		return errors.New("insufficient native balance"), ""
	}

	methodSig := []byte("swapExactTokensForTokensAndLPP(address,uint256,address[],uint256,address)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0xf6481604
	//00000000000000000000000000000000000000000000000000000000000004d2
	//0000000000000000000000000000000000000000000000000000000000000080
	//0000000000000000000000000000000000000000000000000000000000000000
	//0000000000000000000000000237ce5fcc88afca1d8a5a752f6aa4f5d77ca64d
	//0000000000000000000000000000000000000000000000000000000000000002
	//000000000000000000000000208eecdbc49c137d0174b848def5f8cb74d6951e
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	var data []byte
	data = append(data, methodID...)
	// router addr
	data = append(data, common.LeftPadBytes(router.Bytes(), 32)...)
	// amountIn
	data = append(data, common.LeftPadBytes(amountIn.Bytes(), 32)...)
	// 128
	data = append(data, common.LeftPadBytes([]byte{160}, 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountOutMin.Bytes(), 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(swapWrapperAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5
	//return nil, "gasLimit"

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.SwapExactTokensForTokensAndLPP(
		auth,
		router,
		amountIn,
		path,
		amountOutMin,
		to,
	)
	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}

		event, err2 := contractInstance.ParseTokenSwitched(*l)

		if err2 != nil {
			return errors.Wrap(err2, "parse transfer err"), tx.Hash().Hex()
		}
		if event.AmountIn.Cmp(amountIn) == 0 {
			return nil, tx.Hash().Hex()
		}

	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// SwapTokensForExactETHAndLPPV2 用token0换精确的原生币
// https://scan-testnet.kcc.network/tx/0x70baa993b6514aa965d25616009a46a1eef351f4662335f34042900f55cb8bc1
func SwapTokensForExactETHAndLPPV2(client *ethclient.Client, privateKey string, swapWrapperAddr string, amountOut *big.Int, amountInMax *big.Int, path []common.Address, to, router common.Address) (error, string) {

	contractInstance, err := newSwapWrapperV2Provider(client, swapWrapperAddr)
	if err != nil {
		return err, ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("swapTokensForExactETHAndLPP(address,uint256,address[],uint256,address)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0xcf0d7c4c
	//00000000000000000000000059a4210dd69fdde1457905098ff03e0617a548c5
	//0000000000000000000000000000000000000000000000000000000000000064
	//00000000000000000000000000000000000000000000000000000000000000a0
	//0000000000000000000000000000000000000000000000000000000000000001
	//00000000000000000000000084d238ebb0cb0bf5bca49e448dc73bf69b8cda6c
	//0000000000000000000000000000000000000000000000000000000000000002
	//00000000000000000000000067f6a7bbe0da067a747c6b2bedf8abbf7d6f60dc
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	var data []byte
	data = append(data, methodID...)
	// router addr
	data = append(data, common.LeftPadBytes(router.Bytes(), 32)...)
	// amountIn
	data = append(data, common.LeftPadBytes(amountOut.Bytes(), 32)...)
	// 128
	data = append(data, common.LeftPadBytes([]byte{160}, 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountInMax.Bytes(), 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(swapWrapperAddr)

	//fmt.Println(hexutil.Encode(data))

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "estimate gas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.SwapTokensForExactETHAndLPP(
		auth,
		router,
		amountOut,
		path,
		amountInMax,
		to,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}

		event, err2 := contractInstance.ParseTokenSwitched(*l)

		if err2 != nil {
			return errors.Wrap(err2, "parse token switched err"), ""
		}
		if event.AmountOut.Cmp(amountOut) == 0 {
			return nil, tx.Hash().Hex()
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// SwapExactETHForTokensAndLPPV2 用精确的原生币换token1
// https://scan-testnet.kcc.network/tx/0x555ccbdbdf5ef2d293de1661174b07918fcdd0e1c716c02b11084f3671b6fdb8/token-transfers
func SwapExactETHForTokensAndLPPV2(client *ethclient.Client, privateKey string, swapWrapperAddr string, value, amountOutMin *big.Int, path []common.Address, to, router common.Address) (error, string) {

	contractInstance, err := newSwapWrapperV2Provider(client, swapWrapperAddr)
	if err != nil {
		return err, ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("swapExactETHForTokensAndLPP(address,address[],uint256,address)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0x368e0219
	//0000000000000000000000008c8067ed3bc19acce28c1953bfc18dc85a2127f7
	//0000000000000000000000000000000000000000000000000000000000000080
	//0000000000000000000000000000000000000000000000000000000000000001
	//00000000000000000000000027f2f01c7d160dcd71ce8b267354eed6537747f8
	//0000000000000000000000000000000000000000000000000000000000000002
	//0000000000000000000000004446fc4eb47f2f6586f9faab68b3498f86c07521
	//0000000000000000000000000039f574ee5cc39bdd162e9a88e3eb1f111baf48

	var data []byte
	data = append(data, methodID...)
	// router addr
	data = append(data, common.LeftPadBytes(router.Bytes(), 32)...)
	// 数组
	parseUint, err := strconv.ParseUint(strconv.Itoa(80), 16, 32)
	if err != nil {
		return err, ""
	}
	// 80
	data = append(data, common.LeftPadBytes([]byte{byte(parseUint)}, 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountOutMin.Bytes(), 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(swapWrapperAddr)

	auth.Value = value
	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.SwapExactETHForTokensAndLPP(
		auth,
		router,
		path,
		amountOutMin,
		to,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}

		event, err1 := contractInstance.ParseTokenSwitched(*l)
		if err1 != nil {
			return errors.Wrap(err1, "parse token switched err"), ""
		}
		if event.AmountIn.Cmp(value) == 0 {
			return nil, tx.Hash().Hex()
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}
