package s_mojito

import (
	"context"
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/solcs/sc_mojito/swapwrapper"
	"gitee.com/tfxq/swap-go-sdk/utils"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"math/big"
	"time"
)

func newSwapWrapper(ethClient *ethclient.Client, contractID string) (*swapwrapper.SwapWrapper, error) {
	contractAddress := common.HexToAddress(contractID)
	contract, err := swapwrapper.NewSwapWrapper(contractAddress, ethClient)
	if err != nil {
		return nil, errors.Wrap(err, "init swapwrapper err")
	}
	return contract, nil
}

// SwapTokensForExactTokensAndLPP 用tokenA换精确的tokenB
// https://scan-testnet.kcc.network/tx/0xd10c526c8098dcd75d06379ff7b07b893b6a79468cb0ac1c0c23f068c45694ac/token-transfers
func SwapTokensForExactTokensAndLPP(client *ethclient.Client, privateKey string, swapWrapperAddr string, amountOut *big.Int, amountInMax *big.Int, path []common.Address, to common.Address) (error, string) {

	contractInstance, err := newSwapWrapper(client, swapWrapperAddr)
	if err != nil {
		return err, ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)
	//balance>1e18
	balance, err := client.BalanceAt(context.Background(), userAddress, nil)
	if err != nil {
		return errors.Wrap(err, "BalanceAt err"), ""
	}
	//大于0.005个 5e15=0.005e18
	if balance.Cmp(decimal.NewFromBigInt(big.NewInt(5), 15).BigInt()) < 0 {
		return errors.New("insufficient native balance"), ""
	}

	methodSig := []byte("swapTokensForExactTokensAndLPP(uint256,address[],uint256,address)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0x156aff7b
	//0000000000000000000000000000000000000000000000000000000000000001
	//0000000000000000000000000000000000000000000000000000000000000080
	//0000000000000000000000000000000000000000000000000000000000989680
	//0000000000000000000000000237ce5fcc88afca1d8a5a752f6aa4f5d77ca64d
	//0000000000000000000000000000000000000000000000000000000000000002
	//000000000000000000000000208eecdbc49c137d0174b848def5f8cb74d6951e
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	var data []byte
	data = append(data, methodID...)
	// amountIn
	data = append(data, common.LeftPadBytes(amountOut.Bytes(), 32)...)
	// 128
	data = append(data, common.LeftPadBytes([]byte{128}, 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountInMax.Bytes(), 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(swapWrapperAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "estimate gas err"), ""
	}
	auth.GasLimit = gasLimit * 5
	//return nil, "gasLimit"

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.SwapTokensForExactTokensAndLPP(
		auth,
		amountOut,
		path,
		amountInMax,
		to,
	)
	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}
		event, err2 := contractInstance.ParseTokenSwitched(*l)

		if err2 != nil {
			return errors.Wrap(err2, "parse token switched err"), tx.Hash().Hex()
		}
		if event.AmountOut.Cmp(amountOut) == 0 {
			return nil, tx.Hash().Hex()
		}

	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// SwapExactTokensForTokensAndLPP 精确的token换tokenB
// https://scan-testnet.kcc.network/tx/0x789b4c8f8b99df9d2959e817e57613f517c1d4decf670f244fbd52fe63a8a6b4
func SwapExactTokensForTokensAndLPP(client *ethclient.Client, privateKey string, swapWrapperAddr string, amountIn *big.Int, amountOutMin *big.Int, path []common.Address, to common.Address) (error, string) {

	contractInstance, err := newSwapWrapper(client, swapWrapperAddr)
	if err != nil {
		return err, ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)
	//balance>1e18
	balance, err := client.BalanceAt(context.Background(), userAddress, nil)
	if err != nil {
		return errors.Wrap(err, "BalanceAt err"), ""
	}
	//大于0.005个 5e15=0.005e18
	if balance.Cmp(decimal.NewFromBigInt(big.NewInt(5), 15).BigInt()) < 0 {
		return errors.New("insufficient native balance"), ""
	}

	methodSig := []byte("swapExactTokensForTokensAndLPP(uint256,address[],uint256,address)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0xf6481604
	//00000000000000000000000000000000000000000000000000000000000004d2
	//0000000000000000000000000000000000000000000000000000000000000080
	//0000000000000000000000000000000000000000000000000000000000000000
	//0000000000000000000000000237ce5fcc88afca1d8a5a752f6aa4f5d77ca64d
	//0000000000000000000000000000000000000000000000000000000000000002
	//000000000000000000000000208eecdbc49c137d0174b848def5f8cb74d6951e
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	var data []byte
	data = append(data, methodID...)
	// amountIn
	data = append(data, common.LeftPadBytes(amountIn.Bytes(), 32)...)
	// 128
	data = append(data, common.LeftPadBytes([]byte{128}, 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountOutMin.Bytes(), 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(swapWrapperAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5
	//return nil, "gasLimit"

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.SwapExactTokensForTokensAndLPP(
		auth,
		amountIn,
		path,
		amountOutMin,
		to,
	)
	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}

		event, err2 := contractInstance.ParseTokenSwitched(*l)

		if err2 != nil {
			return errors.Wrap(err2, "parse transfer err"), tx.Hash().Hex()
		}
		if event.AmountIn.Cmp(amountIn) == 0 {
			return nil, tx.Hash().Hex()
		}

	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// SwapTokensForExactETHAndLPP 用token0换精确的原生币
// https://scan-testnet.kcc.network/tx/0x7d7b8ffc4419e8cae61a083f87bc4320e2cbeaaf8e982ec9a40d40018b863cb1
func SwapTokensForExactETHAndLPP(client *ethclient.Client, privateKey string, swapWrapperAddr string, amountOut *big.Int, amountInMax *big.Int, path []common.Address, to common.Address) (error, string) {

	contractInstance, err := newSwapWrapper(client, swapWrapperAddr)
	if err != nil {
		return err, ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("swapTokensForExactETHAndLPP(uint256,address[],uint256,address)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0x8484263e0000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000000000000000000000000000000000008000000000000000000000000000000000000000000000000000000000009896800000000000000000000000000237ce5fcc88afca1d8a5a752f6aa4f5d77ca64d0000000000000000000000000000000000000000000000000000000000000002000000000000000000000000208eecdbc49c137d0174b848def5f8cb74d6951e0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e
	//0x8484263e
	// amountOut
	//0000000000000000000000000000000000000000000000000000000000000001
	// 128行
	//0000000000000000000000000000000000000000000000000000000000000080
	// amountInMax
	//0000000000000000000000000000000000000000000000000000000000989680
	// to
	//0000000000000000000000000237ce5fcc88afca1d8a5a752f6aa4f5d77ca64d
	//0000000000000000000000000000000000000000000000000000000000000002
	//000000000000000000000000208eecdbc49c137d0174b848def5f8cb74d6951e
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e

	var data []byte
	data = append(data, methodID...)
	// amountIn
	data = append(data, common.LeftPadBytes(amountOut.Bytes(), 32)...)
	// 128
	data = append(data, common.LeftPadBytes([]byte{128}, 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountInMax.Bytes(), 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(swapWrapperAddr)

	//fmt.Println(hexutil.Encode(data))

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "estimate gas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.SwapTokensForExactETHAndLPP(
		auth,
		amountOut,
		path,
		amountInMax,
		to,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}

		event, err2 := contractInstance.ParseTokenSwitched(*l)

		if err2 != nil {
			return errors.Wrap(err2, "parse token switched err"), ""
		}
		if event.AmountOut.Cmp(amountOut) == 0 {
			return nil, tx.Hash().Hex()
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// SwapExactETHForTokensAndLPP 用精确的原生币换token1
// https://scan-testnet.kcc.network/tx/0x555ccbdbdf5ef2d293de1661174b07918fcdd0e1c716c02b11084f3671b6fdb8/token-transfers
func SwapExactETHForTokensAndLPP(client *ethclient.Client, privateKey string, swapWrapperAddr string, value, amountOutMin *big.Int, path []common.Address, to common.Address) (error, string) {

	contractInstance, err := newSwapWrapper(client, swapWrapperAddr)
	if err != nil {
		return err, ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("swapExactETHForTokensAndLPP(address[],uint256,address)")
	methodID := crypto.Keccak256(methodSig)[:4]

	//0x671d2257
	//0000000000000000000000000000000000000000000000000000000000000060
	//0000000000000000000000000000000000000000000000000000000000000001
	//0000000000000000000000000237ce5fcc88afca1d8a5a752f6aa4f5d77ca64d
	//0000000000000000000000000000000000000000000000000000000000000002
	//0000000000000000000000006551358edc7fee9adab1e2e49560e68a12e82d9e
	//000000000000000000000000208eecdbc49c137d0174b848def5f8cb74d6951e

	var data []byte
	data = append(data, methodID...)
	// 96 16进制60
	data = append(data, common.LeftPadBytes([]byte{96}, 32)...)
	// amountOutMin
	data = append(data, common.LeftPadBytes(amountOutMin.Bytes(), 32)...)
	// to addr
	data = append(data, common.LeftPadBytes(to.Bytes(), 32)...)
	// path length
	data = append(data, common.LeftPadBytes([]byte{byte(len(path))}, 32)...)
	// path addr
	for _, v := range path {
		data = append(data, common.LeftPadBytes(v.Bytes(), 32)...)
	}

	sendTo := common.HexToAddress(swapWrapperAddr)

	auth.Value = value
	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	auth.Context = ctx

	tx, err := contractInstance.SwapExactETHForTokensAndLPP(
		auth,
		path,
		amountOutMin,
		to,
	)

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		if l.Address.Hex() != contractInstance.Address().Hex() {
			continue
		}

		event, err1 := contractInstance.ParseTokenSwitched(*l)
		if err1 != nil {
			return errors.Wrap(err1, "parse token switched err"), ""
		}
		if event.AmountIn.Cmp(value) == 0 {
			return nil, tx.Hash().Hex()
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}
