package services

import (
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/services/s_common"
	"gitee.com/tfxq/swap-go-sdk/utils"
	"github.com/pkg/errors"
	"math/big"
)

type ERC20BalanceResultModel struct {
	Balance string `json:"balance"`
}

// GetERC20Balance 获取RC20代币余额
func (as *AppService) GetERC20Balance(contractAddr, userAddr string) (*ApiResponse, error) {
	err, balance := s_common.GetErc20Balance(as.rpcClient, contractAddr, userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	return OkWithData(ERC20BalanceResultModel{
		Balance: balance.String(),
	})
}

// GetERC20Balance1 获取RC20代币余额
func (as *AppService) GetERC20Balance1(contractAddr, userAddr string) (*big.Int, error) {
	err, balance := s_common.GetErc20Balance(as.rpcClient, contractAddr, userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return nil, errors.New(m)
	}
	return balance, nil
}

// VerifyERC20BalanceIsGreaterThanAndEqualAmount 校验用户erc20余额是否大于输入金额
func (as *AppService) VerifyERC20BalanceIsGreaterThanAndEqualAmount(contractAddr, userAddr string, amount *big.Int) (bool, error) {
	err, balance := s_common.GetErc20Balance(as.rpcClient, contractAddr, userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return false, errors.New(m)
	}
	if balance.Cmp(amount) >= 0 {
		return true, nil
	}
	return false, nil
}

type ERC20ApprovalResultModel struct {
	Value  string `json:"value"`
	TxHash string `json:"txHash"`
}

// ERC20Approval ERC20代币授权
func (as *AppService) ERC20Approval(contractAddr, spender string, amount *big.Int) (*ApiResponse, error) {
	err, txHash := s_common.Approve(as.rpcClient, as.privateKey, contractAddr, spender, amount)
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(ERC20ApprovalResultModel{
		Value:  amount.String(),
		TxHash: txHash,
	})
}

// ERC20ApprovalMax ERC20代币授权最大
func (as *AppService) ERC20ApprovalMax(contractAddr, spender string) (*ApiResponse, error) {
	maxValue := utils.MaxUint256
	err, txHash := s_common.Approve(as.rpcClient, as.privateKey, contractAddr, spender, maxValue)
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(ERC20ApprovalResultModel{
		Value:  maxValue.String(),
		TxHash: txHash,
	})
}

type ERC20TransferResultModel struct {
	Value  string `json:"value"`
	TxHash string `json:"txHash"`
}

// ERC20Transfer ERC20代币转账
func (as *AppService) ERC20Transfer(contractAddr, recipient string, amount *big.Int) (*ApiResponse, error) {

	err, txHash := s_common.TransferHaveAmountArgs(as.rpcClient, as.privateKey, contractAddr, recipient, amount)
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(ERC20TransferResultModel{
		Value:  amount.String(),
		TxHash: txHash,
	})
}

// ERC20TransferMax ERC20代币转账，转账数量为用户余额的最大值即全部
func (as *AppService) ERC20TransferMax(contractAddr, recipient string) (*ApiResponse, error) {

	err, fromAddr := utils.ConvertKeyToAddr(as.privateKey)
	if err != nil {
		m := fmt.Sprintf("convert key to addr err: %v", err.Error())
		return FailWithCodeAndMessage(CodeConvertKeyToAddrErr, m)
	}
	err, balance := s_common.GetErc20Balance(as.rpcClient, contractAddr, fromAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	if balance.Cmp(big.NewInt(0)) <= 0 {
		m := fmt.Sprintf("insufficient erc20 balance,addr: %v", fromAddr)
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}
	err, txHash := s_common.TransferHaveAmountArgs(as.rpcClient, as.privateKey, contractAddr, recipient, balance)
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(ERC20TransferResultModel{
		Value:  balance.String(),
		TxHash: txHash,
	})
}
