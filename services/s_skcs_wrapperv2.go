package services

import (
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/services/s_mojito"
	"github.com/ethereum/go-ethereum/common"
	"math/big"
)

// SafeSKCSSwap 用tokenA换精确的tokenB
func (as *AppService) SafeSKCSSwap(usdtAddr common.Address, borrowAmount *big.Int, path []common.Address, minProfitAmount *big.Int) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.SafeSKCSSwap(as.rpcClient, as.privateKey, as.Info.Mojito.SKCSWrapperV2,
		usdtAddr, borrowAmount,
		path, minProfitAmount,
	)
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

func (as *AppService) DepositKCSAndSwapExactSKCSForKCSAndToAndVerifyAndLPP(value, txFee *big.Int, path []common.Address, toAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.DepositKCSAndSwapAndLPP(as.rpcClient, as.privateKey, as.Info.Mojito.SKCSWrapperV2, value, txFee, path, common.HexToAddress(toAddr))
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

func (as *AppService) DepositKCSAndSwapAndToAndLPPV2(value, txFee *big.Int, path []common.Address, toAddr, routerAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.DepositKCSAndSwapAndLPPV2(as.rpcClient, as.privateKey, as.Info.Mojito.SKCSWrapperV2, value, txFee, path, common.HexToAddress(toAddr), common.HexToAddress(routerAddr))
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SKCSCall 用tokenA换精确的tokenB
func (as *AppService) SKCSCall(borrowAmount []*big.Int, r1, r2, r3 common.Address, path1, path2, path3 []common.Address, minProfitAmount *big.Int) ([]*s_mojito.SKCSResult, error) {

	// 输入最大花费多少
	array, err := s_mojito.GetSKCSLensArray(as.rpcUrl, as.Info.Chain.Multicall2,
		as.Info.Mojito.SKCSWrapperV2,
		borrowAmount,
		r1,
		r2,
		r3,
		path1,
		path2,
		path3,
		minProfitAmount,
	)
	if err != nil {
		_ = fmt.Errorf("%v", err.Error())
		return nil, err
	}
	return array, nil
}
