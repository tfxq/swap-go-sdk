package services

import (
	"github.com/shopspring/decimal"
	"time"
)

func GetDeadline() decimal.Decimal {
	return decimal.New(time.Now().UTC().Unix(), 0).Add(decimal.New(600, 0))
}
