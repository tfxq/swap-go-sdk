package services

import (
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/services/s_common"
	"gitee.com/tfxq/swap-go-sdk/services/s_mojito"
	"github.com/ethereum/go-ethereum/common"
	"math/big"
)

// SwapTokensForExactETHAndToAndVerifyAndLPP 用的token0代币换准确数量的原生币
func (as *AppService) SwapTokensForExactETHAndToAndVerifyAndLPP(amountOut, amountInMax *big.Int, path []common.Address, userAddr, toAddr string) (*ApiResponse, error) {

	// 判断输出数量固定的token1时，输入token0的🈷余额
	err, balance := s_common.GetErc20Balance(as.rpcClient, path[0].Hex(), userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	if balance.Cmp(amountInMax) < 0 {
		m := fmt.Sprintf("insufficient erc20 balance,addr: %v", path[0].Hex())
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}
	// 输入最大花费多少
	err, txHash := s_mojito.SwapTokensForExactETHAndLPP(as.rpcClient, as.privateKey, as.Info.Mojito.SwapWrapper, amountOut, amountInMax, path, common.HexToAddress(toAddr))
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapTokensForExactTokensAndToAndVerifyAndLPP 用tokenA换精确的tokenB
func (as *AppService) SwapTokensForExactTokensAndToAndVerifyAndLPP(amountOut, amountInMax *big.Int, path []common.Address, userAddr, toAddr string) (*ApiResponse, error) {

	// 判断输出数量固定的token1时，输入token0的🈷余额
	err, balance := s_common.GetErc20Balance(as.rpcClient, path[0].Hex(), userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	if balance.Cmp(amountInMax) < 0 {
		m := fmt.Sprintf("insufficient erc20 balance, addr: %v", path[0].Hex())
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}
	// 输入最大花费多少
	err, txHash := s_mojito.SwapTokensForExactTokensAndLPP(as.rpcClient, as.privateKey, as.Info.Mojito.SwapWrapper, amountOut, amountInMax, path, common.HexToAddress(toAddr))
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactTokensForTokensAndToAndVerifyAndLPP 精确的tokenA换精确的tokenB
func (as *AppService) SwapExactTokensForTokensAndToAndVerifyAndLPP(amountIn, amountOutMin *big.Int, path []common.Address, userAddr, toAddr string) (*ApiResponse, error) {

	// 判断输出数量固定的token1时，输入token0的🈷余额
	err, balance := s_common.GetErc20Balance(as.rpcClient, path[0].Hex(), userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	if balance.Cmp(amountIn) < 0 {
		m := fmt.Sprintf("insufficient erc20 balance, addr: %v", path[0].Hex())
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}
	// 输入最大花费多少
	err, txHash := s_mojito.SwapExactTokensForTokensAndLPP(as.rpcClient, as.privateKey, as.Info.Mojito.SwapWrapper, amountIn, amountOutMin, path, common.HexToAddress(toAddr))
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactETHForTokensAndToAndVerifyAndLPP 精确的原生币换tokenB
func (as *AppService) SwapExactETHForTokensAndToAndVerifyAndLPP(value, amountOutMin *big.Int, path []common.Address, userAddr, toAddr string) (*ApiResponse, error) {

	// 判断输出数量固定的token1时，输入token0的🈷余额
	balance, err := as.GetNativeBalance(userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	if balance.Cmp(value) <= 0 {
		m := fmt.Sprintf("insufficient native balance, addr: %v", path[0].Hex())
		return FailWithCodeAndMessage(CodeBalanceInsufficient, m)
	}
	// 输入最大花费多少
	err, txHash := s_mojito.SwapExactETHForTokensAndLPP(as.rpcClient, as.privateKey, as.Info.Mojito.SwapWrapper, value, amountOutMin, path, common.HexToAddress(toAddr))
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}
