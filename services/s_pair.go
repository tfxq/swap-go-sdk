package services

import (
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/services/s_mojito"
)

type PairBalanceResultModel struct {
	Balance string `json:"balance"`
}

// GetPair20Balance 获取某账户在某交易对合约中的余额
func (as *AppService) GetPair20Balance(contractAddr, userAddr string) (*ApiResponse, error) {
	err, balance := s_mojito.GetPairBalance(as.rpcClient, contractAddr, userAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	return OkWithData(ERC20BalanceResultModel{
		Balance: balance.String(),
	})
}

// GetPairTotalSupply 获取某交易对合约总供应量
func (as *AppService) GetPairTotalSupply(contractAddr string) (*ApiResponse, error) {
	err, balance := s_mojito.GetPairTotalSupply(as.rpcClient, contractAddr)
	if err != nil {
		m := fmt.Sprintf("contract inquiry exceptions: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractInquiryException, m)
	}
	return OkWithData(ERC20BalanceResultModel{
		Balance: balance.String(),
	})
}
