package services

import (
	"gitee.com/tfxq/swap-go-sdk/services/s_common"
	"math/big"
)

func (as *AppService) DepositNative(value *big.Int) (string, error) {
	err, tx := s_common.Deposit(as.rpcClient, as.privateKey, as.Info.Token.WKCSAddress, value)
	if err != nil {
		return "", err
	}
	return tx, err
}

func (as *AppService) WithdrawMaxNativeMax() (string, error) {
	err, tx := s_common.WithdrawMax(as.rpcClient, as.privateKey, as.Info.Token.WKCSAddress)
	if err != nil {
		return "", err
	}
	return tx, err
}
