package s_common

import (
	"context"
	"encoding/json"
	"gitee.com/tfxq/swap-go-sdk/solcs/sc_common/multicall2"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"math/big"
	"strings"
)

type Call struct {
	Name     string         `json:"name"`
	Target   common.Address `json:"target"`
	CallData []byte         `json:"call_data"`
}

type CallResponse struct {
	Success    bool   `json:"success"`
	ReturnData []byte `json:"returnData"`
}

func (call Call) GetMultiCall() multicall2.Multicall2Call {
	return multicall2.Multicall2Call{Target: call.Target, CallData: call.CallData}
}

func randomSigner() *bind.TransactOpts {
	privateKey, err := crypto.GenerateKey()
	if err != nil {
		panic(err)
	}

	signer, err := bind.NewKeyedTransactorWithChainID(privateKey, big.NewInt(1))
	if err != nil {
		panic(err)
	}

	signer.NoSend = true
	signer.Context = context.Background()
	signer.GasPrice = big.NewInt(0)

	return signer
}

type EthMultiCaller struct {
	Signer          *bind.TransactOpts
	Client          *ethclient.Client
	Abi             abi.ABI
	ContractAddress common.Address
}

func NewMulticall2Provider(client *ethclient.Client, contractAddr string) EthMultiCaller {
	// Load Multicall abi for later use
	mcAbi, err := abi.JSON(strings.NewReader(multicall2.Multicall2ABI))
	if err != nil {
		panic(err)
	}

	contractAddress := common.HexToAddress(contractAddr)

	return EthMultiCaller{
		Signer:          randomSigner(),
		Client:          client,
		Abi:             mcAbi,
		ContractAddress: contractAddress,
	}
}

func (caller *EthMultiCaller) Execute(calls []Call) (map[string]CallResponse, error) {
	var responses []CallResponse

	var multiCalls = make([]multicall2.Multicall2Call, 0, len(calls))

	// Add calls to multicall structure for the contract
	for _, call := range calls {
		multiCalls = append(multiCalls, call.GetMultiCall())
	}

	// Prepare calldata for multicall
	callData, err := caller.Abi.Pack("tryAggregate", false, multiCalls)
	if err != nil {
		return nil, err
	}

	// Perform multicall
	resp, err := caller.Client.CallContract(context.Background(), ethereum.CallMsg{To: &caller.ContractAddress, Data: callData}, nil)
	if err != nil {
		return nil, err
	}

	// Unpack results
	unpackedResp, _ := caller.Abi.Unpack("tryAggregate", resp)

	a, err := json.Marshal(unpackedResp[0])
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(a, &responses)
	if err != nil {
		return nil, err
	}

	// Create mapping for results. Be aware that we sometimes get two empty results initially, unsure why
	results := make(map[string]CallResponse)
	for i, response := range responses {
		results[calls[i].Name] = response
	}

	return results, nil
}
