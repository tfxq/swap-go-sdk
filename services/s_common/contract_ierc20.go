package s_common

import (
	"context"
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/solcs/sc_common/ierc20"
	"gitee.com/tfxq/swap-go-sdk/utils"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"math/big"
)

func NewERC20(ethClient *ethclient.Client, contractID string) (*ierc20.IERC20, error) {
	contractAddress := common.HexToAddress(contractID)
	contract, err := ierc20.NewIERC20(contractAddress, ethClient)
	if err != nil {
		return nil, errors.Wrap(err, "init ierc20 err")
	}
	return contract, nil
}

func GetErc20Balance(rpcClient *ethclient.Client, contractAddr string, userAddr string) (error, *big.Int) {

	ercContract, err := NewERC20(rpcClient, contractAddr)
	if err != nil {
		return errors.Wrap(err, "NewERC20 err"), big.NewInt(0)
	}

	balance, err := ercContract.BalanceOf(nil, common.HexToAddress(userAddr))
	if err != nil {
		return errors.Wrap(err, "BalanceOf err"), big.NewInt(0)
	}
	return nil, balance
}

// Approve erc20合约授权
func Approve(rpcClient *ethclient.Client, privateKey string, contractAddr string, spender string, amount *big.Int) (error, string) {

	ercContract, err := NewERC20(rpcClient, contractAddr)
	if err != nil {
		return errors.Wrap(err, "NewERC20 err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, rpcClient)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	allowance, err := ercContract.Allowance(nil, userAddress, common.HexToAddress(spender))
	if err != nil {
		return errors.Wrap(err, "Allowance err"), ""
	}
	// 如果授权额度大于1e91e18
	if allowance.Cmp(decimal.NewFromBigInt(big.NewInt(1000000000), 18).BigInt()) > 0 {
		return errors.New("already approved"), ""
	}
	err, erc20Balance := GetErc20Balance(rpcClient, contractAddr, userAddress.String())
	if err != nil {
		return err, ""
	}
	if erc20Balance.Cmp(big.NewInt(0)) <= 0 {
		return errors.New("insufficient erc20 balance"), ""
	}

	methodSig := []byte("approve(address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	// recipient
	data = append(data, common.LeftPadBytes(common.HexToAddress(spender).Bytes(), 32)...)
	// amount
	data = append(data, common.LeftPadBytes(amount.Bytes(), 32)...)

	sendTo := common.HexToAddress(contractAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := rpcClient.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	tx, err := ercContract.Approve(auth, common.HexToAddress(spender), amount)
	if err != nil {
		return errors.Wrap(err, "Approve err"), ""
	}

	receipt, err := bind.WaitMined(context.Background(), rpcClient, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		//查看是否有path中第一个地址的转账日志，如果有就是交易成功
		if l.Address.Hex() != ercContract.Address().Hex() {
			continue
		}
		event, err := ercContract.ParseApproval(*l)

		if err != nil {
			return errors.Wrap(err, "parse transfer err"), ""
		}
		//校验owner地址是否一样
		if event.Owner.Hex() == userAddress.Hex() {
			//校验spender地址是否一样
			if event.Spender.Hex() == common.HexToAddress(spender).Hex() {
				//校验value金额是否一样
				if event.Value.Cmp(amount) == 0 {
					return nil, tx.Hash().Hex()
				}
			}
		}
	}
	return errors.New("tx not be mined"), tx.Hash().Hex()
}

// TransferHaveAmountArgs  ERC20合约转账
func TransferHaveAmountArgs(rpcClient *ethclient.Client, privateKey string, contractAddr string, recipient string, amount *big.Int) (error, string) {

	ercContract, err := NewERC20(rpcClient, contractAddr)
	if err != nil {
		return errors.Wrap(err, "NewERC20 err"), ""
	}

	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, rpcClient)
	if err != nil {
		return err, ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("transfer(address,uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	// recipient
	data = append(data, common.LeftPadBytes(common.HexToAddress(recipient).Bytes(), 32)...)
	// amount
	data = append(data, common.LeftPadBytes(amount.Bytes(), 32)...)

	sendTo := common.HexToAddress(contractAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := rpcClient.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	tx, err := ercContract.Transfer(auth, common.HexToAddress(recipient), amount)
	if err != nil {
		return errors.Wrap(err, "transfer err"), ""
	}

	receipt, err := bind.WaitMined(context.Background(), rpcClient, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}
	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		//查看是否有path中第一个地址的转账日志，如果有就是交易成功
		if l.Address.Hex() != ercContract.Address().Hex() {
			continue
		}
		event, err := ercContract.ParseTransfer(*l)

		if err != nil {
			return errors.Wrap(err, "parse transfer err"), ""
		}
		//校验to地址是否一样
		if event.To.Hex() == common.HexToAddress(recipient).Hex() {
			if event.Value.Cmp(amount) == 0 {
				return nil, tx.Hash().Hex()
			}
		}
	}

	return errors.New("tx not be mined"), tx.Hash().Hex()
}
