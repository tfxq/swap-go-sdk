package s_common

import (
	"context"
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/solcs/sc_mojito/wkcs"
	"gitee.com/tfxq/swap-go-sdk/utils"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"math/big"
)

func NewWBaseToken(ethClient *ethclient.Client, contractID string) (*wkcs.WKCS, error) {
	contractAddress := common.HexToAddress(contractID)
	contract, err := wkcs.NewWKCS(contractAddress, ethClient)
	if err != nil {
		return nil, errors.Wrap(err, "init wkcs err")
	}
	return contract, nil
}

func Deposit(client *ethclient.Client, privateKey, contractAddr string, value *big.Int) (error, string) {
	contract, err := NewWBaseToken(client, contractAddr)
	if err != nil {
		return err, ""
	}
	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	balance, err := contract.BalanceOf(nil, common.HexToAddress(userAddressString))
	if err != nil {
		return errors.Wrap(err, "BalanceOf err"), ""
	}
	if balance.Cmp(big.NewInt(0)) == 0 {
		return errors.New("insufficient amount"), ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("deposit()")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)

	sendTo := common.HexToAddress(contractAddr)

	auth.Value = value
	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	tx, err := contract.Deposit(auth)
	if err != nil {
		return errors.Wrap(err, "transfer err"), ""
	}

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}

	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		//查看是否有path中第一个地址的转账日志，如果有就是交易成功
		if l.Address.Hex() != contract.Address().Hex() {
			continue
		}
		event, err := contract.ParseDeposit(*l)

		if err != nil {
			return errors.Wrap(err, "parse transfer err"), ""
		}
		//校验to地址是否一样
		if event.Dst.Hex() == common.HexToAddress(userAddressString).Hex() {
			return nil, tx.Hash().Hex()
		}
	}

	return errors.New("tx not be mined"), tx.Hash().Hex()
}

func WithdrawMax(client *ethclient.Client, privateKey, contractAddr string) (error, string) {
	contract, err := NewWBaseToken(client, contractAddr)
	if err != nil {
		return err, ""
	}
	err, userAddressString, auth := utils.ConvertKeyToAuth(privateKey, client)
	if err != nil {
		return err, ""
	}
	balance, err := contract.BalanceOf(nil, common.HexToAddress(userAddressString))
	if err != nil {
		return errors.Wrap(err, "BalanceOf err"), ""
	}
	if balance.Cmp(big.NewInt(0)) == 0 {
		return errors.New("insufficient amount"), ""
	}
	userAddress := common.HexToAddress(userAddressString)

	methodSig := []byte("withdraw(uint256)")
	methodID := crypto.Keccak256(methodSig)[:4]

	var data []byte
	data = append(data, methodID...)
	// amount
	data = append(data, common.LeftPadBytes(balance.Bytes(), 32)...)

	sendTo := common.HexToAddress(contractAddr)

	callMsg := ethereum.CallMsg{
		From:     userAddress,
		To:       &sendTo,
		Gas:      0,
		GasPrice: auth.GasPrice,
		Value:    auth.Value,
		Data:     data,
	}

	gasLimit, err := client.EstimateGas(context.Background(), callMsg)
	if err != nil {
		return errors.Wrap(err, "EstimateGas err"), ""
	}
	auth.GasLimit = gasLimit * 5

	tx, err := contract.Withdraw(auth, balance)
	if err != nil {
		return errors.Wrap(err, "transfer err"), ""
	}

	receipt, err := bind.WaitMined(context.Background(), client, tx)
	if err != nil {
		msg := fmt.Sprintf("wait mined err,txhash: %v", tx.Hash().Hex())
		return errors.Wrap(err, msg), tx.Hash().Hex()
	}

	if receipt.Status == 0 {
		return errors.New("request err,receipt status is zero"), ""
	}
	for _, l := range receipt.Logs {
		//查看是否有path中第一个地址的转账日志，如果有就是交易成功
		if l.Address.Hex() != contract.Address().Hex() {
			continue
		}
		event, err := contract.ParseWithdrawal(*l)

		if err != nil {
			return errors.Wrap(err, "parse transfer err"), ""
		}
		//校验to地址是否一样
		if event.Src.Hex() == common.HexToAddress(userAddressString).Hex() {
			return nil, tx.Hash().Hex()
		}
	}

	return errors.New("tx not be mined"), tx.Hash().Hex()
}
