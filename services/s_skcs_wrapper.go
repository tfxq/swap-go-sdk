package services

import (
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/services/s_mojito"
	"github.com/ethereum/go-ethereum/common"
	"math/big"
)

// DepositKCSAndSwapExactTokensForTokensAndToAndVerifyAndLPP 用tokenA换精确的tokenB
func (as *AppService) DepositKCSAndSwapExactTokensForTokensAndToAndVerifyAndLPP(value, txFee *big.Int, path []common.Address, toAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.DepositKCSAndSwapAndLPP(as.rpcClient, as.privateKey, as.Info.Mojito.SKCSWrapper, value, txFee, path, common.HexToAddress(toAddr))
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}
