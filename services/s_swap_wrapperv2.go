package services

import (
	"fmt"
	"gitee.com/tfxq/swap-go-sdk/services/s_mojito"
	"github.com/ethereum/go-ethereum/common"
	"math/big"
)

// SwapTokensForExactETHAndToAndLPPV2 用的token0代币换准确数量的原生币
func (as *AppService) SwapTokensForExactETHAndToAndLPPV2(amountOut, amountInMax *big.Int, path []common.Address, userAddr, toAddr, routerAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.SwapTokensForExactETHAndLPPV2(as.rpcClient,
		as.privateKey, as.Info.Mojito.SwapWrapperV2,
		amountOut, amountInMax, path,
		common.HexToAddress(toAddr),
		common.HexToAddress(routerAddr),
	)
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapTokensForExactTokensAndToAndLPPV2 用tokenA换精确的tokenB
func (as *AppService) SwapTokensForExactTokensAndToAndLPPV2(amountOut, amountInMax *big.Int, path []common.Address, userAddr, toAddr, routerAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.SwapTokensForExactTokensAndLPPV2(as.rpcClient, as.privateKey, as.Info.Mojito.SwapWrapperV2,
		amountOut, amountInMax, path,
		common.HexToAddress(toAddr),
		common.HexToAddress(routerAddr),
	)
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactTokensForTokensAndToAndLPPV2 精确的tokenA换精确的tokenB
func (as *AppService) SwapExactTokensForTokensAndToAndLPPV2(amountIn, amountOutMin *big.Int, path []common.Address, userAddr, toAddr, routerAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.SwapExactTokensForTokensAndLPPV2(as.rpcClient, as.privateKey, as.Info.Mojito.SwapWrapperV2,
		amountIn, amountOutMin, path,
		common.HexToAddress(toAddr),
		common.HexToAddress(routerAddr),
	)
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}

// SwapExactETHForTokensAndToAndLPPV2 精确的原生币换tokenB
func (as *AppService) SwapExactETHForTokensAndToAndLPPV2(value, amountOutMin *big.Int, path []common.Address, userAddr, toAddr, routerAddr string) (*ApiResponse, error) {

	// 输入最大花费多少
	err, txHash := s_mojito.SwapExactETHForTokensAndLPPV2(as.rpcClient, as.privateKey, as.Info.Mojito.SwapWrapperV2,
		value, amountOutMin, path,
		common.HexToAddress(toAddr),
		common.HexToAddress(routerAddr),
	)
	if err != nil {
		m := fmt.Sprintf("contract execution error: %v", err.Error())
		return FailWithCodeAndMessage(CodeContractExecutionErr, m)
	}
	return OkWithData(SwapExactTokensForTokensResultModel{
		TxHash: txHash,
	})
}
