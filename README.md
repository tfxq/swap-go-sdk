
# Go SDK for Swap

## Install

```bash
go get gitee.com/tfxq/swap-go-sdk
```



### HTTP错误码

### 系统错误码

| 代码	    | 含义                          |
|--------|-----------------------------|
| 300001 | EstimateGas err             |
| 300002 | contract inquiry exception  |
| 300003 | balance insufficient        |
| 400001 | to decimal convert err      |

### Notice

swapWrapper 合约先初始化，设置交易员，然后授权给该合约usdt，usdc，eth，btc额度

skcsWrapper 合约部署后，设置交易员即可

### ToDO

swap input data弄懂

### solc

```
pip3 install solc-select
solc-select install 0.7.6
solc-select use 0.7.6

solc-select install 0.6.6
solc-select use 0.6.6

solc-select install 0.8.0
solc-select use 0.8.0

solc-select install 0.8.0
solc-select use 0.8.0

solc-select install 0.8.15
solc-select use 0.8.15

solc-select install 0.4.18
solc-select use 0.4.18
```

### OKC

- 代币列表

https://www.oklink.com/zh-cn/okc/token-list

### 参考

- https://github.com/OpenZeppelin/openzeppelin-subgraphs/blob/main/generated/erc20.schema.graphql

- https://github.com/Uniswap/v2-subgraph/blob/master/schema.graphql

- https://github.com/ethereum/go-ethereum/blob/master/accounts/abi/bind/bind_test.go